$(document).ready(function () {
    if (window.history.replaceState) window.history.replaceState(null, null, window.location.href);
});


/*$(document).ready(function(){
    $('.date_time').mask('00/00/0000 00:00:00');
    $('.cep').mask('00000-000');
    $('.phone_us').mask('(00) 9 000-0000');
    $('.crm').mask('0000 0000-0');
    $('.cpf').mask('000.000.000-00', {reverse: false});
  });*/


$('.dt-table').DataTable({
    responsive: true
});

$('input[type="text"][data-form="money"]').on('keypress', function (e) {
    return App.moeda(this, '', '.', e);
});

$("#fileUpload").on('change', function () {
    if (typeof (FileReader) != "undefined") {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#image-holder-img').attr('src', e.target.result);
        }
        var file = $(this)[0].files[0];
        if (typeof (file) != "undefined") reader.readAsDataURL($(this)[0].files[0]);

    } else {
        alert("Este navegador nao suporta FileReader.");
    }
});

$('.card-avatar').bind('click', function () {
    $('#foto_branca_file').click();
});

$('#image-holder').click(function () {
    $('#fileUpload').click();
});

var App = {
    logout: () => {
        let link = 'app/ajax/logout.php';

        $.ajax({
            url: link,
            type: 'POST',
            success: function () {
                window.location = './home';
            }
        });
    },
    search: () => {
        let link = 'app/ajax/search.php';
        alert("testando");
        /*$.ajax({
            url: link,
            type: 'POST',
            data: valor,
            success: function (data) {
                alert("enviou");
            }
        });*/
    },
    moeda: (a, e, r, t) => {
        let n = "",
            h = j = 0,
            u = tamanho2 = 0,
            l = ajd2 = "",
            o = window.Event ? t.which : t.keyCode;
        if (13 == o || 8 == o)
            return !0;
        if (n = String.fromCharCode(o),
            -1 == "0123456789".indexOf(n))
            return !1;
        for (u = a.value.length,
            h = 0; h < u && ("0" == a.value.charAt(h) || a.value.charAt(h) == r); h++)
            ;
        for (l = ""; h < u; h++)
            -
                1 != "0123456789".indexOf(a.value.charAt(h)) && (l += a.value.charAt(h));
        if (l += n,
            0 == (u = l.length) && (a.value = ""),
            1 == u && (a.value = "0" + r + "0" + l),
            2 == u && (a.value = "0" + r + l),
            u > 2) {
            for (ajd2 = "",
                j = 0,
                h = u - 3; h >= 0; h--)
                3 == j && (ajd2 += e,
                    j = 0),
                    ajd2 += l.charAt(h),
                    j++;
            for (a.value = "",
                tamanho2 = ajd2.length,
                h = tamanho2 - 1; h >= 0; h--)
                a.value += ajd2.charAt(h);
            a.value += r + l.substr(u - 2, u)
        }
        return !1
    }
}