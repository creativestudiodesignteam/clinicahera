<?php

include 'app/core/connection.class.php';
include 'app/core/session.class.php';
include 'app/core/title.class.php';
include 'app/core/url.class.php';
include 'app/core/notify.class.php';
include 'app/core/components.class.php';

$db         = new Connection();
$url        = new URL();
$session    = new Session();
$components = new Components();

$db = $db->connect();
$session->start();

$components->header();

foreach ($_GET as $key => $value) if (!is_array($value)) $_GET[$key] = $url->filter($value);
foreach ($_POST as $key => $value) if (!is_array($value)) $_POST[$key] = $url->filter($value);

?>
<div class="container-fluid">
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		<?php $url->page(); ?>
	</div>
</div>

<?php $components->footer(); ?>