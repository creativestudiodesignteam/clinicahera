-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 25-Mar-2020 às 19:48
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `hera_admin`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `addresses`
--

CREATE TABLE `addresses` (
  `id` int(10) NOT NULL,
  `postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `neighborhood` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `complement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `addresses`
--

INSERT INTO `addresses` (`id`, `postcode`, `state`, `city`, `neighborhood`, `street`, `number`, `complement`, `created_at`, `updated_at`) VALUES
(1, '', 'CE', '', '', '', '', '', '2020-02-06 18:27:44', '2020-03-09 02:20:57'),
(2, '00000-000', 'CE', 'teste', 'teste', 'teste', 'teste', 'teste', '2020-02-06 18:27:44', '2020-02-17 18:49:12'),
(3, '00000-000', 'CE', 'teste', 'teste', 'teste', 'teste', 'teste', '2020-02-06 18:29:03', '2020-02-06 18:29:03'),
(4, '00000-000', 'CE', 'teste', 'teste', 'teste', 'teste', 'teste', '2020-02-06 18:29:04', '2020-02-06 18:29:04'),
(5, '00000-000', 'CE', 'teste', 'teste', 'teste', 'teste', 'teste', '2020-02-06 18:30:12', '2020-02-06 18:30:12'),
(6, '00000-000', 'CE', 'teste', 'teste', 'teste', 'teste', 'teste', '2020-02-06 18:30:30', '2020-02-06 18:30:30'),
(7, '00000-000', 'CE', 'teste', 'teste', 'teste', 'teste', 'teste', '2020-02-06 18:33:29', '2020-02-06 18:33:29'),
(8, '00000-000', 'CE', 'Morada News', 'bairro', 'rua', 'numero', 'só testando', '2020-02-12 13:35:27', '2020-02-12 13:35:27'),
(9, '00000-000', 'CE', 'dasda', 'dasda', 'dffsd', 'fsfsd', 'fsdfs', '2020-02-13 18:36:17', '2020-02-13 18:36:17'),
(10, '00000-000', 'RN', 'fsdf', 'fdsdfs', 'fsdfs', '0fsdfs', 'fsdf', '2020-02-13 18:37:13', '2020-02-13 18:37:13'),
(11, '00000-000', 'RN', 'fsdf', 'fdsdfs', 'fsdfs', '0fsdfs', 'fsdf', '2020-02-13 18:37:53', '2020-02-13 18:37:53'),
(12, '00000-000', 'RN', 'fsdf', 'fdsdfs', 'fsdfs', '0fsdfs', 'fsdf', '2020-02-13 18:38:22', '2020-02-13 18:38:22'),
(13, '00000-000', 'RN', 'fsdf', 'fdsdfs', 'fsdfs', '0fsdfs', 'fsdf', '2020-02-13 18:40:26', '2020-02-13 18:40:26'),
(14, '00000-000', 'RN', 'fsdf', 'fdsdfs', 'fsdfs', '0fsdfs', 'fsdf', '2020-02-13 18:40:40', '2020-02-13 18:40:40'),
(15, '00000-000', 'RN', 'fsdf', 'fdsdfs', 'fsdfs', '0fsdfs', 'fsdf', '2020-02-13 18:41:27', '2020-02-13 18:41:27'),
(16, '00000-000', 'RN', 'fsdf', 'fdsdfs', 'fsdfs', '0fsdfs', 'fsdf', '2020-02-13 18:41:52', '2020-02-13 18:41:52'),
(17, '00000-000', 'RN', 'fsdf', 'fdsdfs', 'fsdfs', '0fsdfs', 'fsdf', '2020-02-13 18:42:06', '2020-02-13 18:42:06'),
(18, '00000-000', 'MG', '000000000', '0000000', '00000000', '00000', '0', '2020-02-13 18:43:24', '2020-02-13 18:43:24'),
(19, '04814-000', '19', 'Morada Novaaaa', 'Alto Tiradentes', 'Av presidente Geisel', '1400', 'testeasda', '2020-02-14 15:06:57', '2020-02-17 14:09:50'),
(20, '00000-000', 'AC', 'fsdfsdf', '00000', '00000000', '0000000', '000000', '2020-02-17 20:35:51', '2020-02-17 20:35:51'),
(21, '00000-000', 'AC', 'fsdfsdf', '00000', '00000000', '0000000', '000000', '2020-02-17 20:36:32', '2020-02-17 20:36:32'),
(22, '00000-000', 'AC', 'fsdfsdf', '00000', '00000000', '0000000', '000000', '2020-02-17 20:37:23', '2020-02-17 20:37:23'),
(23, '00000-000', 'AC', 'fsdfsdf', '00000', '00000000', '0000000', '000000', '2020-02-17 20:38:29', '2020-02-17 20:38:29'),
(24, '00000-000', 'RJ', 'fsdfsdf', 'fsdfsd', '0fsdfs', '', 'gdfgdf', '2020-02-17 20:38:55', '2020-02-17 20:38:55'),
(25, '00000-000', 'RJ', 'fsdfsdf', 'fsdfsd', '0fsdfs', '', 'gdfgdf', '2020-02-17 20:41:30', '2020-02-17 20:41:30'),
(26, '00000-000', 'PA', 'fsdfsdf', 'gdfgdfg', 'gdfgdf', 'dsadsd', 'asdas', '2020-02-17 20:41:59', '2020-02-17 20:41:59'),
(27, '00000-000', 'PA', 'fsdfsdf', 'gdfgdfg', 'gdfgdf', 'dsadsd', 'asdas', '2020-02-17 20:42:22', '2020-02-17 20:42:22'),
(28, '00000-000', 'PA', 'fsdfsdf', 'gdfgdfg', 'gdfgdf', 'dsadsd', 'asdas', '2020-02-17 20:43:08', '2020-02-17 20:43:08'),
(29, '00000-000', 'PA', 'fsdfsdf', 'gdfgdfg', 'gdfgdf', 'dsadsd', 'asdas', '2020-02-17 20:43:34', '2020-02-17 20:43:34'),
(30, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 02:45:33', '2020-02-18 02:45:33'),
(31, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 02:45:56', '2020-02-18 02:45:56'),
(32, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 02:46:12', '2020-02-18 02:46:12'),
(33, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 02:46:35', '2020-02-18 02:46:35'),
(34, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 02:46:59', '2020-02-18 02:46:59'),
(35, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 02:47:00', '2020-02-18 02:47:00'),
(36, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 02:47:07', '2020-02-18 02:47:07'),
(37, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 02:48:20', '2020-02-18 02:48:20'),
(38, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 02:48:37', '2020-02-18 02:48:37'),
(39, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 02:49:11', '2020-02-18 02:49:11'),
(40, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 02:49:31', '2020-02-18 02:49:31'),
(41, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 02:49:42', '2020-02-18 02:49:42'),
(42, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 02:50:00', '2020-02-18 02:50:00'),
(43, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 02:51:11', '2020-02-18 02:51:11'),
(44, '00000-000', 'CE', 'dasda', 'fsdfsd', 'fsdfsd', '00000', 'fsdfds', '2020-02-18 02:51:41', '2020-02-18 02:51:41'),
(45, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'fsdfsd', 'fsdfsdf', 'fsdfs', '2020-02-18 02:52:29', '2020-02-18 02:52:29'),
(46, '00000-000', 'CE', 'Morada News', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 12:57:36', '2020-02-18 12:57:36'),
(47, '00000-000', 'CE', 'Morada News', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 12:59:50', '2020-02-18 12:59:50'),
(48, '00000-000', 'CE', 'Morada News', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:00:14', '2020-02-18 13:00:14'),
(49, '00000-000', 'CE', 'Morada News', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:01:24', '2020-02-18 13:01:24'),
(50, '00000-000', 'CE', 'Morada News', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:01:30', '2020-02-18 13:01:30'),
(51, '00000-000', 'CE', 'Morada News', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:01:31', '2020-02-18 13:01:31'),
(52, '00000-000', 'CE', 'Morada News', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:01:40', '2020-02-18 13:01:40'),
(53, '00000-000', 'CE', 'Morada News', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:01:40', '2020-02-18 13:01:40'),
(54, '00000-000', 'CE', 'Morada News', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:01:59', '2020-02-18 13:01:59'),
(55, '00000-000', 'CE', 'Morada News', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:01:59', '2020-02-18 13:01:59'),
(56, '00000-000', 'CE', 'Morada News', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:02:14', '2020-02-18 13:02:14'),
(57, '00000-000', 'CE', 'Morada News', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:02:42', '2020-02-18 13:02:42'),
(58, '00000-000', 'CE', 'Morada News', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:02:50', '2020-02-18 13:02:50'),
(59, '00000-000', 'CE', 'Morada News', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:04:45', '2020-02-18 13:04:45'),
(60, '00000-000', 'CE', 'Morada News', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:04:47', '2020-02-18 13:04:47'),
(61, '00000-000', 'CE', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:05:24', '2020-02-18 13:05:24'),
(62, '00000-000', 'CE', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:06:57', '2020-02-18 13:06:57'),
(63, '00000-000', 'CE', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:07:18', '2020-02-18 13:07:18'),
(64, '00000-000', 'CE', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:07:26', '2020-02-18 13:07:26'),
(65, '00000-000', 'CE', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:08:26', '2020-02-18 13:08:26'),
(66, '00000-000', 'CE', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:08:27', '2020-02-18 13:08:27'),
(67, '00000-000', 'CE', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:08:28', '2020-02-18 13:08:28'),
(68, '00000-000', 'CE', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:09:41', '2020-02-18 13:09:41'),
(69, '00000-000', 'CE', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:10:23', '2020-02-18 13:10:23'),
(70, '00000-000', 'CE', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:10:33', '2020-02-18 13:10:33'),
(71, '00000-000', 'CE', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:10:54', '2020-02-18 13:10:54'),
(72, '00000-000', 'CE', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 13:11:02', '2020-02-18 13:11:02'),
(73, '00000-000', 'CE', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 14:05:15', '2020-02-18 14:05:15'),
(74, '00000-000', 'CE', 'Morada nova', 'Av presidente geisel', 'teste', 'teste', 'teste', '2020-02-18 14:10:45', '2020-02-18 14:38:29'),
(75, '00000-000', 'CE', 'AC', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 14:36:09', '2020-02-18 14:36:09'),
(76, '20000-000', 'AL', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 18:39:04', '2020-02-18 18:39:04'),
(77, '00000-000', 'CE', '00000000', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 18:39:32', '2020-02-18 18:39:32'),
(78, '00000-000', 'CE', 'Morada nova', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 18:40:55', '2020-02-18 18:41:25'),
(79, '00000-000', 'AC', 'fsdfsdf', '0000000', 'teste', 'teste', 'teste', '2020-02-18 18:43:39', '2020-02-18 18:43:39'),
(80, '00000-000', 'CE', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-18 18:45:30', '2020-02-18 18:45:30'),
(81, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:10:51', '2020-02-20 02:10:51'),
(82, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:11:15', '2020-02-20 02:11:15'),
(83, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:11:16', '2020-02-20 02:11:16'),
(84, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:11:33', '2020-02-20 02:11:33'),
(85, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:13:48', '2020-02-20 02:13:48'),
(86, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:14:19', '2020-02-20 02:14:19'),
(87, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:15:24', '2020-02-20 02:15:24'),
(88, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:15:44', '2020-02-20 02:15:44'),
(89, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:16:05', '2020-02-20 02:16:05'),
(90, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:16:57', '2020-02-20 02:16:57'),
(91, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:17:47', '2020-02-20 02:17:47'),
(92, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:19:08', '2020-02-20 02:19:08'),
(93, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:21:16', '2020-02-20 02:21:16'),
(94, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:21:28', '2020-02-20 02:21:28'),
(95, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:21:36', '2020-02-20 02:21:36'),
(96, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:21:36', '2020-02-20 02:21:36'),
(97, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:21:48', '2020-02-20 02:21:48'),
(98, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:21:57', '2020-02-20 02:21:57'),
(99, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:27:39', '2020-02-20 02:27:39'),
(100, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:27:56', '2020-02-20 02:27:56'),
(101, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:28:20', '2020-02-20 02:28:20'),
(102, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:37:08', '2020-02-20 02:37:08'),
(103, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:37:49', '2020-02-20 02:37:49'),
(104, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:38:55', '2020-02-20 02:38:55'),
(105, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:40:26', '2020-02-20 02:40:26'),
(106, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:40:27', '2020-02-20 02:40:27'),
(107, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:40:40', '2020-02-20 02:40:40'),
(108, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:41:11', '2020-02-20 02:41:11'),
(109, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:41:12', '2020-02-20 02:41:12'),
(110, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:41:22', '2020-02-20 02:41:22'),
(111, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:41:29', '2020-02-20 02:41:29'),
(112, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:41:29', '2020-02-20 02:41:29'),
(113, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:41:39', '2020-02-20 02:41:39'),
(114, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:41:52', '2020-02-20 02:41:52'),
(115, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:42:33', '2020-02-20 02:42:33'),
(116, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:42:35', '2020-02-20 02:42:35'),
(117, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:42:58', '2020-02-20 02:42:58'),
(118, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:43:33', '2020-02-20 02:43:33'),
(119, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:47:21', '2020-02-20 02:47:21'),
(120, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:48:57', '2020-02-20 02:48:57'),
(121, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:48:59', '2020-02-20 02:48:59'),
(122, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:48:59', '2020-02-20 02:48:59'),
(123, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:49:00', '2020-02-20 02:49:00'),
(124, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:49:01', '2020-02-20 02:49:01'),
(125, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 02:49:17', '2020-02-20 02:49:17'),
(126, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 03:04:09', '2020-02-20 03:04:09'),
(127, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 03:04:10', '2020-02-20 03:04:10'),
(128, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 03:04:48', '2020-02-20 03:04:48'),
(129, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 03:05:20', '2020-02-20 03:05:20'),
(130, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 03:05:35', '2020-02-20 03:05:35'),
(131, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 03:06:58', '2020-02-20 03:06:58'),
(132, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 03:10:28', '2020-02-20 03:10:28'),
(133, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 03:15:38', '2020-02-20 03:15:38'),
(134, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 03:15:39', '2020-02-20 03:15:39'),
(135, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 03:17:21', '2020-02-20 03:17:21'),
(136, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 03:18:07', '2020-02-20 03:18:07'),
(137, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 03:18:58', '2020-02-20 03:18:58'),
(138, '00000-000', 'PE', 'fsdfsdf', 'gdfgdfg', 'dasda', 'teste', 'teste', '2020-02-20 03:20:27', '2020-02-20 03:20:27'),
(139, '00000-000', 'PE', 'fsdfsdf', 'gdfgdfg', 'dasda', 'teste', 'teste', '2020-02-20 03:20:45', '2020-02-20 03:20:45'),
(140, '00000-000', 'PE', 'fsdfsdf', 'gdfgdfg', 'dasda', 'teste', 'teste', '2020-02-20 03:21:25', '2020-02-20 03:21:25'),
(141, '00000-000', 'PE', 'fsdfsdf', 'gdfgdfg', 'dasda', 'teste', 'teste', '2020-02-20 03:23:24', '2020-02-20 03:23:24'),
(142, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 03:23:51', '2020-02-20 03:23:51'),
(143, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 03:24:16', '2020-02-20 03:24:16'),
(144, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 03:24:17', '2020-02-20 03:24:17'),
(145, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 03:25:24', '2020-02-20 03:25:24'),
(146, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 03:25:25', '2020-02-20 03:25:25'),
(147, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 03:25:25', '2020-02-20 03:25:25'),
(148, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 03:26:06', '2020-02-20 03:26:06'),
(149, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 03:26:18', '2020-02-20 03:26:18'),
(150, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 13:29:38', '2020-02-20 13:29:38'),
(151, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 13:31:17', '2020-02-20 13:31:17'),
(152, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 13:31:28', '2020-02-20 13:31:28'),
(153, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 13:59:31', '2020-02-20 13:59:31'),
(154, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 14:01:47', '2020-02-20 14:01:47'),
(155, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-02-20 14:02:04', '2020-02-20 14:02:04'),
(156, '00000-000', 'AC', 'fsdfsdf', 'fsdfsdf', 'teste', 'teste', 'teste', '2020-03-03 17:31:23', '2020-03-03 17:31:23'),
(157, '00000-000', 'PI', 'fsdfsdf', 'dasda', 'gdfgdf', 'dgfdg', 'asdas', '2020-03-06 17:40:01', '2020-03-06 17:40:01'),
(158, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-03-06 17:41:06', '2020-03-06 17:41:06'),
(159, '00000-000', 'AC', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-03-06 17:42:19', '2020-03-06 17:42:19'),
(160, '00000-000', 'CE', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-03-06 17:42:40', '2020-03-06 18:29:34'),
(161, '00000-000', 'AL', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-03-09 01:20:39', '2020-03-09 01:20:39'),
(162, '00000-000', 'AL', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-03-09 01:21:36', '2020-03-09 01:21:36'),
(163, '00000-000', 'AL', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-03-09 01:21:43', '2020-03-09 01:21:43'),
(164, '00000-000', 'AL', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-03-09 01:22:14', '2020-03-09 01:22:14'),
(165, '00000-000', 'AL', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-03-09 01:25:01', '2020-03-09 01:25:01'),
(166, '00000-000', 'AL', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-03-09 01:26:18', '2020-03-09 01:26:18'),
(167, '20000-000', 'SP', 'fsdfsdf', 'teste', '00000000', '00000', 'asdas', '2020-03-09 01:26:50', '2020-03-09 01:26:50'),
(168, '20000-000', 'SP', 'fsdfsdf', 'teste', '00000000', '00000', 'asdas', '2020-03-09 01:27:14', '2020-03-09 01:27:14'),
(169, '', 'SP', '', '', '', '', '', '2020-03-09 01:31:59', '2020-03-09 02:23:02'),
(170, '20000-000', 'SP', 'fsdfsdf', 'teste', '00000000', '00000', 'asdas', '2020-03-09 01:32:58', '2020-03-09 01:32:58'),
(171, '20000-000', 'SP', 'fsdfsdf', 'teste', '00000000', '00000', 'asdas', '2020-03-09 01:33:16', '2020-03-09 01:33:16'),
(172, '20000-000', 'SP', 'fsdfsdf', 'teste', '00000000', '00000', 'asdas', '2020-03-09 01:33:31', '2020-03-09 01:33:31'),
(173, '20000-000', 'SP', 'fsdfsdf', 'teste', '00000000', '00000', 'asdas', '2020-03-09 01:33:49', '2020-03-09 01:33:49'),
(174, '20000-000', 'SP', 'fsdfsdf', 'teste', '00000000', '', 'asdas', '2020-03-09 01:34:13', '2020-03-09 02:04:02'),
(175, '20000-000', 'SP', 'fsdfsdf', 'teste', '00000000', '00000', 'asdas', '2020-03-09 01:34:40', '2020-03-09 01:34:40'),
(176, '00000-000', 'CE', 'fsdfsdf', 'gdfgdfg', 'teste', '02', 'fsdfs', '2020-03-09 16:54:50', '2020-03-09 16:54:50'),
(177, '00000-000', 'CE', 'fsdfsdf', 'gdfgdfg', 'teste', '02', 'fsdfs', '2020-03-09 16:55:58', '2020-03-09 16:55:58'),
(178, '00000-000', 'CE', 'fsdfsdf', 'gdfgdfg', 'teste', '02', 'fsdfs', '2020-03-09 16:55:59', '2020-03-09 16:55:59'),
(179, '00000-000', 'CE', 'teste', 'gdfgdfg', 'fsdfsd', 'dasd', 'asdas', '2020-03-09 16:57:48', '2020-03-09 16:57:48'),
(180, '00000-000', 'CE', 'teste', 'gdfgdfg', 'fsdfsd', 'dasd', 'asdas', '2020-03-09 16:58:27', '2020-03-09 16:58:27'),
(181, '00000-000', 'CE', 'fsdfsdf', 'fsdfsdf', 'teste', '4234', '', '2020-03-09 16:59:04', '2020-03-09 16:59:04'),
(182, '00000-000', 'CE', '00000000', 'gdfgdfg', '000000', '0000000000', '00000000', '2020-03-09 17:00:36', '2020-03-09 17:00:36'),
(183, '00000-000', 'CE', '00000000', 'gdfgdfg', '000000', '0000000000', '00000000', '2020-03-09 17:01:05', '2020-03-09 17:01:05'),
(184, '00000-000', 'CE', '00000000', 'gdfgdfg', '000000', '0000000000', '00000000', '2020-03-09 17:01:26', '2020-03-09 17:01:26'),
(185, '00000-000', 'CE', '00000000', 'gdfgdfg', '000000', '0000000000', '00000000', '2020-03-09 17:01:54', '2020-03-09 17:01:54'),
(186, '00000-000', 'CE', 'Morada Nova', 'Centro', 'Não sei ', '2254', 'fhsodhf', '2020-03-09 18:06:57', '2020-03-09 18:06:57'),
(187, '00000-000', 'CE', 'fsdfsdf', 'gdfgdfg', 'teste', 'teste', 'teste', '2020-03-10 17:01:39', '2020-03-10 17:01:39');

-- --------------------------------------------------------

--
-- Estrutura da tabela `bank_account`
--

CREATE TABLE `bank_account` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `number` varchar(255) NOT NULL,
  `agency` varchar(255) NOT NULL,
  `account` varchar(255) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `bank_account`
--

INSERT INTO `bank_account` (`id`, `name`, `number`, `agency`, `account`, `create_at`, `update_at`) VALUES
(1, 'Bradesco', '00000000000000000000', '0000000', '000000000000', '2020-03-09 16:54:50', '2020-03-09 16:54:50'),
(2, 'Bradesco', '00000000000000000000', '0000000', '000000000000', '2020-03-09 16:55:58', '2020-03-09 16:55:58'),
(3, 'Bradesco', '00000000000000000000', '0000000', '000000000000', '2020-03-09 16:55:59', '2020-03-09 16:55:59'),
(4, 'Bradesco', '00000000000', '0000000', '000000000000', '2020-03-09 16:57:48', '2020-03-09 16:57:48'),
(5, 'Bradesco', '00000000000', '0000000', '000000000000', '2020-03-09 16:58:27', '2020-03-09 16:58:27'),
(6, 'Bradesco', 'dasda', '0000000', '000000000000', '2020-03-09 16:59:04', '2020-03-09 16:59:04'),
(7, 'Bradesco', 'dasd', '0000000', '000000000000', '2020-03-09 17:00:36', '2020-03-09 17:00:36'),
(8, 'Bradesco', 'dasd', '0000000', '000000000000', '2020-03-09 17:01:05', '2020-03-09 17:01:05'),
(9, 'Bradesco', '42342', '0000000', '000000000000', '2020-03-09 17:01:26', '2020-03-09 17:01:26'),
(10, 'Bradesco', '42342', '0000000', '00.000.000/0000-00', '2020-03-09 17:01:54', '2020-03-09 17:15:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `categories`
--

INSERT INTO `categories` (`id`, `name`, `create_at`, `update_at`) VALUES
(1, 'teste', '2020-02-19 14:04:33', '2020-02-19 14:04:33'),
(2, 'dsads', '2020-02-19 14:22:57', '2020-02-19 14:22:57'),
(3, 'Testeee', '2020-02-19 14:23:24', '2020-02-19 14:23:24'),
(4, 'testanu', '2020-02-19 14:23:39', '2020-02-19 14:23:39'),
(5, '1', '2020-02-19 14:27:24', '2020-02-19 14:27:24');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clinics`
--

CREATE TABLE `clinics` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cnpj` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tellphone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cellphone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_addresses` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `clinics`
--

INSERT INTO `clinics` (`id`, `name`, `cnpj`, `email`, `tellphone`, `cellphone`, `fk_addresses`, `created_at`, `updated_at`) VALUES
(6, 'Vila Mariana', '00.000.000/0000-00', 'rabelojunior105@gmail.com', '(00) 0000-0000', '(00) 0 0000-0000', 8, '2020-02-12 13:35:27', '2020-02-12 13:35:27'),
(7, 'testeeeee', '00.000.000/0000-00', 'exemplo@exemplo.com', '(00) 0000-0000', '(00) 0 0000-0000', 78, '2020-02-18 18:40:55', '2020-02-18 18:41:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `consultancies`
--

CREATE TABLE `consultancies` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `fk_clinics` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `consultancies`
--

INSERT INTO `consultancies` (`id`, `name`, `fk_clinics`, `create_at`, `update_at`) VALUES
(1, 'Consultorio 1', 6, '2020-02-18 14:49:56', '2020-02-27 17:40:36'),
(2, 'Colsultorio 2 ', 6, '2020-02-18 14:59:14', '2020-02-18 14:59:14');

-- --------------------------------------------------------

--
-- Estrutura da tabela `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `group`
--

CREATE TABLE `group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `group`
--

INSERT INTO `group` (`id`, `name`, `created_at`, `update_at`) VALUES
(1, 'Administração', '2020-02-17 16:04:23', '2020-02-17 16:04:23');

-- --------------------------------------------------------

--
-- Estrutura da tabela `health_insurance`
--

CREATE TABLE `health_insurance` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `site` varchar(255) DEFAULT NULL,
  `cnpj` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `tellphone_commercial` varchar(255) DEFAULT NULL,
  `tellphone_residential` varchar(255) DEFAULT NULL,
  `table_references` varchar(255) NOT NULL,
  `register_ans` varchar(255) NOT NULL,
  `fk_addresses` int(10) DEFAULT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `health_insurance`
--

INSERT INTO `health_insurance` (`id`, `name`, `email`, `site`, `cnpj`, `login`, `password`, `tellphone_commercial`, `tellphone_residential`, `table_references`, `register_ans`, `fk_addresses`, `create_at`, `update_at`) VALUES
(1, 'Bradescoo', 'ad@da.com', 'www', '', '', '', '(00) 0000-0000', '(00) 0000-0000', 'AC', '01135435', 1, '2020-02-17 20:07:36', '2020-03-06 18:43:11'),
(2, 'Itau saude', 'ex@ex.comm', 'www.exxample.com', '', '', '', '(00) 0000-0000', '(00) 0000-0000', 'AC', '00000000', 28, '2020-02-17 20:43:08', '2020-03-06 18:43:09'),
(3, 'name', 'ex@ex.com', 'www.exxample.com', '', '', '', '(00) 0000-0000', '(00) 0000-0000', 'AC', '00000000', 29, '2020-02-17 20:43:34', '2020-02-17 20:43:34'),
(4, 'bradesco', 'www@dasd.com', 'www.exxample.com', '', '', '', '(00) 0000-0000', '(00) 0000-0000', 'AC', 'dasdasds000', 79, '2020-02-18 18:43:39', '2020-02-20 20:58:36'),
(5, 'TESTEEEEEE', 'www@dasd.com', 'www.exxample.com', '00.000.000/0000-00', 'juim', 'Junior123', '(00) 0000-0000', '(00) 0000-0000', '', '2114545', 160, '2020-03-06 18:29:34', '2020-03-06 18:43:05');

-- --------------------------------------------------------

--
-- Estrutura da tabela `hospital`
--

CREATE TABLE `hospital` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `reason_social` varchar(255) NOT NULL,
  `cnpj` varchar(255) NOT NULL,
  `tellphone` varchar(255) NOT NULL,
  `fk_addresses` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `hospital`
--

INSERT INTO `hospital` (`id`, `name`, `email`, `reason_social`, `cnpj`, `tellphone`, `fk_addresses`, `create_at`, `update_at`) VALUES
(2, 'HGF', 'hospital@hgf.com', 'Hospital Geral', '00.000.000/0000-00', '(00) 0000-0000', 187, '2020-03-10 17:01:39', '2020-03-10 17:01:39');

-- --------------------------------------------------------

--
-- Estrutura da tabela `medics`
--

CREATE TABLE `medics` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `cpf` varchar(255) NOT NULL,
  `cnpj` varchar(255) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `rg` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `specialty` varchar(255) NOT NULL,
  `council_number` varchar(255) NOT NULL,
  `council_initials` varchar(255) NOT NULL,
  `council_uf` varchar(255) NOT NULL,
  `sexo` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tellphone` varchar(255) NOT NULL,
  `cellphone` varchar(255) NOT NULL,
  `fk_addresses` int(10) NOT NULL,
  `fk_clinics` int(10) NOT NULL,
  `fk_bank` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `medics`
--

INSERT INTO `medics` (`id`, `name`, `time`, `cpf`, `cnpj`, `reason`, `rg`, `birthday`, `specialty`, `council_number`, `council_initials`, `council_uf`, `sexo`, `email`, `tellphone`, `cellphone`, `fk_addresses`, `fk_clinics`, `fk_bank`, `created_at`, `updated_at`) VALUES
(4, 'Dr Junior Rabelo', '30', '082.647.633-32', '', '', '2009397820-0', '2001-01-02', 'Tester', '0800 0000-0', 'CRM', 'CE', 'Masculino', 'rabelojunior105@gmail.com', '(00) 0000-0000', '(00) 0 0000-0000', 1, 6, 0, '2020-02-14 15:06:58', '2020-02-17 19:25:36'),
(5, 'Dr Felipe', '30', '000.000.000-00', '', '', '0000000000-0', '2020-01-01', 'teste', '0000 0000-0', 'CRM', 'CE', 'Masculino', 'felipe@exemplo.com', '(00) 0000-0000', '(00) 0 0000-0000', 80, 6, 0, '2020-02-18 18:45:30', '2020-02-18 18:45:46'),
(6, 'Dr Junior', '30', '000.000.000-00', '', '', '0000000000-0', '2009-12-01', 'Ginecololgia', '0000 0000-0', 'CRM', 'CE', 'Masculino', 'rabelojunior105@gmail.com', '0000000000', '(00) 0 0000-0000', 156, 6, 0, '2020-03-03 17:31:23', '2020-03-03 17:31:23'),
(7, 'Dr T', '10', '000.000.000-00', '00.000.000/0000-00', 'dasda', '0000000000-0', '2020-12-31', '0', '0000 0000-0', 'CRM', 'AL', 'Masculino', 'esse@das.com', '(00) 0000-0000', '(00) 0 0000-0000', 185, 6, 10, '2020-03-09 17:16:27', '2020-03-09 17:16:27');

-- --------------------------------------------------------

--
-- Estrutura da tabela `office`
--

CREATE TABLE `office` (
  `id` int(11) NOT NULL,
  `dayWeek` varchar(255) NOT NULL,
  `timeStart` time NOT NULL,
  `timeEnd` time NOT NULL,
  `fk_medics` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `office`
--

INSERT INTO `office` (`id`, `dayWeek`, `timeStart`, `timeEnd`, `fk_medics`, `create_at`, `update_at`) VALUES
(13, 'Segunda', '10:00:00', '18:00:00', 4, '2020-02-14 15:06:58', '2020-02-14 15:06:58'),
(14, 'Sexta', '10:00:00', '18:00:00', 4, '2020-02-14 15:06:58', '2020-02-14 15:06:58'),
(15, 'Segunda', '06:00:00', '06:00:00', 4, '2020-02-17 19:21:52', '2020-02-17 19:21:52'),
(16, 'Domingo', '05:00:00', '05:00:00', 4, '2020-02-17 19:21:52', '2020-02-17 19:24:56'),
(17, 'Terça', '08:00:00', '20:00:00', 4, '2020-02-17 19:21:52', '2020-02-17 19:21:52'),
(18, 'Segunda', '08:00:00', '17:00:00', 5, '2020-02-18 18:45:30', '2020-02-18 18:45:30'),
(19, 'Terça', '08:00:00', '17:00:00', 5, '2020-02-18 18:45:30', '2020-02-18 18:45:30'),
(20, 'Segunda', '06:00:00', '18:00:00', 6, '2020-03-03 17:31:23', '2020-03-03 17:31:23'),
(21, 'Terça', '06:00:00', '18:00:00', 6, '2020-03-03 17:31:23', '2020-03-03 17:31:23');

-- --------------------------------------------------------

--
-- Estrutura da tabela `patients`
--

CREATE TABLE `patients` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `rg` varchar(255) DEFAULT NULL,
  `cpf` varchar(255) DEFAULT NULL,
  `emitting_organ` varchar(255) DEFAULT NULL,
  `sexo` varchar(255) DEFAULT NULL,
  `birth_place` varchar(255) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `groups_references` varchar(255) DEFAULT NULL,
  `situation` varchar(255) DEFAULT NULL,
  `specialty` varchar(255) DEFAULT NULL,
  `tellphone_commercial` varchar(255) DEFAULT NULL,
  `tellphone_residential` varchar(255) DEFAULT NULL,
  `name_message` varchar(255) DEFAULT NULL,
  `cellphone` varchar(255) DEFAULT NULL,
  `fk_addresses` int(10) DEFAULT NULL,
  `fk_medic` int(10) DEFAULT NULL,
  `fk_plan` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `patients`
--

INSERT INTO `patients` (`id`, `name`, `email`, `rg`, `cpf`, `emitting_organ`, `sexo`, `birth_place`, `birthday`, `groups_references`, `situation`, `specialty`, `tellphone_commercial`, `tellphone_residential`, `name_message`, `cellphone`, `fk_addresses`, `fk_medic`, `fk_plan`, `create_at`, `update_at`) VALUES
(4, 'Junior Rabelo', 'dasda@adsa.com', '0000000000-0', '000.000.000-00', 'SSP', 'dasda', 'dasda', '2020-01-01', 'dasda', 'dasda', 'dasda', '', '(00) 0000-0000', 'dasda', '(00) 0 0000-0000', 1, 4, 0, '2020-02-20 14:27:55', '2020-03-09 02:06:49'),
(8, 'Junioooor', 'rabe@junior.com', '0000000000-0', '000.000.000-00', 'sp', 'masculino', 'São paiulo', '1999-12-31', 'Definitivo', '1° Consulta', 'teste', '(00) 0000-0000', '(00) 0000-0000', 'dasda', '(00) 0 0000-0000', NULL, 4, 0, '2020-03-09 02:23:02', '2020-03-09 02:23:02'),
(9, 'Junior Rabelo', 'rabe@junior.com', '0000000000-0', '000.000.000-00', 'sp', 'masculino', 'São paiulo', '1999-12-31', 'Definitivo', '1° Consulta', 'teste', '(00) 0000-0000', '(00) 0000-0000', 'dasda', '(00) 0 0000-0000', 170, 4, 1, '2020-03-09 01:32:58', '2020-03-09 01:32:58'),
(10, 'Junior Rabelo', 'rabe@junior.com', '0000000000-0', '000.000.000-00', 'sp', 'masculino', 'São paiulo', '1999-12-31', 'Definitivo', '1° Consulta', 'teste', '(00) 0000-0000', '(00) 0000-0000', 'dasda', '(00) 0 0000-0000', 171, 4, 1, '2020-03-09 01:33:16', '2020-03-09 01:33:16'),
(11, 'Junior Rabelo', 'rabe@junior.com', '0000000000-0', '000.000.000-00', 'sp', 'masculino', 'São paiulo', '1999-12-31', 'Definitivo', '1° Consulta', 'teste', '(00) 0000-0000', '(00) 0000-0000', 'dasda', '(00) 0 0000-0000', 172, 4, 1, '2020-03-09 01:33:31', '2020-03-09 01:33:31'),
(12, 'Junior Rabelo', 'rabe@junior.com', '0000000000-0', '000.000.000-00', 'sp', 'masculino', 'São paiulo', '1999-12-31', 'Definitivo', '1° Consulta', 'teste', '(00) 0000-0000', '(00) 0000-0000', 'dasda', '(00) 0 0000-0000', 173, 4, 1, '2020-03-09 01:33:49', '2020-03-09 01:33:49'),
(13, 'Junior Rabelo', 'rabe@junior.com', '0000000000-0', '000.000.000-00', 'sp', 'masculino', 'São paiulo', '1999-12-31', 'Definitivo', '1° Consulta', 'teste', '(00) 0000-0000', '(00) 0000-0000', 'dasda', '(00) 0 0000-0000', 174, 4, 7, '2020-03-09 01:34:13', '2020-03-09 01:34:13'),
(14, 'Junior Rabelo', 'rabe@junior.com', '0000000000-0', '000.000.000-00', 'sp', 'masculino', 'São paiulo', '1999-12-31', 'Definitivo', '1° Consulta', 'teste', '(00) 0000-0000', '(00) 0000-0000', 'dasda', '(00) 0 0000-0000', 175, 4, 8, '2020-03-09 01:34:40', '2020-03-09 01:34:40'),
(16, 'dasda', 'dasda@adsa.com', '', '', '', NULL, '', '2000-01-01', NULL, NULL, NULL, '', '', '', '', 0, 4, 0, '2020-03-09 01:35:56', '2020-03-09 01:35:56'),
(17, 'Junior Rabelo', 'dasda@adsa.com', '', '', '', NULL, '', '2000-01-01', NULL, NULL, NULL, '', '', '', '', 0, 4, 0, '2020-03-09 01:36:30', '2020-03-09 01:36:30'),
(18, 'Junior Rabelo', 'rabe@junior.com', '0000000000-0', '000.000.000-00', 'EMISSOR', 'masculino', 'São paiulo', '1999-12-31', 'Definitivo', '1° Consulta', 'teste', '(00) 0000-0000', '(00) 0000-0000', 'dasda', '(00) 0 0000-0000', NULL, 4, 9, '2020-03-09 02:04:02', '2020-03-09 02:04:02'),
(19, 'Junior Rabelo', 'dasda@adsa.com', '0000000000-0', '000.000.000-00', 'EMISSOR', 'dasda', 'dasda', '2020-01-01', 'dasda', 'dasda', 'dasda', '(00) 0000-0000', '(00) 0000-0000', 'dasda', '(00) 0 0000-0000', NULL, 4, 10, '2020-03-09 02:04:15', '2020-03-09 02:04:15');

-- --------------------------------------------------------

--
-- Estrutura da tabela `plan`
--

CREATE TABLE `plan` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `accommodation` varchar(255) NOT NULL,
  `wallet` varchar(255) NOT NULL,
  `product` varchar(255) NOT NULL,
  `fk_health` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `plan`
--

INSERT INTO `plan` (`id`, `name`, `accommodation`, `wallet`, `product`, `fk_health`, `create_at`, `update_at`) VALUES
(1, 'Junior Rabelo', '', '', '', 1, '2020-03-09 01:27:14', '2020-03-09 02:23:02'),
(2, 'dasda', 'Enfermaria', '4524524', '245204', 1, '2020-03-09 01:31:59', '2020-03-09 01:31:59'),
(3, 'dasda', 'Enfermaria', '4524524', '245204', 1, '2020-03-09 01:32:58', '2020-03-09 01:32:58'),
(4, 'dasda', 'Enfermaria', '4524524', '245204', 1, '2020-03-09 01:33:16', '2020-03-09 01:33:16'),
(5, 'dasda', 'Enfermaria', '4524524', '245204', 1, '2020-03-09 01:33:31', '2020-03-09 01:33:31'),
(6, 'dasda', 'Enfermaria', '4524524', '245204', 1, '2020-03-09 01:33:48', '2020-03-09 01:33:48'),
(7, 'dasda', 'Enfermaria', '4524524', '245204', 1, '2020-03-09 01:34:13', '2020-03-09 01:34:13'),
(8, 'dasda', 'Enfermaria', '4524524', '245204', 1, '2020-03-09 01:34:39', '2020-03-09 01:34:39');

-- --------------------------------------------------------

--
-- Estrutura da tabela `procedures`
--

CREATE TABLE `procedures` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `hours` varchar(255) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `table_tuss` varchar(255) DEFAULT NULL,
  `situation` varchar(255) NOT NULL,
  `obs` varchar(255) NOT NULL,
  `dateSendDocs` varchar(255) NOT NULL,
  `material` varchar(255) NOT NULL,
  `indication` varchar(255) NOT NULL,
  `cid_10` varchar(255) NOT NULL,
  `type_charge` varchar(255) NOT NULL,
  `fk_team` tinyint(1) DEFAULT NULL,
  `fk_hospital` int(11) DEFAULT NULL,
  `fk_patient` int(11) DEFAULT NULL,
  `fk_medics` int(11) DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT current_timestamp(),
  `update_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `procedures`
--

INSERT INTO `procedures` (`id`, `name`, `value`, `date`, `hours`, `duration`, `table_tuss`, `situation`, `obs`, `dateSendDocs`, `material`, `indication`, `cid_10`, `type_charge`, `fk_team`, `fk_hospital`, `fk_patient`, `fk_medics`, `create_at`, `update_at`) VALUES
(21, 'Teste', 'R$ 28500', '2020-03-25', '03:00', '04:00', NULL, 'Operado', '                            gsdgfsd                        ', '2020-03-25', 'fsdfsd', 'sdfsdfdasda', 'sda153', 'Avista', 1, 2, 8, 4, '2020-03-25 15:54:44', '2020-03-25 18:45:33');

-- --------------------------------------------------------

--
-- Estrutura da tabela `scheduling`
--

CREATE TABLE `scheduling` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `tellphone` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `situation` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `fk_medic` int(11) NOT NULL,
  `fk_patients` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `scheduling`
--

INSERT INTO `scheduling` (`id`, `email`, `value`, `tellphone`, `type`, `reason`, `situation`, `color`, `start`, `end`, `fk_medic`, `fk_patients`, `create_at`, `update_at`) VALUES
(10, 'rabelojunior01@gmai.vdikn', 'R$ 100', '(00) 0 0000-0000', 'Confirmado', 'Consulta', 'Cliente Chegou', '#2BB31F', '2020-03-01 06:00:00', '2020-03-01 06:30:00', 5, 4, '2020-03-03 17:19:13', '2020-03-12 02:30:53'),
(11, 'rabelojunior01@gmai.vdikn', 'R$ 100', '(00) 0 0000-000', 'Consulta', 'dasda', 'Atendido', '#453EFF', '2020-03-02 06:30:00', '2020-03-02 07:00:00', 4, 4, '2020-03-05 18:55:33', '2020-03-12 02:30:45'),
(12, 'rabelojunior01@gmai.vdikn', 'R$ 100', '(00) 0 0000-000', 'Consulta', 'dasda', 'Atendido', '#453EFF', '2020-03-02 06:30:00', '2020-03-02 07:00:00', 4, 4, '2020-03-05 18:55:44', '2020-03-12 02:30:50'),
(13, 'rabelojunior01@gmai.vdikn', 'R$ 500', '(00) 0 0000-000', 'Retorno', 'dasda', 'Confirmado', '#93CC98', '2019-02-08 05:59:00', '2019-02-08 05:29:00', 5, 4, '2020-03-11 22:09:53', '2020-03-11 22:09:53'),
(14, 'rabelojunior01@gmai.vdikn', 'R$ 150', '(88) 9 8150-0134', 'Consulta', 'Estou me sentindo mal por tais aspectos', 'Atendido', '#453EFF', '2020-03-11 06:00:00', '2020-03-11 07:29:00', 5, 4, '2020-03-12 00:23:12', '2020-03-12 00:23:12');

-- --------------------------------------------------------

--
-- Estrutura da tabela `table_tuss`
--

CREATE TABLE `table_tuss` (
  `id` int(11) NOT NULL,
  `cod` varchar(255) NOT NULL,
  `group_tuss` varchar(255) NOT NULL,
  `sub_group` varchar(255) NOT NULL,
  `procedure_tuss` varchar(255) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `table_tuss`
--

INSERT INTO `table_tuss` (`id`, `cod`, `group_tuss`, `sub_group`, `procedure_tuss`, `create_at`, `update_at`) VALUES
(2, '      10101012   ', '          PROCEDIMENTOS GERAIS CONSULTAS          ', 'CONSULTA     ', 'CONSULTA EM CONSULTORIO (NO HORARIO NORMAL OU PRE ESTABELECIDO)     ', '2020-03-24 17:15:53', '2020-03-24 18:48:33'),
(3, '\n      10101039\n     ', 'PROCEDIMENTOS GERAIS CONSULTAS\n     ', 'CONSULTA\n     ', 'CONSULTA EM PRONTO SOCORRO\n     ', '2020-03-24 17:15:53', '2020-03-24 17:15:53'),
(4, '\n      10102019\n     ', 'PROCEDIMENTOS GERAIS CONSULTAS\n     ', 'VISITAS\n     ', 'VISITA HOSPITALAR (PACIENTE INTERNADO)\n     ', '2020-03-24 17:15:53', '2020-03-24 17:15:53'),
(5, '\n      30602017\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'BIOPSIA INCISIONAL DE MAMA\n     ', '2020-03-24 17:15:53', '2020-03-24 17:15:53'),
(6, '\n      30602025\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'COLETA DE FLUXO PAPILAR DE MAMA\n     ', '2020-03-24 17:15:53', '2020-03-24 17:15:53'),
(7, '\n      30602041\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'CORRECAO DE INVERSAO PAPILAR - UNILATERAL\n     ', '2020-03-24 17:15:53', '2020-03-24 17:15:53'),
(8, '\n      30602050\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'DRENAGEM DE ABSCESSO DE MAMA\n     ', '2020-03-24 17:15:53', '2020-03-24 17:15:53'),
(9, '\n      30602068\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'DRENAGEM E/OU ASPIRACAO DE SEROMA\n     ', '2020-03-24 17:15:53', '2020-03-24 17:15:53'),
(10, '\n      30602076\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'EXERESE DE LESAO DA MAMA POR MARCACAO ESTEREOTAXICA OU ROLL\n     ', '2020-03-24 17:15:53', '2020-03-24 17:15:53'),
(11, '\n      30602084\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'EXERESE DE MAMA SUPRA-NUMERARIA - UNILATERAL\n     ', '2020-03-24 17:15:53', '2020-03-24 17:15:53'),
(12, '\n      30602092\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'EXERESE DE NODULO\n     ', '2020-03-24 17:15:53', '2020-03-24 17:15:53'),
(13, '\n      30602106\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'FISTULECTOMIA DE MAMA\n     ', '2020-03-24 17:15:53', '2020-03-24 17:15:53'),
(14, '\n      30602114\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'GINECOMASTIA - UNILATERAL\n     ', '2020-03-24 17:15:53', '2020-03-24 17:15:53'),
(15, '\n      30602130\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'LINFADENECTOMIA AXILAR\n     ', '2020-03-24 17:15:53', '2020-03-24 17:15:53'),
(16, '\n      30602149\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'MASTECTOMIA RADICAL OU RADICAL MODIFICADA - QUALQUER TECNICA\n     ', '2020-03-24 17:15:53', '2020-03-24 17:15:53'),
(17, '\n      30602157\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'MASTECTOMIA SIMPLES\n     ', '2020-03-24 17:15:53', '2020-03-24 17:15:53'),
(18, '\n      30602165\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'MASTECTOMIA SUBCUTANEA E INCLUSAO DA PROTESE\n     ', '2020-03-24 17:15:53', '2020-03-24 17:15:53'),
(19, '\n      30602173\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'MASTOPLASTIA EM MAMA OPOSTA APOS RECONSTRUCAO DA CONTRALATERAL\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(20, '\n      30602181\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'PUNCAO OU BIOPSIA PERCUTANEA DE AGULHA FINA - POR NODULO (MAXIMO DE 3 NODULOS POR MAMA)\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(21, '\n      30602190\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'QUADRANTECTOMIA E LINFADENECTOMIA AXILAR\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(22, '\n      30602203\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'QUADRANTECTOMIA - RESSECCAO SEGMENTAR\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(23, '\n      30602211\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'RECONSTRUCAO DA PLACA AREOLO MAMILAR - UNILATERAL\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(24, '\n      30602238\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'RECONSTRUCAO MAMARIA COM RETALHO MUSCULAR OU MIOCUTANEO - UNILATERAL\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(25, '\n      30602246\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'RECONSTRUCAO MAMARIA COM RETALHOS CUTANEOS REGIONAIS\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(26, '\n      30602254\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'RECONSTRUCAO PARCIAL DA MAMA POS- QUADRANTECTOMIA\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(27, '\n      30602262\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'RECONSTRUCAO DA MAMA COM PROTESE E/OU EXPANSOR\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(28, '\n      30602289\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'RESSECCAO DO LINFONODO SENTINELA / TORACICA LATERAL\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(29, '\n      30602297\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'RESSECCAO DO LINFONODO SENTINELA / TORACICA MEDIAL\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(30, '\n      30602300\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'RESSECCAO DOS DUCTOS PRINCIPAIS DA MAMA - UNILATERAL\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(31, '\n      30602319\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'RETIRADA DA VALVULA APOS COLOCACAO DE EXPANSOR PERMANENTE\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(32, '\n      30602327\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'SUBSTITUICAO DE PROTESE\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(33, '\n      30602343\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'LINFADENECTOMIA POR INCISAO EXTRA-AXILAR\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(34, '\n      30602351\n     ', 'PROCED CIRURGICOS E INVASIVOS PAREDE TORACICA\n     ', 'MAMAS\n     ', 'MAMOPLASTIA\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(35, '\n      31001017\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'ATRESIA DE ESOFAGO COM FISTULA TRAQUEAL - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(36, '\n      31001025\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'ATRESIA DE ESOFAGO SEM FISTULA (DUPLA ESTOMIA) - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(37, '\n      31001033\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'AUTOTRANSPLANTE COM MICROCIRURGIA\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(38, '\n      31001041\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'ESOFAGECTOMIA DISTAL COM TORACOTOMIA\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(39, '\n      31001050\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'ESOFAGECTOMIA DISTAL SEM TORACOTOMIA\n     ', '2020-03-24 17:15:54', '2020-03-24 17:15:54'),
(40, '\n      31001068\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'ESOFAGOPLASTIA  COLOPLASTIA\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(41, '\n      31001076\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'ESOFAGOPLASTIA  GASTROPLASTIA\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(42, '\n      31001084\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'ESTENOSE DE ESOFAGO - TRATAMENTO CIRURGICO VIA TORACICA\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(43, '\n      31001092\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'FARINGO-LARINGO-ESOFAGECTOMIA TOTAL COM OU SEM TORACOTOMIA\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(44, '\n      31001106\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'FISTULA TRAQUEO ESOFAGICA - TRATAMENTO CIRURGICO VIA CERVICAL\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(45, '\n      31001114\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'FISTULA TRAQUEO ESOFAGICA - TRATAMENTO CIRURGICO VIA TORACICA\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(46, '\n      31001149\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'REINTERVENCAO SOBRE A TRANSICAO ESOFAGO GASTRICA\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(47, '\n      31001157\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'RESSECCAO DO ESOFAGO CERVICAL E/OU TORACICO E TRANSPLANTE COM MICROCIRURGIA\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(48, '\n      31001165\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'SUBSTITUICAO ESOFAGICA - COLON OU TUBO GASTRICO\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(49, '\n      31001173\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'TRATAMENTO CIRURGICO DAS VARIZES ESOFAGICAS\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(50, '\n      31001181\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'TRATAMENTO CIRURGICO CONSERVADOR DO MEGAESOFAGO\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(51, '\n      31001190\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'TUNELIZACAO ESOFAGICA\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(52, '\n      31001203\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'ESOFAGORRAFIA CERVICAL\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(53, '\n      31001211\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'ESOFAGORRAFIA TORACICA\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(54, '\n      31001220\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'ESOFAGOSTOMIA\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(55, '\n      31001238\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'TRATAMENTO CIRURGICO DO DIVERTICULO ESOFAGICO\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(56, '\n      31001246\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'TRATAMENTO CIRURGICO DO DIVERTICULO FARINGOESOFAGICO\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(57, '\n      31001254\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'ESOFAGECTOMIA SUBTOTAL COM LINFADENECTOMIA COM OU SEM TORACOTOMIA\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(58, '\n      31001262\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'REFLUXO GASTROESOFAGICO - TRATAMENTO CIRURGICO (HERNIA DE HIATO)\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(59, '\n      31001270\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'RECONSTRUCAO DO ESOFAGO CERVICAL E TORACICO COM TRANSPLANTE SEGMENTAR DE INTESTINO\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(60, '\n      31001289\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'RECONSTRUCAO DO ESOFAGO CERVICAL OU TORACICO, COM TRANSPLANTE DE INTESTINO\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(61, '\n      31001297\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'DISSECCAO DO ESOFAGO TORACICO  QUALQUER TECNICA\n     ', '2020-03-24 17:15:55', '2020-03-24 17:15:55'),
(62, '\n      31001300\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'ESOFAGECTOMIA DISTAL COM OU SEM TORACOTOMIA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:15:56', '2020-03-24 17:15:56'),
(63, '\n      31001319\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'REINTERVENCAO SOBRE A TRANSICAO ESOFAGO GASTRICA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:15:56', '2020-03-24 17:15:56'),
(64, '\n      31001327\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'TRATAMENTO CIRURGICO DAS VARIZES ESOFAGICAS POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:15:56', '2020-03-24 17:15:56'),
(65, '\n      31001335\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'TRATAMENTO CIRURGICO CONSERVADOR DO MEGAESOFAGO POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:15:56', '2020-03-24 17:15:56'),
(66, '\n      31001343\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'ESOFAGORRAFIA TORACICA POR VIDEOTORACOSCOPIA\n     ', '2020-03-24 17:15:56', '2020-03-24 17:15:56'),
(67, '\n      31001351\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'TRATAMENTO CIRURGICO DO DIVERTICULO ESOFAGICO POR VIDEOTORACOSCOPIA\n     ', '2020-03-24 17:15:56', '2020-03-24 17:15:56'),
(68, '\n      31001360\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESOFAGO\n     ', 'REFLUXO GASTROESOFAGICO - TRATAMENTO CIRURGICO HERNIA DE HIATO  POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:15:56', '2020-03-24 17:15:56'),
(69, '\n      31002013\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'COLOCACAO DE BANDA GASTRICA\n     ', '2020-03-24 17:15:56', '2020-03-24 17:15:56'),
(70, '\n      31002021\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'CONVERSAO DE ANASTOMOSE GASTROJEJUNAL (QUALQUER TECNICA)\n     ', '2020-03-24 17:15:56', '2020-03-24 17:15:56'),
(71, '\n      31002030\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'DEGASTROGASTRECTOMIA COM VAGOTOMIA\n     ', '2020-03-24 17:15:56', '2020-03-24 17:15:56'),
(72, '\n      31002048\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'DEGASTROGASTRECTOMIA SEM VAGOTOMIA\n     ', '2020-03-24 17:15:56', '2020-03-24 17:15:56'),
(73, '\n      31002056\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTROSTOMIA CONFECCAO / FECHAMENTO\n     ', '2020-03-24 17:15:56', '2020-03-24 17:15:56'),
(74, '\n      31002064\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTRECTOMIA PARCIAL COM LINFADENECTOMIA\n     ', '2020-03-24 17:15:56', '2020-03-24 17:15:56'),
(75, '\n      31002072\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTRECTOMIA PARCIAL COM VAGOTOMIA\n     ', '2020-03-24 17:15:56', '2020-03-24 17:15:56'),
(76, '\n      31002080\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTRECTOMIA PARCIAL SEM VAGOTOMIA\n     ', '2020-03-24 17:15:56', '2020-03-24 17:15:56'),
(77, '\n      31002099\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTRECTOMIA POLAR SUPERIOR COM RECONSTRUCAO JEJUNAL COM TORACOTOMIA\n     ', '2020-03-24 17:15:56', '2020-03-24 17:15:56'),
(78, '\n      31002102\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTRECTOMIA POLAR SUPERIOR COM RECONSTRUCAO JEJUNAL SEM TORACOTOMIA\n     ', '2020-03-24 17:15:56', '2020-03-24 17:15:56'),
(79, '\n      31002110\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTRECTOMIA TOTAL COM LINFADENECTOMIA\n     ', '2020-03-24 17:15:56', '2020-03-24 17:15:56'),
(80, '\n      31002129\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTRECTOMIA TOTAL VIA ABDOMINAL\n     ', '2020-03-24 17:15:57', '2020-03-24 17:15:57'),
(81, '\n      31002137\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTROENTEROANASTOMOSE\n     ', '2020-03-24 17:15:57', '2020-03-24 17:15:57'),
(82, '\n      31002145\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTRORRAFIA\n     ', '2020-03-24 17:15:57', '2020-03-24 17:15:57'),
(83, '\n      31002153\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTROTOMIA COM SUTURA DE VARIZES\n     ', '2020-03-24 17:15:57', '2020-03-24 17:15:57'),
(84, '\n      31002161\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTROTOMIA PARA RETIRADA DE CE OU LESAO ISOLADA\n     ', '2020-03-24 17:15:57', '2020-03-24 17:15:57'),
(85, '\n      31002170\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTROTOMIA PARA QUALQUER FINALIDADE\n     ', '2020-03-24 17:15:57', '2020-03-24 17:15:57'),
(86, '\n      31002188\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'MEMBRANA ANTRAL - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:15:57', '2020-03-24 17:15:57'),
(87, '\n      31002196\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'PILOROPLASTIA\n     ', '2020-03-24 17:15:57', '2020-03-24 17:15:57'),
(88, '\n      31002218\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTROPLASTIA PARA OBESIDADE MORBIDA - QUALQUER TECNICA\n     ', '2020-03-24 17:15:57', '2020-03-24 17:15:57'),
(89, '\n      31002242\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'TRATAMENTO CIRURGICO DAS VARIZES GASTRICAS\n     ', '2020-03-24 17:15:57', '2020-03-24 17:15:57'),
(90, '\n      31002250\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'VAGOTOMIA COM OPERACAO DE DRENAGEM\n     ', '2020-03-24 17:15:57', '2020-03-24 17:15:57'),
(91, '\n      31002269\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'VAGOTOMIA GASTRICA PROXIMAL OU SUPERSELETIVA COM DUODENOPLASTIA (OPERACAO DE DRENAGEM)\n     ', '2020-03-24 17:15:57', '2020-03-24 17:15:57'),
(92, '\n      31002277\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'VAGOTOMIA SUPERSELETIVA OU VAGOTOMIA GASTRICA PROXIMAL\n     ', '2020-03-24 17:15:57', '2020-03-24 17:15:57'),
(93, '\n      31002285\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'COLOCACAO DE BANDA GASTRICA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:15:57', '2020-03-24 17:15:57'),
(94, '\n      31002293\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'CONVERSAO DE ANASTOMOSE GASTROJEJUNAL POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:15:57', '2020-03-24 17:15:57'),
(95, '\n      31002307\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTRECTOMIA PARCIAL COM LINFADENECTOMIA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:15:57', '2020-03-24 17:15:57'),
(96, '\n      31002315\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTRECTOMIA PARCIAL COM VAGOTOMIA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:15:57', '2020-03-24 17:15:57'),
(97, '\n      31002323\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTRECTOMIA PARCIAL SEM VAGOTOMIA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:15:57', '2020-03-24 17:15:57'),
(98, '\n      31002331\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTRECTOMIA TOTAL COM LINFADENECTOMIA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:15:57', '2020-03-24 17:15:57'),
(99, '\n      31002340\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTRECTOMIA TOTAL VIA ABDOMINAL POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(100, '\n      31002358\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTROENTEROANASTOMOSE POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(101, '\n      31002366\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTROTOMIA PARA RETIRADA DE CE OU LESAO ISOLADA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(102, '\n      31002374\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'PILOROPLASTIA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(103, '\n      31002390\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'GASTROPLASTIA PARA OBESIDADE MORBIDA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(104, '\n      31002404\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'VAGOTOMIA GASTRICA PROXIMAL OU SUPERSELETIVA COM DUODENOPLASTIA  OPERACAO DE DRENAGEM  POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(105, '\n      31002412\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ESTOMAGO\n     ', 'VAGOTOMIA SUPERSELETIVA OU VAGOTOMIA GASTRICA PROXIMAL POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(106, '\n      31004016\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'ABSCESSO ANORRETAL - DRENAGEM\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(107, '\n      31004024\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'ABSCESSO ISQUIO-RETAL - DRENAGEM\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(108, '\n      31004032\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'CERCLAGEM ANAL\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(109, '\n      31004040\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'CORPO ESTRANHO DO RETO - RETIRADA\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(110, '\n      31004059\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'CRIPTECTOMIA (UNICA OU MULTIPLA)\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(111, '\n      31004067\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'DILATACAO DIGITAL OU INSTRUMENTAL DO ANUS E/OU DO RETO\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(112, '\n      31004075\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'ESFINCTEROPLASTIA ANAL (QUALQUER TECNICA)\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(113, '\n      31004083\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'ESTENOSE ANAL - TRATAMENTO CIRURGICO (QUALQUER TECNICA)\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(114, '\n      31004091\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'EXCISAO DE PLICOMA\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(115, '\n      31004105\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'FISSURECTOMIA COM OU SEM ESFINCTEROTOMIA\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(116, '\n      31004113\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'FISTULA RETO-VAGINAL E FISTULA ANAL EM FERRADURA - TRATAMENTO CIRURGICO VIA PERINEAL\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(117, '\n      31004121\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'FISTULECTOMIA ANAL EM DOIS TEMPOS\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(118, '\n      31004130\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'FISTULECTOMIA ANAL EM FERRADURA\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(119, '\n      31004148\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'FISTULECTOMIA ANAL EM UM TEMPO\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(120, '\n      31004156\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'FISTULECTOMIA ANORRETAL COM ABAIXAMENTO MUCOSO\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(121, '\n      31004164\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'FISTULECTOMIA PERINEAL\n     ', '2020-03-24 17:15:58', '2020-03-24 17:15:58'),
(122, '\n      31004180\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'HEMORROIDAS - LIGADURA ELASTICA (POR SESSAO)\n     ', '2020-03-24 17:15:59', '2020-03-24 17:15:59'),
(123, '\n      31004199\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'HEMORROIDAS - TRATAMENTO ESCLEROSANTE (POR SESSAO)\n     ', '2020-03-24 17:15:59', '2020-03-24 17:15:59'),
(124, '\n      31004202\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'HEMORROIDECTOMIA ABERTA OU FECHADA, COM OU SEM ESFINCTEROTOMIA\n     ', '2020-03-24 17:15:59', '2020-03-24 17:15:59'),
(125, '\n      31004210\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'LACERACAO ANORRETAL - TRATAMENTO CIRURGICO POR VIA PERINEAL\n     ', '2020-03-24 17:15:59', '2020-03-24 17:15:59'),
(126, '\n      31004229\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'LESAO ANAL - ELETROCAUTERIZACAO\n     ', '2020-03-24 17:15:59', '2020-03-24 17:15:59'),
(127, '\n      31004237\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'PAPILECTOMIA (UNICA OU MULTIPLA)\n     ', '2020-03-24 17:15:59', '2020-03-24 17:15:59'),
(128, '\n      31004245\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'POLIPO RETAL - RESSECCAO ENDOANAL\n     ', '2020-03-24 17:15:59', '2020-03-24 17:15:59'),
(129, '\n      31004253\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'PROLAPSO RETAL - ESCLEROSE (POR SESSAO)\n     ', '2020-03-24 17:15:59', '2020-03-24 17:15:59'),
(130, '\n      31004261\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'PROLAPSO RETAL - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:15:59', '2020-03-24 17:15:59'),
(131, '\n      31004270\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'RECONSTITUICAO DE ESFINCTER ANAL POR PLASTICA MUSCULAR (QUALQUER TECNICA)\n     ', '2020-03-24 17:15:59', '2020-03-24 17:15:59'),
(132, '\n      31004288\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'RECONSTRUCAO TOTAL ANOPERINEAL\n     ', '2020-03-24 17:15:59', '2020-03-24 17:15:59'),
(133, '\n      31004300\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'TRATAMENTO CIRURGICO DE RETOCELE (COLPOPERINEOPLASTIA POSTERIOR)\n     ', '2020-03-24 17:15:59', '2020-03-24 17:15:59'),
(134, '\n      31004318\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'TROMBOSE HEMORROIDARIA - EXERESE\n     ', '2020-03-24 17:15:59', '2020-03-24 17:15:59'),
(135, '\n      31004326\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'PRURIDO ANAL - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:15:59', '2020-03-24 17:15:59'),
(136, '\n      31004334\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'ESFINCTEROTOMIA - ANUS\n     ', '2020-03-24 17:15:59', '2020-03-24 17:15:59'),
(137, '\n      31004342\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ANUS\n     ', 'ANOPEXIA MECANICA COM GRAMPEADOR\n     ', '2020-03-24 17:15:59', '2020-03-24 17:15:59'),
(138, '\n      31005012\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'ABSCESSO HEPATICO - DRENAGEM CIRURGICA (ATE 3 FRAGMENTOS)\n     ', '2020-03-24 17:15:59', '2020-03-24 17:15:59'),
(139, '\n      31005020\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'ALCOOLIZACAO PERCUTANEA DIRIGIDA DE TUMOR HEPATICO\n     ', '2020-03-24 17:16:00', '2020-03-24 17:16:00'),
(140, '\n      31005039\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'ANASTOMOSE BILIODIGESTIVA INTRA-HEPATICA\n     ', '2020-03-24 17:16:00', '2020-03-24 17:16:00'),
(141, '\n      31005047\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'ATRESIA DE VIAS BILIARES - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:00', '2020-03-24 17:16:00'),
(142, '\n      31005063\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'BIOPSIA HEPATICA POR LAPAROTOMIA (ATE 3 FRAGMENTOS)\n     ', '2020-03-24 17:16:00', '2020-03-24 17:16:00'),
(143, '\n      31005071\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'BIOPSIA HEPATICA TRANSPARIETAL  ATE 3 FRAGMENTOS\n     ', '2020-03-24 17:16:00', '2020-03-24 17:16:00'),
(144, '\n      31005080\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'LAPAROTOMIA PARA IMPLANTACAO CIRURGICA DE CATETER ARTERIAL VISCERAL PARA QUIMIOTERAPIA\n     ', '2020-03-24 17:16:00', '2020-03-24 17:16:00'),
(145, '\n      31005098\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'CISTO DE COLEDOCO - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:00', '2020-03-24 17:16:00'),
(146, '\n      31005101\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'COLECISTECTOMIA COM COLANGIOGRAFIA\n     ', '2020-03-24 17:16:00', '2020-03-24 17:16:00'),
(147, '\n      31005110\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'COLECISTECTOMIA COM FISTULA BILIODIGESTIVA\n     ', '2020-03-24 17:16:00', '2020-03-24 17:16:00'),
(148, '\n      31005128\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'COLECISTECTOMIA SEM COLANGIOGRAFIA\n     ', '2020-03-24 17:16:00', '2020-03-24 17:16:00'),
(149, '\n      31005136\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'COLECISTOJEJUNOSTOMIA\n     ', '2020-03-24 17:16:00', '2020-03-24 17:16:00'),
(150, '\n      31005144\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'COLECISTOSTOMIA\n     ', '2020-03-24 17:16:00', '2020-03-24 17:16:00'),
(151, '\n      31005152\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'COLEDOCO OU HEPATICO-JEJUNOSTOMIA (QUALQUER TECNICA)\n     ', '2020-03-24 17:16:00', '2020-03-24 17:16:00'),
(152, '\n      31005160\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'COLEDOCO OU HEPATICOPLASTIA\n     ', '2020-03-24 17:16:00', '2020-03-24 17:16:00'),
(153, '\n      31005179\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'COLEDOCO-DUODENOSTOMIA\n     ', '2020-03-24 17:16:00', '2020-03-24 17:16:00'),
(154, '\n      31005187\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'COLEDOCOTOMIA OU COLEDOCOSTOMIA SEM COLECISTECTOMIA\n     ', '2020-03-24 17:16:01', '2020-03-24 17:16:01'),
(155, '\n      31005195\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'COLEDOCOSCOPIA INTRA-OPERATORIA\n     ', '2020-03-24 17:16:01', '2020-03-24 17:16:01'),
(156, '\n      31005209\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'DERIVACAO PORTO SISTEMICA\n     ', '2020-03-24 17:16:01', '2020-03-24 17:16:01'),
(157, '\n      31005217\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'DESCONEXAO AZIGOS - PORTAL COM ESPLENECTOMIA\n     ', '2020-03-24 17:16:01', '2020-03-24 17:16:01'),
(158, '\n      31005225\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'DESCONEXAO AZIGOS - PORTAL SEM ESPLENECTOMIA\n     ', '2020-03-24 17:16:01', '2020-03-24 17:16:01'),
(159, '\n      31005233\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'DESVASCULARIZACAO HEPATICA\n     ', '2020-03-24 17:16:01', '2020-03-24 17:16:01'),
(160, '\n      31005241\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'DRENAGEM BILIAR TRANS-HEPATICA\n     ', '2020-03-24 17:16:01', '2020-03-24 17:16:01'),
(161, '\n      31005250\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'ENUCLEACAO DE METASTASES HEPATICAS\n     ', '2020-03-24 17:16:01', '2020-03-24 17:16:01'),
(162, '\n      31005268\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'ENUCLEACAO DE METASTASES, POR METASTASE\n     ', '2020-03-24 17:16:01', '2020-03-24 17:16:01'),
(163, '\n      31005276\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'HEPATORRAFIA\n     ', '2020-03-24 17:16:01', '2020-03-24 17:16:01'),
(164, '\n      31005284\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'HEPATORRAFIA COMPLEXA COM LESAO DE ESTRUTURAS VASCULARES BILIARES\n     ', '2020-03-24 17:16:01', '2020-03-24 17:16:01'),
(165, '\n      31005292\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'LOBECTOMIA HEPATICA DIREITA\n     ', '2020-03-24 17:16:01', '2020-03-24 17:16:01'),
(166, '\n      31005306\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'LOBECTOMIA HEPATICA ESQUERDA\n     ', '2020-03-24 17:16:01', '2020-03-24 17:16:01'),
(167, '\n      31005314\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'PAPILOTOMIA TRANSDUODENAL\n     ', '2020-03-24 17:16:01', '2020-03-24 17:16:01'),
(168, '\n      31005322\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'PUNCAO HEPATICA PARA DRENAGEM DE ABSCESSOS\n     ', '2020-03-24 17:16:01', '2020-03-24 17:16:01'),
(169, '\n      31005330\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'RADIOABLACAO / TERMOABLACAO DE TUMORES HEPATICOS\n     ', '2020-03-24 17:16:02', '2020-03-24 17:16:02'),
(170, '\n      31005357\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'RESSECCAO DE CISTO HEPATICO COM HEPATECTOMIA\n     ', '2020-03-24 17:16:02', '2020-03-24 17:16:02'),
(171, '\n      31005365\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'RESSECCAO DE CISTO HEPATICO SEM HEPATECTOMIA\n     ', '2020-03-24 17:16:02', '2020-03-24 17:16:02'),
(172, '\n      31005373\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'RESSECCAO DE TUMOR DE VESICULA OU DA VIA BILIAR COM HEPATECTOMIA\n     ', '2020-03-24 17:16:02', '2020-03-24 17:16:02'),
(173, '\n      31005381\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'RESSECCAO DE TUMOR DE VESICULA OU DA VIA BILIAR SEM HEPATECTOMIA\n     ', '2020-03-24 17:16:02', '2020-03-24 17:16:02'),
(174, '\n      31005390\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'SEGMENTECTOMIA HEPATICA\n     ', '2020-03-24 17:16:02', '2020-03-24 17:16:02'),
(175, '\n      31005403\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'SEQUESTRECTOMIA HEPATICA\n     ', '2020-03-24 17:16:02', '2020-03-24 17:16:02'),
(176, '\n      31005420\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'TRATAMENTO CIRURGICO DE ESTENOSE CICATRICIAL DAS VIAS BILIARES\n     ', '2020-03-24 17:16:02', '2020-03-24 17:16:02'),
(177, '\n      31005438\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'TRISSEGMENTECTOMIAS\n     ', '2020-03-24 17:16:02', '2020-03-24 17:16:02'),
(178, '\n      31005446\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'COLEDOCOTOMIA OU COLEDOCOSTOMIA COM COLECISTECTOMIA\n     ', '2020-03-24 17:16:02', '2020-03-24 17:16:02'),
(179, '\n      31005454\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'ABSCESSO HEPATICO - DRENAGEM CIRURGICA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:02', '2020-03-24 17:16:02'),
(180, '\n      31005462\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'ALCOOLIZACAO PERCUTANEA DIRIGIDA DE TUMOR HEPATICO POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:02', '2020-03-24 17:16:02'),
(181, '\n      31005470\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'COLECISTECTOMIA COM COLANGIOGRAFIA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:02', '2020-03-24 17:16:02'),
(182, '\n      31005489\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'COLECISTECTOMIA COM FISTULA BILIODIGESTIVA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:02', '2020-03-24 17:16:02'),
(183, '\n      31005497\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'COLECISTECTOMIA SEM COLANGIOGRAFIA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:02', '2020-03-24 17:16:02'),
(184, '\n      31005500\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'COLECISTOJEJUNOSTOMIA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:02', '2020-03-24 17:16:02'),
(185, '\n      31005519\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'COLECISTOSTOMIA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:02', '2020-03-24 17:16:02'),
(186, '\n      31005527\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'COLEDOCO OU HEPATICO-JEJUNOSTOMIA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:02', '2020-03-24 17:16:02'),
(187, '\n      31005535\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'COLEDOCO-DUODENOSTOMIA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:02', '2020-03-24 17:16:02'),
(188, '\n      31005543\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'COLEDOCOTOMIA OU COLEDOCOSTOMIA COM COLECISTECTOMIA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:03', '2020-03-24 17:16:03'),
(189, '\n      31005551\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'COLEDOCOTOMIA OU COLEDOCOSTOMIA SEM COLECISTECTOMIA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:03', '2020-03-24 17:16:03'),
(190, '\n      31005560\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'DESCONEXAO AZIGOS - PORTAL COM ESPLENECTOMIA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:03', '2020-03-24 17:16:03'),
(191, '\n      31005578\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'DESCONEXAO AZIGOS - PORTAL SEM ESPLENECTOMIA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:03', '2020-03-24 17:16:03'),
(192, '\n      31005586\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'ENUCLEACAO DE METASTASE HEPATICAS POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:03', '2020-03-24 17:16:03'),
(193, '\n      31005594\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'HEPATORRAFIA COMPLEXA COM LESAO DE ESTRUTURAS VASCULARES BILIARES POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:03', '2020-03-24 17:16:03'),
(194, '\n      31005608\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'HEPATORRAFIA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:03', '2020-03-24 17:16:03'),
(195, '\n      31005616\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'LOBECTOMIA HEPATICA DIREITA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:03', '2020-03-24 17:16:03'),
(196, '\n      31005624\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'LOBECTOMIA HEPATICA ESQUERDA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:03', '2020-03-24 17:16:03'),
(197, '\n      31005632\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'PUNCAO HEPATICA PARA DRENAGEM DE ABCESSOS POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:03', '2020-03-24 17:16:03'),
(198, '\n      31005640\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'RADIOABLACAO / TERMOABLACAO DE TUMORES HEPATICOS POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:03', '2020-03-24 17:16:03'),
(199, '\n      31005659\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'RESSECCAO DE CISTO HEPATICO COM HEPATECTOMIA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:03', '2020-03-24 17:16:03'),
(200, '\n      31005667\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'RESSECCAO DE CISTO HEPATICO SEM HEPATECTOMIA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:03', '2020-03-24 17:16:03'),
(201, '\n      31005675\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'BIOPSIA HEPATICA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:03', '2020-03-24 17:16:03'),
(202, '\n      31005683\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'BIOPSIA HEPATICA POR LAPAROTOMIA (ACIMA DE 3 FRAGMENTOS)\n     ', '2020-03-24 17:16:03', '2020-03-24 17:16:03'),
(203, '\n      31005691\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'FIGADO E VIAS BILIARES\n     ', 'BIOPSIA HEPATICA TRANSPARIETAL (ACIMA DE 3 FRAGMENTOS)\n     ', '2020-03-24 17:16:03', '2020-03-24 17:16:03'),
(204, '\n      31008054\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'PERITONIO\n     ', 'EPIPLOPLASTIA\n     ', '2020-03-24 17:16:03', '2020-03-24 17:16:03'),
(205, '\n      31008062\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'PERITONIO\n     ', 'IMPLANTE DE CATETER PERITONEAL\n     ', '2020-03-24 17:16:03', '2020-03-24 17:16:03'),
(206, '\n      31008100\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'PERITONIO\n     ', 'EPIPLOPLASTIA POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:04', '2020-03-24 17:16:04'),
(207, '\n      31009018\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'ABSCESSO PERINEAL - DRENAGEM CIRURGICA\n     ', '2020-03-24 17:16:04', '2020-03-24 17:16:04'),
(208, '\n      31009026\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'BIOPSIA DE PAREDE ABDOMINAL\n     ', '2020-03-24 17:16:04', '2020-03-24 17:16:04'),
(209, '\n      31009042\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'CISTO SACRO-COCCIGEO - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:04', '2020-03-24 17:16:04'),
(210, '\n      31009050\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'DIASTASE DOS RETOS-ABDOMINAIS - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:04', '2020-03-24 17:16:04'),
(211, '\n      31009069\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'HERNIA INGUINAL ENCARCERADA EM RN OU LACTENTE - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:04', '2020-03-24 17:16:04'),
(212, '\n      31009077\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'HERNIORRAFIA COM RESSECCAO INTESTINAL - ESTRANGULADA\n     ', '2020-03-24 17:16:04', '2020-03-24 17:16:04'),
(213, '\n      31009085\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'HERNIORRAFIA CRURAL - UNILATERAL\n     ', '2020-03-24 17:16:04', '2020-03-24 17:16:04'),
(214, '\n      31009093\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'HERNIORRAFIA EPIGASTRICA\n     ', '2020-03-24 17:16:04', '2020-03-24 17:16:04'),
(215, '\n      31009107\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'HERNIORRAFIA INCISIONAL\n     ', '2020-03-24 17:16:04', '2020-03-24 17:16:04'),
(216, '\n      31009115\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'HERNIORRAFIA INGUINAL - UNILATERAL\n     ', '2020-03-24 17:16:04', '2020-03-24 17:16:04'),
(217, '\n      31009123\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'HERNIORRAFIA INGUINAL NO RN OU LACTENTE\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(218, '\n      31009131\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'HERNIORRAFIA LOMBAR\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(219, '\n      31009140\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'HERNIORRAFIA RECIDIVANTE\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(220, '\n      31009158\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'HERNIORRAFIA SEM RESSECCAO INTESTINAL ENCARCERADA\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(221, '\n      31009166\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'HERNIORRAFIA UMBILICAL\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(222, '\n      31009174\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'LAPAROTOMIA EXPLORADORA, OU PARA BIOPSIA, OU PARA DRENAGEM DE ABSCESSO, OU PARA LIBERACAO DE BRIDAS EM VIGENCIA DE OCLUSAO\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(223, '\n      31009204\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'NEUROBLASTOMA ABDOMINAL - EXERESE\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(224, '\n      31009220\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'ONFALOCELE/GASTROSQUISE EM 1 TEMPO OU PRIMEIRO TEMPO OU PROTESE - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(225, '\n      31009239\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'ONFALOCELE/GASTROSQUISE - SEGUNDO TEMPO - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(226, '\n      31009247\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'PARACENTESE ABDOMINAL\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(227, '\n      31009255\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'RECONSTRUCAO DA PAREDE ABDOMINAL COM RETALHO MUSCULAR OU MIOCUTANEO\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(228, '\n      31009263\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'REPARACAO DE OUTRAS HERNIAS (INCLUI HERNIORRAFIA MUSCULAR)\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(229, '\n      31009271\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'RESSECCAO DE CISTO OU FISTULA DE URACO\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(230, '\n      31009280\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'RESSECCAO DE CISTO OU FISTULA OU RESTOS DO DUCTO ONFALOMESENTERICO\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(231, '\n      31009298\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'RESSUTURA DA PAREDE ABDOMINAL (POR DEISCENCIA TOTAL OU EVISCERACAO)\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(232, '\n      31009301\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'TERATOMA SACRO-COCCIGEO - EXERESE\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05');
INSERT INTO `table_tuss` (`id`, `cod`, `group_tuss`, `sub_group`, `procedure_tuss`, `create_at`, `update_at`) VALUES
(233, '\n      31009310\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'HERNIORRAFIA COM RESSECCAO INTESTINAL - ESTRANGULADA - POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(234, '\n      31009328\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'HERNIORRAFIA CRURAL - UNILATERAL POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(235, '\n      31009336\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'HERNIORRAFIA INGUINAL - UNILATERAL POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(236, '\n      31009344\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'HERNIORRAFIA RECIDIVANTE POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(237, '\n      31009352\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'LAPAROTOMIA EXPLORADORA, OU PARA BIOPSIA, OU PARA DRENAGEM DE ABSCESSO, OU PARA LIBERACAO DE BRIDAS EM VIGENCIA DE OCLUSAO POR VIDEOLAPAROSCOPIA\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(238, '\n      31009360\n     ', 'PROCED CIR E INVASIVOS  SIST DIGESTIVO E ANEXOS\n     ', 'ABDOME, PAREDE E CAVIDADE\n     ', 'HERNIORRAFIA INGUINAL EM CRIANCA - UNILATERAL\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(239, '\n      31102018\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'BIOPSIA CIRURGICA DE URETER UNILATERAL\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(240, '\n      31102026\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'BIOPSIA ENDOSCOPICA DE URETER UNILATERAL\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(241, '\n      31102034\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'CATETERISMO URETERAL UNILATERAL\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(242, '\n      31102042\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'COLOCACAO CIRURGICA DE DUPLO J UNILATERAL\n     ', '2020-03-24 17:16:05', '2020-03-24 17:16:05'),
(243, '\n      31102050\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'COLOCACAO CISTOSCOPICA DE DUPLO J UNILATERAL\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(244, '\n      31102069\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'COLOCACAO NEFROSCOPICA DE DUPLO J UNILATERAL\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(245, '\n      31102077\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'COLOCACAO URETEROSCOPICA DE DUPLO J - UNILATERAL\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(246, '\n      31102085\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'DILATACAO ENDOSCOPICA UNILATERAL\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(247, '\n      31102093\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'DUPLICACAO PIELOURETERAL - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(248, '\n      31102107\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'FISTULA URETERO-CUTANEA UNILATERAL (TRATAMENTO CIRURGICO)\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(249, '\n      31102115\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'FISTULA URETERO-INTESTINAL UNILATERAL (TRATAMENTO CIRURGICO)\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(250, '\n      31102123\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'FISTULA URETERO-VAGINAL UNILATERAL (TRATAMENTO CIRURGICO)\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(251, '\n      31102131\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'MEATOTOMIA ENDOSCOPICA UNILATERAL\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(252, '\n      31102174\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'REIMPLANTE URETEROINTESTINAL - UNI OU BILATERAL\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(253, '\n      31102182\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'REIMPLANTE URETERAL POR VIA EXTRA OU INTRAVESICAL - UNILATERAL\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(254, '\n      31102204\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'REIMPLANTE URETERO-VESICAL UNILATERAL - VIA COMBINADA\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(255, '\n      31102220\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'RETIRADA ENDOSCOPICA DE CALCULO DE URETER - UNILATERAL\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(256, '\n      31102239\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'TRANSURETEROSTOMIA\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(257, '\n      31102247\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETERECTOMIA UNILATERAL\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(258, '\n      31102255\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROCELE UNILATERAL - RESSECCAO A CEU ABERTO\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(259, '\n      31102263\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROCELES - TRATAMENTO ENDOSCOPICO\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(260, '\n      31102271\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROILEOCISTOSTOMIA UNILATERAL\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(261, '\n      31102280\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROILEOSTOMIA CUTANEA UNILATERAL\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(262, '\n      31102298\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROLISE UNILATERAL\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(263, '\n      31102301\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROLITOTOMIA UNILATERAL\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(264, '\n      31102310\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROLITOTRIPSIA EXTRACORPOREA - 1A SESSAO\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(265, '\n      31102328\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROLITOTRIPSIA EXTRACORPOREA - REAPLICACOES ATE 3 MESES\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(266, '\n      31102344\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROPLASTIA UNILATERAL\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(267, '\n      31102352\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETERORRENOLITOTOMIA UNILATERAL\n     ', '2020-03-24 17:16:06', '2020-03-24 17:16:06'),
(268, '\n      31102360\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETERORRENOLITOTRIPSIA FLEXIVEL A LASER UNILATERAL\n     ', '2020-03-24 17:16:07', '2020-03-24 17:16:07'),
(269, '\n      31102379\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETERORRENOLITOTRIPSIA RIGIDA UNILATERAL\n     ', '2020-03-24 17:16:07', '2020-03-24 17:16:07'),
(270, '\n      31102409\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROSSIGMOIDOPLASTIA UNILATERAL\n     ', '2020-03-24 17:16:07', '2020-03-24 17:16:07'),
(271, '\n      31102417\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROSSIGMOIDOSTOMIA UNILATERAL\n     ', '2020-03-24 17:16:07', '2020-03-24 17:16:07'),
(272, '\n      31102425\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROSTOMIA CUTANEA UNILATERAL\n     ', '2020-03-24 17:16:07', '2020-03-24 17:16:07'),
(273, '\n      31102433\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROTOMIA INTERNA PERCUTANEA UNILATERAL\n     ', '2020-03-24 17:16:07', '2020-03-24 17:16:07'),
(274, '\n      31102441\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROTOMIA INTERNA URETEROSCOPICA FLEXIVEL UNILATERAL\n     ', '2020-03-24 17:16:07', '2020-03-24 17:16:07'),
(275, '\n      31102450\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROTOMIA INTERNA URETEROSCOPICA RIGIDA UNILATERAL\n     ', '2020-03-24 17:16:07', '2020-03-24 17:16:07'),
(276, '\n      31102468\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROURETEROCISTONEOSTOMIA\n     ', '2020-03-24 17:16:07', '2020-03-24 17:16:07'),
(277, '\n      31102476\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROURETEROSTOMIA UNILATERAL\n     ', '2020-03-24 17:16:07', '2020-03-24 17:16:07'),
(278, '\n      31102492\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROLITOTOMIA LAPAROSCOPICA UNILATERAL\n     ', '2020-03-24 17:16:07', '2020-03-24 17:16:07'),
(279, '\n      31102506\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROLISE LAPAROSCOPICA UNILATERAL\n     ', '2020-03-24 17:16:07', '2020-03-24 17:16:07'),
(280, '\n      31102514\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROURETEROSTOMIA LAPAROSCOPICA UNILATERAL\n     ', '2020-03-24 17:16:07', '2020-03-24 17:16:07'),
(281, '\n      31102522\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETEROPLASTIA LAPAROSCOPICA UNILATERAL\n     ', '2020-03-24 17:16:07', '2020-03-24 17:16:07'),
(282, '\n      31102530\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'CORRECAO LAPAROSCOPICA DE REFLUXO VESICO- URETERAL UNILATERAL\n     ', '2020-03-24 17:16:07', '2020-03-24 17:16:07'),
(283, '\n      31102549\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'REIMPLANTE URETERO-VESICAL LAPAROSCOPICO UNILATERAL\n     ', '2020-03-24 17:16:07', '2020-03-24 17:16:07'),
(284, '\n      31102557\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'REIMPLANTE URETEROINTESTINAL LAPAROSCOPICO UNILATERAL\n     ', '2020-03-24 17:16:07', '2020-03-24 17:16:07'),
(285, '\n      31102565\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETER\n     ', 'URETERORRENOLITOTRIPSIA RIGIDA UNILATERAL A LASER\n     ', '2020-03-24 17:16:07', '2020-03-24 17:16:07'),
(286, '\n      31103014\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'AMPLIACAO VESICAL\n     ', '2020-03-24 17:16:07', '2020-03-24 17:16:07'),
(287, '\n      31103022\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'BEXIGA PSOICA - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:07', '2020-03-24 17:16:07'),
(288, '\n      31103030\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'BIOPSIA ENDOSCOPICA DE BEXIGA  INCLUI CISTOSCOPIA\n     ', '2020-03-24 17:16:08', '2020-03-24 17:16:08'),
(289, '\n      31103049\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'BIOPSIA VESICAL A CEU ABERTO\n     ', '2020-03-24 17:16:08', '2020-03-24 17:16:08'),
(290, '\n      31103057\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'CALCULO VESICAL - EXTRACAO ENDOSCOPICA\n     ', '2020-03-24 17:16:08', '2020-03-24 17:16:08'),
(291, '\n      31103065\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'CISTECTOMIA PARCIAL\n     ', '2020-03-24 17:16:08', '2020-03-24 17:16:08'),
(292, '\n      31103073\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'CISTECTOMIA RADICAL (INCLUI PROSTATA OU UTERO)\n     ', '2020-03-24 17:16:08', '2020-03-24 17:16:08'),
(293, '\n      31103081\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'CISTECTOMIA TOTAL\n     ', '2020-03-24 17:16:08', '2020-03-24 17:16:08'),
(294, '\n      31103090\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'CISTOLITOTOMIA\n     ', '2020-03-24 17:16:08', '2020-03-24 17:16:08'),
(295, '\n      31103103\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'CISTOLITOTRIPSIA EXTRACORPOREA - 1A SESSAO\n     ', '2020-03-24 17:16:08', '2020-03-24 17:16:08'),
(296, '\n      31103111\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'CISTOLITOTRIPSIA EXTRACORPOREA - REAPLICACOES (ATE 3 MESES)\n     ', '2020-03-24 17:16:08', '2020-03-24 17:16:08'),
(297, '\n      31103138\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'CISTOLITOTRIPSIA PERCUTANEA (U.S., E.H., E.C.)\n     ', '2020-03-24 17:16:08', '2020-03-24 17:16:08'),
(298, '\n      31103146\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'CISTOLITOTRIPSIA TRANSURETRAL  U.S., E.H., E.C.\n     ', '2020-03-24 17:16:08', '2020-03-24 17:16:08'),
(299, '\n      31103154\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'CISTOPLASTIA REDUTORA\n     ', '2020-03-24 17:16:08', '2020-03-24 17:16:08'),
(300, '\n      31103162\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'CISTORRAFIA (TRAUMA)\n     ', '2020-03-24 17:16:08', '2020-03-24 17:16:08'),
(301, '\n      31103170\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'CISTOSTOMIA CIRURGICA\n     ', '2020-03-24 17:16:09', '2020-03-24 17:16:09'),
(302, '\n      31103189\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'CISTOSTOMIA COM PROCEDIMENTO ENDOSCOPICO\n     ', '2020-03-24 17:16:09', '2020-03-24 17:16:09'),
(303, '\n      31103197\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'CISTOSTOMIA POR PUNCAO COM TROCATER\n     ', '2020-03-24 17:16:09', '2020-03-24 17:16:09'),
(304, '\n      31103200\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'COLO DE DIVERTICULO - RESSECCAO ENDOSCOPICA\n     ', '2020-03-24 17:16:09', '2020-03-24 17:16:09'),
(305, '\n      31103219\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'COLO VESICAL - RESSECCAO ENDOSCOPICA\n     ', '2020-03-24 17:16:09', '2020-03-24 17:16:09'),
(306, '\n      31103227\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'CORPO ESTRANHO - EXTRACAO CIRURGICA\n     ', '2020-03-24 17:16:09', '2020-03-24 17:16:09'),
(307, '\n      31103235\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'CORPO ESTRANHO - EXTRACAO ENDOSCOPICA\n     ', '2020-03-24 17:16:09', '2020-03-24 17:16:09'),
(308, '\n      31103243\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'DIVERTICULECTOMIA VESICAL\n     ', '2020-03-24 17:16:09', '2020-03-24 17:16:09'),
(309, '\n      31103251\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'ENTEROCISTOPLASTIA (AMPLIACAO VESICAL)\n     ', '2020-03-24 17:16:09', '2020-03-24 17:16:09'),
(310, '\n      31103260\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'EXTROFIA EM CLOACA - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:09', '2020-03-24 17:16:09'),
(311, '\n      31103278\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'EXTROFIA VESICAL - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:09', '2020-03-24 17:16:09'),
(312, '\n      31103286\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'FISTULA VESICO-CUTANEA - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:09', '2020-03-24 17:16:09'),
(313, '\n      31103294\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'FISTULA VESICO-ENTERICA - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:09', '2020-03-24 17:16:09'),
(314, '\n      31103308\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'FISTULA VESICO-RETAL - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:09', '2020-03-24 17:16:09'),
(315, '\n      31103316\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'FISTULA VESICO-UTERINA - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:09', '2020-03-24 17:16:09'),
(316, '\n      31103324\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'FISTULA VESICO-VAGINAL - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:10', '2020-03-24 17:16:10'),
(317, '\n      31103332\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'INCONTINENCIA URINARIA - SLING VAGINAL OU ABDOMINAL\n     ', '2020-03-24 17:16:10', '2020-03-24 17:16:10'),
(318, '\n      31103340\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'INCONTINENCIA URINARIA - SUSPENSAO ENDOSCOPICA DE COLO\n     ', '2020-03-24 17:16:10', '2020-03-24 17:16:10'),
(319, '\n      31103359\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'INCONTINENCIA URINARIA - TRATAMENTO CIRURGICO SUPRA- PUBICO\n     ', '2020-03-24 17:16:10', '2020-03-24 17:16:10'),
(320, '\n      31103367\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'INCONTINENCIA URINARIA - TRATAMENTO ENDOSCOPICO (INJECAO)\n     ', '2020-03-24 17:16:10', '2020-03-24 17:16:10'),
(321, '\n      31103375\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'INCONTINENCIA URINARIA COM COLPOPLASTIA ANTERIOR - TRATAMENTO CIRURGICO (COM OU SEM USO DE PROTESE)\n     ', '2020-03-24 17:16:10', '2020-03-24 17:16:10'),
(322, '\n      31103383\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'POLIPOS VESICAIS - RESSECCAO CIRURGICA\n     ', '2020-03-24 17:16:10', '2020-03-24 17:16:10'),
(323, '\n      31103391\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'POLIPOS VESICAIS - RESSECCAO ENDOSCOPICA\n     ', '2020-03-24 17:16:10', '2020-03-24 17:16:10'),
(324, '\n      31103405\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'PUNCAO E ASPIRACAO VESICAL\n     ', '2020-03-24 17:16:10', '2020-03-24 17:16:10'),
(325, '\n      31103413\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'REIMPLANTE URETERO-VESICAL A BOARI\n     ', '2020-03-24 17:16:10', '2020-03-24 17:16:10'),
(326, '\n      31103430\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'RETENCAO POR COAGULO - ASPIRACAO VESICAL\n     ', '2020-03-24 17:16:11', '2020-03-24 17:16:11'),
(327, '\n      31103448\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'TUMOR VESICAL - FOTOCOAGULACAO A LASER\n     ', '2020-03-24 17:16:11', '2020-03-24 17:16:11'),
(328, '\n      31103456\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'TUMOR VESICAL - RESSECCAO ENDOSCOPICA\n     ', '2020-03-24 17:16:11', '2020-03-24 17:16:11'),
(329, '\n      31103464\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'VESICOSTOMIA CUTANEA\n     ', '2020-03-24 17:16:11', '2020-03-24 17:16:11'),
(330, '\n      31103472\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'RETIRADA ENDOSCOPICA DE DUPLO J\n     ', '2020-03-24 17:16:11', '2020-03-24 17:16:11'),
(331, '\n      31103480\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'NEOBEXIGA CUTANEA CONTINENTE\n     ', '2020-03-24 17:16:11', '2020-03-24 17:16:11'),
(332, '\n      31103499\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'NEOBEXIGA RETAL CONTINENTE\n     ', '2020-03-24 17:16:11', '2020-03-24 17:16:11'),
(333, '\n      31103502\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'NEOBEXIGA URETRAL CONTINENTE\n     ', '2020-03-24 17:16:11', '2020-03-24 17:16:11'),
(334, '\n      31103510\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'CORRECAO LAPAROSCOPICA DE INCONTINENCIA URINARIA\n     ', '2020-03-24 17:16:11', '2020-03-24 17:16:11'),
(335, '\n      31103529\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'CISTECTOMIA PARCIAL LAPAROSCOPICA\n     ', '2020-03-24 17:16:11', '2020-03-24 17:16:11'),
(336, '\n      31103537\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'CISTECTOMIA RADICAL LAPAROSCOPICA  INCLUI PROSTATA OU UTERO\n     ', '2020-03-24 17:16:11', '2020-03-24 17:16:11'),
(337, '\n      31103545\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'NEOBEXIGA LAPAROSCOPICA\n     ', '2020-03-24 17:16:11', '2020-03-24 17:16:11'),
(338, '\n      31103553\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'DIVERTICULECTOMIA VESICAL LAPAROSCOPICA\n     ', '2020-03-24 17:16:11', '2020-03-24 17:16:11'),
(339, '\n      31103561\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'CISTOLITOTRIPSIA A LASER\n     ', '2020-03-24 17:16:11', '2020-03-24 17:16:11'),
(340, '\n      31103570\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'BEXIGA\n     ', 'COLO VESICAL - RESSECCAO CIRURGICA\n     ', '2020-03-24 17:16:11', '2020-03-24 17:16:11'),
(341, '\n      31104010\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'ABSCESSO PERIURETRAL - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:11', '2020-03-24 17:16:11'),
(342, '\n      31104029\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'BIOPSIA ENDOSCOPICA DE URETRA\n     ', '2020-03-24 17:16:11', '2020-03-24 17:16:11'),
(343, '\n      31104037\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'CORPO ESTRANHO OU CALCULO - EXTRACAO CIRURGICA\n     ', '2020-03-24 17:16:12', '2020-03-24 17:16:12'),
(344, '\n      31104045\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'CORPO ESTRANHO OU CALCULO - EXTRACAO ENDOSCOPICA\n     ', '2020-03-24 17:16:12', '2020-03-24 17:16:12'),
(345, '\n      31104053\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'DIVERTICULO URETRAL - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:12', '2020-03-24 17:16:12'),
(346, '\n      31104061\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'ELETROCOAGULACAO ENDOSCOPICA\n     ', '2020-03-24 17:16:12', '2020-03-24 17:16:12'),
(347, '\n      31104070\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'ESFINCTEROTOMIA - URETRA\n     ', '2020-03-24 17:16:12', '2020-03-24 17:16:12'),
(348, '\n      31104088\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'FISTULA URETRO-CUTANEA - CORRECAO CIRURGICA\n     ', '2020-03-24 17:16:12', '2020-03-24 17:16:12'),
(349, '\n      31104096\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'FISTULA URETRO-RETAL - CORRECAO CIRURGICA\n     ', '2020-03-24 17:16:12', '2020-03-24 17:16:12'),
(350, '\n      31104100\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'FISTULA URETRO-VAGINAL - CORRECAO CIRURGICA\n     ', '2020-03-24 17:16:12', '2020-03-24 17:16:12'),
(351, '\n      31104118\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'INCONTINENCIA URINARIA MASCULINA - TRATAMENTO CIRURGICO (EXCLUI IMPLANTE DE ESFINCTER ARTIFICIAL)\n     ', '2020-03-24 17:16:12', '2020-03-24 17:16:12'),
(352, '\n      31104126\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'INJECOES PERIURETRAIS  INCLUINDO URETROCISTOCOPIA POR TRATAMENTO\n     ', '2020-03-24 17:16:12', '2020-03-24 17:16:12'),
(353, '\n      31104134\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'MEATOPLASTIA (RETALHO CUTANEO)\n     ', '2020-03-24 17:16:12', '2020-03-24 17:16:12'),
(354, '\n      31104142\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'MEATOTOMIA URETRAL\n     ', '2020-03-24 17:16:13', '2020-03-24 17:16:13'),
(355, '\n      31104150\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'NEOURETRA PROXIMAL (CISTOURETROPLASTIA)\n     ', '2020-03-24 17:16:13', '2020-03-24 17:16:13'),
(356, '\n      31104169\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'RESSECCAO DE CARUNCULA\n     ', '2020-03-24 17:16:13', '2020-03-24 17:16:13'),
(357, '\n      31104177\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'RESSECCAO DE VALVULA URETRAL POSTERIOR\n     ', '2020-03-24 17:16:13', '2020-03-24 17:16:13'),
(358, '\n      31104185\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'TUMOR URETRAL - EXCISAO\n     ', '2020-03-24 17:16:13', '2020-03-24 17:16:13'),
(359, '\n      31104193\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'URETROPLASTIA ANTERIOR\n     ', '2020-03-24 17:16:13', '2020-03-24 17:16:13'),
(360, '\n      31104207\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'URETROPLASTIA POSTERIOR\n     ', '2020-03-24 17:16:13', '2020-03-24 17:16:13'),
(361, '\n      31104215\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'URETROSTOMIA\n     ', '2020-03-24 17:16:13', '2020-03-24 17:16:13'),
(362, '\n      31104223\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'URETROTOMIA INTERNA\n     ', '2020-03-24 17:16:13', '2020-03-24 17:16:13'),
(363, '\n      31104231\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'URETROTOMIA INTERNA COM PROTESE ENDOURETRAL\n     ', '2020-03-24 17:16:13', '2020-03-24 17:16:13'),
(364, '\n      31104240\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'URETRECTOMIA TOTAL\n     ', '2020-03-24 17:16:13', '2020-03-24 17:16:13'),
(365, '\n      31104274\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'INCONTINENCIA URINARIA MASCULINA - SLING\n     ', '2020-03-24 17:16:13', '2020-03-24 17:16:13'),
(366, '\n      31104282\n     ', 'PROCED CIRURGICOS E INVASIVOS SISTEMA URINARIO\n     ', 'URETRA\n     ', 'INCONTINENCIA URINARIA MASCULINA - ESFINCTER ARTIFICIAL\n     ', '2020-03-24 17:16:13', '2020-03-24 17:16:13'),
(367, '\n      31203060\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPROD MASCUL\n     ', 'TESTICULO\n     ', 'ORQUIDOPEXIA UNILATERAL\n     ', '2020-03-24 17:16:13', '2020-03-24 17:16:13'),
(368, '\n      31203078\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPROD MASCUL\n     ', 'TESTICULO\n     ', 'ORQUIECTOMIA UNILATERAL\n     ', '2020-03-24 17:16:13', '2020-03-24 17:16:13'),
(369, '\n      31203108\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPROD MASCUL\n     ', 'TESTICULO\n     ', 'TORCAO DE TESTICULO - CURA CIRURGICA\n     ', '2020-03-24 17:16:13', '2020-03-24 17:16:13'),
(370, '\n      31203116\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPROD MASCUL\n     ', 'TESTICULO\n     ', 'TUMOR DE TESTICULO - RESSECCAO\n     ', '2020-03-24 17:16:13', '2020-03-24 17:16:13'),
(371, '\n      31203124\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPROD MASCUL\n     ', 'TESTICULO\n     ', 'VARICOCELE UNILATERAL - CORRECAO CIRURGICA\n     ', '2020-03-24 17:16:14', '2020-03-24 17:16:14'),
(372, '\n      31203132\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPROD MASCUL\n     ', 'TESTICULO\n     ', 'ORQUIDOPEXIA LAPAROSCOPICA UNILATERAL\n     ', '2020-03-24 17:16:14', '2020-03-24 17:16:14'),
(373, '\n      31203140\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPROD MASCUL\n     ', 'TESTICULO\n     ', 'ORQUIECTOMIA INTRA-ABDOMINAL LAPAROSCOPICA UNILATERAL\n     ', '2020-03-24 17:16:14', '2020-03-24 17:16:14'),
(374, '\n      31204015\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPROD MASCUL\n     ', 'EPIDIDIMO\n     ', 'BIOPSIA DE EPIDIDIMO\n     ', '2020-03-24 17:16:14', '2020-03-24 17:16:14'),
(375, '\n      31204023\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPROD MASCUL\n     ', 'EPIDIDIMO\n     ', 'DRENAGEM DE ABSCESSO - EPIDIDIMO\n     ', '2020-03-24 17:16:14', '2020-03-24 17:16:14'),
(376, '\n      31205011\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPROD MASCUL\n     ', 'CORDAO ESPERMATICO\n     ', 'ESPERMATOCELECTOMIA UNILATERAL\n     ', '2020-03-24 17:16:14', '2020-03-24 17:16:14'),
(377, '\n      31205020\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPROD MASCUL\n     ', 'CORDAO ESPERMATICO\n     ', 'EXPLORACAO CIRURGICA DO DEFERENTE UNILATERAL\n     ', '2020-03-24 17:16:14', '2020-03-24 17:16:14'),
(378, '\n      31205038\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPROD MASCUL\n     ', 'CORDAO ESPERMATICO\n     ', 'RECANALIZACAO DOS DUCTUS DEFERENTES\n     ', '2020-03-24 17:16:14', '2020-03-24 17:16:14'),
(379, '\n      31205046\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPROD MASCUL\n     ', 'CORDAO ESPERMATICO\n     ', 'VASECTOMIA UNILATERAL\n     ', '2020-03-24 17:16:14', '2020-03-24 17:16:14'),
(380, '\n      31205070\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPROD MASCUL\n     ', 'CORDAO ESPERMATICO\n     ', 'CIRURGIA ESTERILIZADORA MASCULINA\n     ', '2020-03-24 17:16:14', '2020-03-24 17:16:14'),
(381, '\n      31301010\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VULVA\n     ', 'BARTOLINECTOMIA UNILATERAL\n     ', '2020-03-24 17:16:14', '2020-03-24 17:16:14'),
(382, '\n      31301029\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VULVA\n     ', 'BIOPSIA DE VULVA\n     ', '2020-03-24 17:16:14', '2020-03-24 17:16:14'),
(383, '\n      31301037\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VULVA\n     ', 'CAUTERIZACAO QUIMICA, OU ELETROCAUTERIZACAO, OU CRIOCAUTERIZACAO DE LESOES DA VULVA (POR GRUPO DE ATE 5 LESOES)\n     ', '2020-03-24 17:16:14', '2020-03-24 17:16:14'),
(384, '\n      31301045\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VULVA\n     ', 'CLITORECTOMIA (PARCIAL OU TOTAL)\n     ', '2020-03-24 17:16:14', '2020-03-24 17:16:14'),
(385, '\n      31301053\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VULVA\n     ', 'CLITOROPLASTIA\n     ', '2020-03-24 17:16:14', '2020-03-24 17:16:14'),
(386, '\n      31301061\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VULVA\n     ', 'EXCISAO RADICAL LOCAL DA VULVA (NAO INCLUI A LINFADENECTOMIA)\n     ', '2020-03-24 17:16:14', '2020-03-24 17:16:14'),
(387, '\n      31301070\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VULVA\n     ', 'EXERESE DE GLANDULA DE SKENE\n     ', '2020-03-24 17:16:14', '2020-03-24 17:16:14'),
(388, '\n      31301088\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VULVA\n     ', 'EXERESE DE LESAO DA VULVA E/OU DO PERINEO  POR GRUPO DE ATE 5 LESOES\n     ', '2020-03-24 17:16:15', '2020-03-24 17:16:15'),
(389, '\n      31301096\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VULVA\n     ', 'HIPERTROFIA DOS PEQUENOS LABIOS - CORRECAO CIRURGICA\n     ', '2020-03-24 17:16:15', '2020-03-24 17:16:15'),
(390, '\n      31301100\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VULVA\n     ', 'INCISAO E DRENAGEM DA GLANDULA DE BARTHOLIN OU SKENE\n     ', '2020-03-24 17:16:15', '2020-03-24 17:16:15'),
(391, '\n      31301118\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VULVA\n     ', 'MARSUPIALIZACAO DA GLANDULA DE BARTHOLIN\n     ', '2020-03-24 17:16:15', '2020-03-24 17:16:15'),
(392, '\n      31301126\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VULVA\n     ', 'VULVECTOMIA AMPLIADA (NAO INCLUI A LINFADENECTOMIA)\n     ', '2020-03-24 17:16:15', '2020-03-24 17:16:15'),
(393, '\n      31301134\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VULVA\n     ', 'VULVECTOMIA SIMPLES\n     ', '2020-03-24 17:16:15', '2020-03-24 17:16:15'),
(394, '\n      31302017\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VAGINA\n     ', 'BIOPSIA DE VAGINA\n     ', '2020-03-24 17:16:15', '2020-03-24 17:16:15'),
(395, '\n      31302025\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VAGINA\n     ', 'COLPECTOMIA\n     ', '2020-03-24 17:16:15', '2020-03-24 17:16:15'),
(396, '\n      31302033\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VAGINA\n     ', 'COLPOCLEISE (LEFORT)\n     ', '2020-03-24 17:16:15', '2020-03-24 17:16:15'),
(397, '\n      31302041\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VAGINA\n     ', 'COLPOPLASTIA ANTERIOR\n     ', '2020-03-24 17:16:15', '2020-03-24 17:16:15'),
(398, '\n      31302050\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VAGINA\n     ', 'COLPOPLASTIA POSTERIOR COM PERINEORRAFIA\n     ', '2020-03-24 17:16:15', '2020-03-24 17:16:15'),
(399, '\n      31302068\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VAGINA\n     ', 'COLPORRAFIA OU COLPOPERINEOPLASTIA INCLUINDO RESSECCAO DE SEPTO OU RESSUTURA DE PAREDE VAGINAL\n     ', '2020-03-24 17:16:15', '2020-03-24 17:16:15'),
(400, '\n      31302076\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VAGINA\n     ', 'COLPOTOMIA OU CULDOCENTESE\n     ', '2020-03-24 17:16:15', '2020-03-24 17:16:15'),
(401, '\n      31302084\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VAGINA\n     ', 'EXERESE DE CISTO VAGINAL\n     ', '2020-03-24 17:16:16', '2020-03-24 17:16:16'),
(402, '\n      31302092\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VAGINA\n     ', 'EXTRACAO DE CORPO ESTRANHO COM ANESTESIA GERAL OU BLOQUEIO\n     ', '2020-03-24 17:16:16', '2020-03-24 17:16:16'),
(403, '\n      31302106\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VAGINA\n     ', 'FISTULA GINECOLOGICA - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:16', '2020-03-24 17:16:16'),
(404, '\n      31302114\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VAGINA\n     ', 'HIMENOTOMIA\n     ', '2020-03-24 17:16:16', '2020-03-24 17:16:16'),
(405, '\n      31302122\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VAGINA\n     ', 'NEOVAGINA (COLON, DELGADO, TUBO DE PELE)\n     ', '2020-03-24 17:16:16', '2020-03-24 17:16:16'),
(406, '\n      31302130\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'VAGINA\n     ', 'CAUTERIZACAO QUIMICA, OU ELETROCAUTERIZACAO, OU CRIOCAUTERIZACAO DE LESOES DA VAGINA (POR GRUPO DE ATE 5 LESOES)\n     ', '2020-03-24 17:16:16', '2020-03-24 17:16:16'),
(407, '\n      31303013\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'ASPIRACAO MANUAL INTRA-UTERINA  AMIU\n     ', '2020-03-24 17:16:16', '2020-03-24 17:16:16'),
(408, '\n      31303021\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'BIOPSIA DO COLO UTERINO\n     ', '2020-03-24 17:16:16', '2020-03-24 17:16:16'),
(409, '\n      31303030\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'BIOPSIA DO ENDOMETRIO\n     ', '2020-03-24 17:16:16', '2020-03-24 17:16:16'),
(410, '\n      31303056\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'CURETAGEM GINECOLOGICA SEMIOTICA E/OU TERAPEUTICA COM OU SEM DILATACAO DE COLO UTERINO\n     ', '2020-03-24 17:16:16', '2020-03-24 17:16:16'),
(411, '\n      31303064\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'DILATACAO DO COLO UTERINO\n     ', '2020-03-24 17:16:16', '2020-03-24 17:16:16'),
(412, '\n      31303072\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'EXCISAO DE POLIPO CERVICAL\n     ', '2020-03-24 17:16:16', '2020-03-24 17:16:16'),
(413, '\n      31303080\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'HISTERECTOMIA SUBTOTAL COM OU SEM ANEXECTOMIA, UNI OU BILATERAL - QUALQUER VIA\n     ', '2020-03-24 17:16:16', '2020-03-24 17:16:16'),
(414, '\n      31303102\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'HISTERECTOMIA TOTAL (QUALQUER VIA)\n     ', '2020-03-24 17:16:16', '2020-03-24 17:16:16'),
(415, '\n      31303110\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'HISTERECTOMIA TOTAL AMPLIADA - QUALQUER VIA - (NAO INCLUI A LINFADENECTOMIA PELVICA)\n     ', '2020-03-24 17:16:17', '2020-03-24 17:16:17'),
(416, '\n      31303129\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'HISTERECTOMIA TOTAL COM ANEXECTOMIA UNI OU BILATERAL (QUALQUER VIA)\n     ', '2020-03-24 17:16:17', '2020-03-24 17:16:17'),
(417, '\n      31303137\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'METROPLASTIA (STRASSMANN OU OUTRA TECNICA)\n     ', '2020-03-24 17:16:17', '2020-03-24 17:16:17'),
(418, '\n      31303145\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'MIOMECTOMIA UTERINA\n     ', '2020-03-24 17:16:17', '2020-03-24 17:16:17'),
(419, '\n      31303153\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'TRAQUELECTOMIA - AMPUTACAO, CONIZACAO - (COM OU SEM CIRURGIA DE ALTA FREQUENCIA / CAF)\n     ', '2020-03-24 17:16:17', '2020-03-24 17:16:17'),
(420, '\n      31303161\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'TRAQUELECTOMIA RADICAL (NAO INCLUI A LINFADENECTOMIA)\n     ', '2020-03-24 17:16:17', '2020-03-24 17:16:17'),
(421, '\n      31303170\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'HISTEROSCOPIA CIRURGICA COM BIOPSIA E/OU CURETAGEM UTERINA, LISE DE SINEQUIAS, RETIRADA DE CORPO ESTRANHO\n     ', '2020-03-24 17:16:17', '2020-03-24 17:16:17'),
(422, '\n      31303188\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'HISTEROSCOPIA COM RESSECTOSCOPIO PARA MIOMECTOMIA, POLIPECTOMIA, METROPLASTIA, ENDOMETRECTOMIA E RESSECCAO DE SINEQUIAS\n     ', '2020-03-24 17:16:17', '2020-03-24 17:16:17'),
(423, '\n      31303196\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'CAUTERIZACAO QUIMICA, OU ELETROCAUTERIZACAO, OU CRIOCAUTERIZACAO DE LESOES DE COLO UTERINO  POR SESSAO\n     ', '2020-03-24 17:16:17', '2020-03-24 17:16:17'),
(424, '\n      31303200\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'HISTERECTOMIA SUBTOTAL LAPAROSCOPICA COM OU SEM ANEXECTOMIA, UNI OU BILATERAL  VIA ALTA\n     ', '2020-03-24 17:16:17', '2020-03-24 17:16:17'),
(425, '\n      31303218\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'HISTERECTOMIA TOTAL LAPAROSCOPICA\n     ', '2020-03-24 17:16:17', '2020-03-24 17:16:17'),
(426, '\n      31303226\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'HISTERECTOMIA TOTAL LAPAROSCOPICA AMPLIADA\n     ', '2020-03-24 17:16:17', '2020-03-24 17:16:17'),
(427, '\n      31303234\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'HISTERECTOMIA TOTAL LAPAROSCOPICA COM ANEXECTOMIA UNI OU BILATERAL\n     ', '2020-03-24 17:16:17', '2020-03-24 17:16:17'),
(428, '\n      31303242\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'METROPLASTIA LAPAROSCOPICA\n     ', '2020-03-24 17:16:17', '2020-03-24 17:16:17'),
(429, '\n      31303250\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'MIOMECTOMIA UTERINA LAPAROSCOPICA\n     ', '2020-03-24 17:16:18', '2020-03-24 17:16:18'),
(430, '\n      31303269\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'IMPLANTE DE DISPOSITIVO INTRA-UTERINO (DIU) NAO HORMONAL\n     ', '2020-03-24 17:16:18', '2020-03-24 17:16:18'),
(431, '\n      31303285\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'HISTERECTOMIA PUERPERAL\n     ', '2020-03-24 17:16:18', '2020-03-24 17:16:18'),
(432, '\n      31303293\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'IMPLANTE DISPOSITIVO INTRA-UTERINO  DIU  HORMONAL\n     ', '2020-03-24 17:16:18', '2020-03-24 17:16:18'),
(433, '\n      31303307\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'RETIRADA DE DIU POR HISTEROSCOPIA\n     ', '2020-03-24 17:16:18', '2020-03-24 17:16:18'),
(434, '\n      31303315\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'CURETAGEM UTERINA POS-PARTO\n     ', '2020-03-24 17:16:18', '2020-03-24 17:16:18'),
(435, '\n      31303323\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'UTERO\n     ', 'HISTERECTOMIA POS-PARTO\n     ', '2020-03-24 17:16:18', '2020-03-24 17:16:18'),
(436, '\n      31304010\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'TUBAS\n     ', 'ESTERILIZACAO TUBARIA\n     ', '2020-03-24 17:16:18', '2020-03-24 17:16:18'),
(437, '\n      31304028\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'TUBAS\n     ', 'NEOSSALPINGOSTOMIA DISTAL\n     ', '2020-03-24 17:16:18', '2020-03-24 17:16:18'),
(438, '\n      31304036\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'TUBAS\n     ', 'RECANALIZACAO TUBARIA (QUALQUER TECNICA), UNI OU BILATERAL (COM MICROSCOPIO OU LUPA)\n     ', '2020-03-24 17:16:18', '2020-03-24 17:16:18'),
(439, '\n      31304044\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'TUBAS\n     ', 'SALPINGECTOMIA UNI OU BILATERAL\n     ', '2020-03-24 17:16:18', '2020-03-24 17:16:18'),
(440, '\n      31304052\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'TUBAS\n     ', 'LAQUEADURA TUBARIA LAPAROSCOPICA\n     ', '2020-03-24 17:16:18', '2020-03-24 17:16:18'),
(441, '\n      31304060\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'TUBAS\n     ', 'NEOSSALPINGOSTOMIA DISTAL LAPAROSCOPICA\n     ', '2020-03-24 17:16:18', '2020-03-24 17:16:18'),
(442, '\n      31304079\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'TUBAS\n     ', 'RECANALIZACAO TUBARIA LAPAROSCOPICA UNI OU BILATERAL\n     ', '2020-03-24 17:16:18', '2020-03-24 17:16:18'),
(443, '\n      31304087\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'TUBAS\n     ', 'SALPINGECTOMIA UNI OU BILATERAL LAPAROSCOPICA\n     ', '2020-03-24 17:16:18', '2020-03-24 17:16:18'),
(444, '\n      31305016\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'OVARIOS\n     ', 'OOFORECTOMIA UNI OU BILATERAL OU OOFOROPLASTIA UNI OU BILATERAL\n     ', '2020-03-24 17:16:18', '2020-03-24 17:16:18'),
(445, '\n      31305024\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'OVARIOS\n     ', 'TRANSLOCACAO DE OVARIOS\n     ', '2020-03-24 17:16:19', '2020-03-24 17:16:19'),
(446, '\n      31305032\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'OVARIOS\n     ', 'OOFORECTOMIA LAPAROSCOPICA UNI OU BILATERAL OU OOFOROPLASTIA UNI OU BILATERAL\n     ', '2020-03-24 17:16:19', '2020-03-24 17:16:19'),
(447, '\n      31306012\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PERINEO\n     ', 'CORRECAO DE DEFEITO LATERAL\n     ', '2020-03-24 17:16:19', '2020-03-24 17:16:19'),
(448, '\n      31306020\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PERINEO\n     ', 'CORRECAO DE ENTEROCELE\n     ', '2020-03-24 17:16:19', '2020-03-24 17:16:19'),
(449, '\n      31306039\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PERINEO\n     ', 'CORRECAO DE ROTURA PERINEAL DE III  GRAU  (COM LESAO DO  ESFINCTER)  E  RECONSTITUICAO  POR  PLASTICA - QUALQUER TECNICA\n     ', '2020-03-24 17:16:19', '2020-03-24 17:16:19'),
(450, '\n      31306047\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PERINEO\n     ', 'PERINEORRAFIA (NAO OBSTETRICA) E/OU EPISIOTOMIA E/OU EPISIORRAFIA\n     ', '2020-03-24 17:16:19', '2020-03-24 17:16:19'),
(451, '\n      31306055\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PERINEO\n     ', 'RECONSTRUCAO PERINEAL COM RETALHOS MIOCUTANEOS\n     ', '2020-03-24 17:16:19', '2020-03-24 17:16:19'),
(452, '\n      31306063\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PERINEO\n     ', 'RESSECCAO DE TUMOR DO SEPTO RETO-VAGINAL\n     ', '2020-03-24 17:16:19', '2020-03-24 17:16:19'),
(453, '\n      31306071\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PERINEO\n     ', 'SEIO UROGENITAL - PLASTICA\n     ', '2020-03-24 17:16:19', '2020-03-24 17:16:19'),
(454, '\n      31307019\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'CANCER DE OVARIO (DEBULKING)\n     ', '2020-03-24 17:16:19', '2020-03-24 17:16:19'),
(455, '\n      31307027\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'CIRURGIA (VIA ALTA  OU  BAIXA)  DO  PROLAPSO  DE  CUPULA VAGINAL (FIXACAO  SACRAL  OU  NO  LIGAMENTO SACRO- ESPINHOSO) QUALQUER TECNICA\n     ', '2020-03-24 17:16:19', '2020-03-24 17:16:19'),
(456, '\n      31307035\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'CULDOPLASTIA (MAC CALL, MOSCHOWICZ, ETC.)\n     ', '2020-03-24 17:16:19', '2020-03-24 17:16:19'),
(457, '\n      31307043\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'ENDOMETRIOSE PERITONIAL - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:19', '2020-03-24 17:16:19'),
(458, '\n      31307051\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'EPIPLOPLASTIA OU APLICACAO DE MEMBRANAS ANTIADERENTES\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20'),
(459, '\n      31307060\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'LAPAROSCOPIA GINECOLOGICA COM OU SEM BIOPSIA  INCLUI A CROMOTUBAGEM\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20'),
(460, '\n      31307078\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'LIBERACAO DE ADERENCIAS PELVICAS COM OU SEM RESSECCAO DE CISTOS PERITONIAIS OU SALPINGOLISE\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20'),
(461, '\n      31307086\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'LIGADURA DE VEIA OVARIANA\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20'),
(462, '\n      31307094\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'LIGAMENTOPEXIA PELVICA\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20'),
(463, '\n      31307108\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'NEURECTOMIA PRE-SACRAL OU DO NERVO GENITO-FEMORAL\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20'),
(464, '\n      31307116\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'OMENTECTOMIA\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20'),
(465, '\n      31307124\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'RESSECCAO DE TUMOR DE PAREDE ABDOMINAL PELVICA\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20'),
(466, '\n      31307132\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'RESSECCAO OU LIGADURA DE VARIZES PELVICAS\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20'),
(467, '\n      31307140\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'SECCAO DE LIGAMENTOS UTERO-SACROS\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20'),
(468, '\n      31307159\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'CANCER DE OVARIO  DEBULKING  LAPAROSCOPICA\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20'),
(469, '\n      31307167\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'CIRURGIA LAPAROSCOPICA DO PROLAPSO DE CUPULA VAGINAL  FIXACAO SACRAL OU NO LIGAMENTO SACRO- ESPINHOSO\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20'),
(470, '\n      31307175\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'CULDOPLASTIA LAPAROSCOPICA  MAC CALL, MOSCHOWICZ, ETC\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20');
INSERT INTO `table_tuss` (`id`, `cod`, `group_tuss`, `sub_group`, `procedure_tuss`, `create_at`, `update_at`) VALUES
(471, '\n      31307183\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'ENDOMETRIOSE PERITONEAL - TRATAMENTO CIRURGICO VIA LAPAROSCOPICA\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20'),
(472, '\n      31307191\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'EPIPLOPLASTIA OU APLICACAO DE MEMBRANAS ANTIADERENTES VIA LAPAROSCOPICA\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20'),
(473, '\n      31307205\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'LIBERACAO LAPAROSCOPICA DE ADERENCIAS PELVICAS COM OU SEM RESSECCAO DE CISTOS PERITONEAIS OU SALPINGOLISE\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20'),
(474, '\n      31307213\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'LIGADURA DE VEIA OVARIANA LAPAROSCOPICA\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20'),
(475, '\n      31307221\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'LIGAMENTOPEXIA PELVICA LAPAROSCOPICA\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20'),
(476, '\n      31307230\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'NEURECTOMIA LAPAROSCOPICA PRE-SACRAL OU DO NERVO GENITO-FEMORAL\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20'),
(477, '\n      31307248\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'OMENTECTOMIA LAPAROSCOPICA\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20'),
(478, '\n      31307256\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'RESSECCAO LAPAROSCOPICA DE TUMOR DE PAREDE ABDOMINAL\n     ', '2020-03-24 17:16:20', '2020-03-24 17:16:20'),
(479, '\n      31307264\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'RESSECCAO OU LIGADURA LAPAROSCOPICA DE VARIZES PELVICAS\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(480, '\n      31307272\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'CAVIDADE E PAREDES PELVICAS\n     ', 'SECCAO LAPAROSCOPICA DE LIGAMENTOS UTERO-SACROS\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(481, '\n      31309011\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PARTOS E OUTROS PROCEDIMENTOS OBSTETRICOS\n     ', 'AMNIORREDUCAO OU AMNIOINFUSAO\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(482, '\n      31309020\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PARTOS E OUTROS PROCEDIMENTOS OBSTETRICOS\n     ', 'ASPIRACAO MANUAL INTRA-UTERINA (AMIU) POS- ABORTAMENTO\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(483, '\n      31309038\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PARTOS E OUTROS PROCEDIMENTOS OBSTETRICOS\n     ', 'ASSISTENCIA AO TRABALHO DE PARTO, POR HORA (ATE O LIMITE DE 6 HORAS). NAO DEVERA SER CONSIDERADO SE O PARTO OCORRER NA PRIMEIRA HORA APOS O INICIO DA ASSISTENCIA. APOS A PRIMEIRA HORA, ALEM DA\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(484, '\n      31309046\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PARTOS E OUTROS PROCEDIMENTOS OBSTETRICOS\n     ', 'CERCLAGEM DO COLO UTERINO (QUALQUER TECNICA)\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(485, '\n      31309054\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PARTOS E OUTROS PROCEDIMENTOS OBSTETRICOS\n     ', 'CESARIANA\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(486, '\n      31309062\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PARTOS E OUTROS PROCEDIMENTOS OBSTETRICOS\n     ', 'CURETAGEM POS-ABORTAMENTO\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(487, '\n      31309089\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PARTOS E OUTROS PROCEDIMENTOS OBSTETRICOS\n     ', 'GRAVIDEZ  ECTOPICA - CIRURGIA\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(488, '\n      31309097\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PARTOS E OUTROS PROCEDIMENTOS OBSTETRICOS\n     ', 'MATURACAO CERVICAL PARA INDUCAO DE ABORTAMENTO OU DE TRABALHO DE PARTO\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(489, '\n      31309100\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PARTOS E OUTROS PROCEDIMENTOS OBSTETRICOS\n     ', 'INVERSAO UTERINA AGUDA - REDUCAO MANUAL\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(490, '\n      31309119\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PARTOS E OUTROS PROCEDIMENTOS OBSTETRICOS\n     ', 'INVERSAO UTERINA - TRATAMENTO CIRURGICO\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(491, '\n      31309127\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PARTOS E OUTROS PROCEDIMENTOS OBSTETRICOS\n     ', 'PARTO (VIA VAGINAL)\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(492, '\n      31309135\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PARTOS E OUTROS PROCEDIMENTOS OBSTETRICOS\n     ', 'PARTO MULTIPLO (CADA UM SUBSEQUENTE AO INICIAL)\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(493, '\n      31309143\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PARTOS E OUTROS PROCEDIMENTOS OBSTETRICOS\n     ', 'PUNCAO ESCALPOFETAL PARA AVALIACAO PH FETAL\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(494, '\n      31309151\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PARTOS E OUTROS PROCEDIMENTOS OBSTETRICOS\n     ', 'REVISAO OBSTETRICA DE PARTO OCORRIDO FORA DO HOSPITAL  INCLUI EXAME, DEQUITACAO E SUTURA DE LACERACOES ATE DE 2  GRAU\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(495, '\n      31309178\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PARTOS E OUTROS PROCEDIMENTOS OBSTETRICOS\n     ', 'VERSAO CEFALICA EXTERNA\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(496, '\n      31309186\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PARTOS E OUTROS PROCEDIMENTOS OBSTETRICOS\n     ', 'GRAVIDEZ ECTOPICA - CIRURGIA LAPAROSCOPICA\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(497, '\n      31309194\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PARTOS E OUTROS PROCEDIMENTOS OBSTETRICOS\n     ', 'INVERSAO UTERINA - TRATAMENTO CIRURGICO LAPAROSCOPICO\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(498, '\n      31309208\n     ', 'PROCED CIR E INVAS  SIST GENITAL E REPR FEMININO\n     ', 'PARTOS E OUTROS PROCEDIMENTOS OBSTETRICOS\n     ', 'CESARIANA COM HISTERECTOMIA\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(499, '\n      40201066\n     ', 'PROCED DIAG E TERAP ELETROFISIOLOGIC/MEC E FUNC\n     ', 'ENDOSCOPIA DIAGNOSTICA\n     ', 'CISTOSCOPIA E/OU URETROSCOPIA\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(500, '\n      40201155\n     ', 'PROCED DIAG E TERAP ELETROFISIOLOGIC/MEC E FUNC\n     ', 'ENDOSCOPIA DIAGNOSTICA\n     ', 'HISTEROSCOPIA DIAGNOSTICA COM BIOPSIA\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(501, '\n      40201163\n     ', 'PROCED DIAG E TERAP ELETROFISIOLOGIC/MEC E FUNC\n     ', 'ENDOSCOPIA DIAGNOSTICA\n     ', 'LAPAROSCOPIA\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(502, '\n      40201279\n     ', 'PROCED DIAG E TERAP ELETROFISIOLOGIC/MEC E FUNC\n     ', 'ENDOSCOPIA DIAGNOSTICA\n     ', 'URETEROSCOPIA FLEXIVEL UNILATERAL\n     ', '2020-03-24 17:16:21', '2020-03-24 17:16:21'),
(503, '\n      40201287\n     ', 'PROCED DIAG E TERAP ELETROFISIOLOGIC/MEC E FUNC\n     ', 'ENDOSCOPIA DIAGNOSTICA\n     ', 'URETEROSCOPIA RIGIDA UNILATERAL\n     ', '2020-03-24 17:16:22', '2020-03-24 17:16:22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `team_procedures`
--

CREATE TABLE `team_procedures` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `specialty` varchar(255) NOT NULL,
  `percentage` varchar(255) NOT NULL,
  `fk_procedure` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `team_procedures`
--

INSERT INTO `team_procedures` (`id`, `name`, `specialty`, `percentage`, `fk_procedure`, `create_at`, `update_at`) VALUES
(1, 'NOME', 'ESPECIALIDADE', 'PERCENT', 21, '2020-03-25 18:43:04', '2020-03-25 18:43:04'),
(2, 'Tesdte', 'teste', 'teste', 21, '2020-03-25 18:43:36', '2020-03-25 18:43:36'),
(3, 'Tesdte', 'teste', 'teste', 21, '2020-03-25 18:44:18', '2020-03-25 18:44:18'),
(4, 'name', 'name', 'name', 21, '2020-03-25 18:44:44', '2020-03-25 18:44:44'),
(5, 'name', 'name', 'name', 21, '2020-03-25 18:44:52', '2020-03-25 18:44:52'),
(6, 'name', 'name', 'name', 21, '2020-03-25 18:45:33', '2020-03-25 18:45:33'),
(7, 'name', 'name', 'name', 21, '2020-03-25 18:46:09', '2020-03-25 18:46:09');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tuss_procedure`
--

CREATE TABLE `tuss_procedure` (
  `id` int(11) NOT NULL,
  `fk_procedure` int(11) NOT NULL,
  `fk_tuss` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `tuss_procedure`
--

INSERT INTO `tuss_procedure` (`id`, `fk_procedure`, `fk_tuss`, `create_at`, `update_at`) VALUES
(1, 19, 2, '2020-03-25 15:42:06', '2020-03-25 15:42:06'),
(2, 19, 3, '2020-03-25 15:42:06', '2020-03-25 15:42:06'),
(3, 20, 2, '2020-03-25 15:42:51', '2020-03-25 15:42:51'),
(4, 20, 3, '2020-03-25 15:42:51', '2020-03-25 15:42:51'),
(5, 21, 5, '2020-03-25 17:38:05', '2020-03-25 17:38:05'),
(6, 21, 6, '2020-03-25 17:38:05', '2020-03-25 17:38:05');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tellphone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cellphone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_addresses` int(11) NOT NULL,
  `fk_group` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `tellphone`, `cellphone`, `fk_addresses`, `fk_group`, `created_at`, `updated_at`) VALUES
(1, 'Junior', 'rabelojunior105@gmail.com', '2a40857ba624d4393e0633c217a10d50', '84046513', '135438512', 1, 1, '2020-02-12 13:33:52', '2020-02-17 16:04:34'),
(4, 'Junior', 'rabelojunior105s@gmail.com', '6a79013aa349d4fe6939ed2c0ae86cf4', '(00) 0000-0000', '(88) 9 8150-0134', 74, 1, '2020-02-18 14:10:45', '2020-02-18 14:36:09'),
(5, 'Felipe ', 'felipe@creative.com', '6a79013aa349d4fe6939ed2c0ae86cf4', '(00) 0000-0000', '(00) 0 0000-0000', 76, 1, '2020-02-18 18:39:04', '2020-02-18 18:39:04'),
(6, 'Felipe ', 'felipe@creative.com', '6a79013aa349d4fe6939ed2c0ae86cf4', '(00) 0000-0000', '(00) 0 0000-0000', 77, 1, '2020-02-18 18:39:32', '2020-02-18 18:39:32');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `bank_account`
--
ALTER TABLE `bank_account`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `clinics`
--
ALTER TABLE `clinics`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_addresses` (`fk_addresses`);

--
-- Índices para tabela `consultancies`
--
ALTER TABLE `consultancies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_clinics` (`fk_clinics`);

--
-- Índices para tabela `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `health_insurance`
--
ALTER TABLE `health_insurance`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `hospital`
--
ALTER TABLE `hospital`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `medics`
--
ALTER TABLE `medics`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `office`
--
ALTER TABLE `office`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_medics` (`fk_medics`);

--
-- Índices para tabela `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `plan`
--
ALTER TABLE `plan`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `procedures`
--
ALTER TABLE `procedures`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `scheduling`
--
ALTER TABLE `scheduling`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `table_tuss`
--
ALTER TABLE `table_tuss`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `team_procedures`
--
ALTER TABLE `team_procedures`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `tuss_procedure`
--
ALTER TABLE `tuss_procedure`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=188;

--
-- AUTO_INCREMENT de tabela `bank_account`
--
ALTER TABLE `bank_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `clinics`
--
ALTER TABLE `clinics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `consultancies`
--
ALTER TABLE `consultancies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `group`
--
ALTER TABLE `group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `health_insurance`
--
ALTER TABLE `health_insurance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `hospital`
--
ALTER TABLE `hospital`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `medics`
--
ALTER TABLE `medics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `office`
--
ALTER TABLE `office`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de tabela `patients`
--
ALTER TABLE `patients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de tabela `plan`
--
ALTER TABLE `plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `procedures`
--
ALTER TABLE `procedures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de tabela `scheduling`
--
ALTER TABLE `scheduling`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de tabela `table_tuss`
--
ALTER TABLE `table_tuss`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=505;

--
-- AUTO_INCREMENT de tabela `team_procedures`
--
ALTER TABLE `team_procedures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `tuss_procedure`
--
ALTER TABLE `tuss_procedure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `clinics`
--
ALTER TABLE `clinics`
  ADD CONSTRAINT `clinics_ibfk_1` FOREIGN KEY (`fk_addresses`) REFERENCES `addresses` (`id`);

--
-- Limitadores para a tabela `consultancies`
--
ALTER TABLE `consultancies`
  ADD CONSTRAINT `consultancies_ibfk_1` FOREIGN KEY (`fk_clinics`) REFERENCES `clinics` (`id`);

--
-- Limitadores para a tabela `office`
--
ALTER TABLE `office`
  ADD CONSTRAINT `office_ibfk_1` FOREIGN KEY (`fk_medics`) REFERENCES `medics` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
