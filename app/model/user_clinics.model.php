<?php 

class User_clinics {

    private $id;
    private $fk_clinics;
    private $fk_user;
    private $created_at;
    private $updated_at;

    public function getId() {
        return $this->id; 
    }

    public function getFk_clinics() {
        return $this->fk_clinics; 
    }

    public function getFk_user() {
        return $this->fk_user; 
    }

    public function getCreated_at() {
        return $this->created_at; 
    }

    public function getUpdated_at() {
        return $this->updated_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setFk_clinics($fk_clinics) {
        $this->fk_clinics = $fk_clinics; 
    }

    public function setFk_user($fk_user) {
        $this->fk_user = $fk_user; 
    }

    public function setCreated_at($created_at) {
        $this->created_at = $created_at; 
    }

    public function setUpdated_at($updated_at) {
        $this->updated_at = $updated_at; 
    }


}