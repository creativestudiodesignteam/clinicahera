<?php 

class User {

    private $id;
    private $name;
    private $email;
    private $password;
    private $tellphone;
    private $cellphone;
    private $fk_addresses;
    private $fk_group;
    private $created_at;
    private $updated_at;

    public function getId() {
        return $this->id; 
    }

    public function getName() {
        return $this->name; 
    }

    public function getEmail() {
        return $this->email; 
    }

    public function getPassword() {
        return $this->password; 
    }

    public function getTellphone() {
        return $this->tellphone; 
    }

    public function getCellphone() {
        return $this->cellphone; 
    }

    public function getFk_addresses() {
        return $this->fk_addresses; 
    }

    public function getFk_group() {
        return $this->fk_group; 
    }

    public function getCreated_at() {
        return $this->created_at; 
    }

    public function getUpdated_at() {
        return $this->updated_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setName($name) {
        $this->name = $name; 
    }

    public function setEmail($email) {
        $this->email = $email; 
    }

    public function setPassword($password) {
        $this->password = $password; 
    }

    public function setTellphone($tellphone) {
        $this->tellphone = $tellphone; 
    }

    public function setCellphone($cellphone) {
        $this->cellphone = $cellphone; 
    }

    public function setFk_addresses($fk_addresses) {
        $this->fk_addresses = $fk_addresses; 
    }

    public function setFk_group($fk_group) {
        $this->fk_group = $fk_group; 
    }

    public function setCreated_at($created_at) {
        $this->created_at = $created_at; 
    }

    public function setUpdated_at($updated_at) {
        $this->updated_at = $updated_at; 
    }


}