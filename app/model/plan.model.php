<?php 

class Plan {

    private $id;
    private $name;
    private $accommodation;
    private $wallet;
    private $product;
    private $fk_health;
    private $create_at;
    private $update_at;

    public function getId() {
        return $this->id; 
    }

    public function getName() {
        return $this->name; 
    }

    public function getAccommodation() {
        return $this->accommodation; 
    }

    public function getWallet() {
        return $this->wallet; 
    }

    public function getProduct() {
        return $this->product; 
    }

    public function getFk_health() {
        return $this->fk_health; 
    }

    public function getCreate_at() {
        return $this->create_at; 
    }

    public function getUpdate_at() {
        return $this->update_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setName($name) {
        $this->name = $name; 
    }

    public function setAccommodation($accommodation) {
        $this->accommodation = $accommodation; 
    }

    public function setWallet($wallet) {
        $this->wallet = $wallet; 
    }

    public function setProduct($product) {
        $this->product = $product; 
    }

    public function setFk_health($fk_health) {
        $this->fk_health = $fk_health; 
    }

    public function setCreate_at($create_at) {
        $this->create_at = $create_at; 
    }

    public function setUpdate_at($update_at) {
        $this->update_at = $update_at; 
    }


}