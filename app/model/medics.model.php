<?php 

class Medics {

    private $id;
    private $name;
    private $time;
    private $cpf;
    private $cnpj;
    private $reason;
    private $rg;
    private $birthday;
    private $specialty;
    private $council_number;
    private $council_initials;
    private $council_uf;
    private $sexo;
    private $email;
    private $tellphone;
    private $cellphone;
    private $fk_addresses;
    private $fk_clinics;
    private $fk_bank;
    private $created_at;
    private $updated_at;

    public function getId() {
        return $this->id; 
    }

    public function getName() {
        return $this->name; 
    }

    public function getTime() {
        return $this->time; 
    }

    public function getCpf() {
        return $this->cpf; 
    }

    public function getCnpj() {
        return $this->cnpj; 
    }

    public function getReason() {
        return $this->reason; 
    }

    public function getRg() {
        return $this->rg; 
    }

    public function getBirthday() {
        return $this->birthday; 
    }

    public function getSpecialty() {
        return $this->specialty; 
    }

    public function getCouncil_number() {
        return $this->council_number; 
    }

    public function getCouncil_initials() {
        return $this->council_initials; 
    }

    public function getCouncil_uf() {
        return $this->council_uf; 
    }

    public function getSexo() {
        return $this->sexo; 
    }

    public function getEmail() {
        return $this->email; 
    }

    public function getTellphone() {
        return $this->tellphone; 
    }

    public function getCellphone() {
        return $this->cellphone; 
    }

    public function getFk_addresses() {
        return $this->fk_addresses; 
    }

    public function getFk_clinics() {
        return $this->fk_clinics; 
    }

    public function getFk_bank() {
        return $this->fk_bank; 
    }

    public function getCreated_at() {
        return $this->created_at; 
    }

    public function getUpdated_at() {
        return $this->updated_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setName($name) {
        $this->name = $name; 
    }

    public function setTime($time) {
        $this->time = $time; 
    }

    public function setCpf($cpf) {
        $this->cpf = $cpf; 
    }

    public function setCnpj($cnpj) {
        $this->cnpj = $cnpj; 
    }

    public function setReason($reason) {
        $this->reason = $reason; 
    }

    public function setRg($rg) {
        $this->rg = $rg; 
    }

    public function setBirthday($birthday) {
        $this->birthday = $birthday; 
    }

    public function setSpecialty($specialty) {
        $this->specialty = $specialty; 
    }

    public function setCouncil_number($council_number) {
        $this->council_number = $council_number; 
    }

    public function setCouncil_initials($council_initials) {
        $this->council_initials = $council_initials; 
    }

    public function setCouncil_uf($council_uf) {
        $this->council_uf = $council_uf; 
    }

    public function setSexo($sexo) {
        $this->sexo = $sexo; 
    }

    public function setEmail($email) {
        $this->email = $email; 
    }

    public function setTellphone($tellphone) {
        $this->tellphone = $tellphone; 
    }

    public function setCellphone($cellphone) {
        $this->cellphone = $cellphone; 
    }

    public function setFk_addresses($fk_addresses) {
        $this->fk_addresses = $fk_addresses; 
    }

    public function setFk_clinics($fk_clinics) {
        $this->fk_clinics = $fk_clinics; 
    }

    public function setFk_bank($fk_bank) {
        $this->fk_bank = $fk_bank; 
    }

    public function setCreated_at($created_at) {
        $this->created_at = $created_at; 
    }

    public function setUpdated_at($updated_at) {
        $this->updated_at = $updated_at; 
    }


}