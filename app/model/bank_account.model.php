<?php 

class Bank_account {

    private $id;
    private $name;
    private $number;
    private $agency;
    private $account;
    private $create_at;
    private $update_at;

    public function getId() {
        return $this->id; 
    }

    public function getName() {
        return $this->name; 
    }

    public function getNumber() {
        return $this->number; 
    }

    public function getAgency() {
        return $this->agency; 
    }

    public function getAccount() {
        return $this->account; 
    }

    public function getCreate_at() {
        return $this->create_at; 
    }

    public function getUpdate_at() {
        return $this->update_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setName($name) {
        $this->name = $name; 
    }

    public function setNumber($number) {
        $this->number = $number; 
    }

    public function setAgency($agency) {
        $this->agency = $agency; 
    }

    public function setAccount($account) {
        $this->account = $account; 
    }

    public function setCreate_at($create_at) {
        $this->create_at = $create_at; 
    }

    public function setUpdate_at($update_at) {
        $this->update_at = $update_at; 
    }


}