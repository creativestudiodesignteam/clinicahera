<?php 

class Team_procedures {

    private $id;
    private $name;
    private $specialty;
    private $percentage;
    private $fk_procedure;
    private $create_at;
    private $update_at;

    public function getId() {
        return $this->id; 
    }

    public function getName() {
        return $this->name; 
    }

    public function getSpecialty() {
        return $this->specialty; 
    }

    public function getPercentage() {
        return $this->percentage; 
    }

    public function getFk_procedure() {
        return $this->fk_procedure; 
    }

    public function getCreate_at() {
        return $this->create_at; 
    }

    public function getUpdate_at() {
        return $this->update_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setName($name) {
        $this->name = $name; 
    }

    public function setSpecialty($specialty) {
        $this->specialty = $specialty; 
    }

    public function setPercentage($percentage) {
        $this->percentage = $percentage; 
    }
    public function setFk_procedure($fk_procedure) {
        $this->fk_procedure = $fk_procedure; 
    }

    public function setCreate_at($create_at) {
        $this->create_at = $create_at; 
    }

    public function setUpdate_at($update_at) {
        $this->update_at = $update_at; 
    }


}