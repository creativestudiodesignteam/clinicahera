<?php 

class Tuss_procedure {

    private $id;
    private $fk_procedure;
    private $fk_tuss;
    private $create_at;
    private $update_at;

    public function getId() {
        return $this->id; 
    }

    public function getFk_procedure() {
        return $this->fk_procedure; 
    }

    public function getFk_tuss() {
        return $this->fk_tuss; 
    }

    public function getCreate_at() {
        return $this->create_at; 
    }

    public function getUpdate_at() {
        return $this->update_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setFk_procedure($fk_procedure) {
        $this->fk_procedure = $fk_procedure; 
    }

    public function setFk_tuss($fk_tuss) {
        $this->fk_tuss = $fk_tuss; 
    }

    public function setCreate_at($create_at) {
        $this->create_at = $create_at; 
    }

    public function setUpdate_at($update_at) {
        $this->update_at = $update_at; 
    }


}