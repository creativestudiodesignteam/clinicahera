<?php 

class Addresses {

    private $id;
    private $postcode;
    private $state;
    private $city;
    private $neighborhood;
    private $street;
    private $number;
    private $complement;
    private $created_at;
    private $updated_at;

    public function getId() {
        return $this->id; 
    }

    public function getPostcode() {
        return $this->postcode; 
    }

    public function getState() {
        return $this->state; 
    }

    public function getCity() {
        return $this->city; 
    }

    public function getNeighborhood() {
        return $this->neighborhood; 
    }

    public function getStreet() {
        return $this->street; 
    }

    public function getNumber() {
        return $this->number; 
    }

    public function getComplement() {
        return $this->complement; 
    }

    public function getCreated_at() {
        return $this->created_at; 
    }

    public function getUpdated_at() {
        return $this->updated_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setPostcode($postcode) {
        $this->postcode = $postcode; 
    }

    public function setState($state) {
        $this->state = $state; 
    }

    public function setCity($city) {
        $this->city = $city; 
    }

    public function setNeighborhood($neighborhood) {
        $this->neighborhood = $neighborhood; 
    }

    public function setStreet($street) {
        $this->street = $street; 
    }

    public function setNumber($number) {
        $this->number = $number; 
    }

    public function setComplement($complement) {
        $this->complement = $complement; 
    }

    public function setCreated_at($created_at) {
        $this->created_at = $created_at; 
    }

    public function setUpdated_at($updated_at) {
        $this->updated_at = $updated_at; 
    }


}