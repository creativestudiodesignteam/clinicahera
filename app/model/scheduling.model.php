<?php

class Scheduling
{

    private $id;
    private $email;
    private $tellphone;
    private $value;
    private $type;
    private $reason;
    private $situation;
    private $color;
    private $start;
    private $end;
    private $fk_medic;
    private $fk_patients;
    private $create_at;
    private $update_at;

    public function getId()
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getTellphone()
    {
        return $this->tellphone;
    }
    public function getValue()
    {
        return $this->value;
    }
    public function getType()
    {
        return $this->type;
    }

    public function getReason()
    {
        return $this->reason;
    }

    public function getSituation()
    {
        return $this->situation;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function getStart()
    {
        return $this->start;
    }

    public function getEnd()
    {
        return $this->end;
    }

    public function getFk_medic()
    {
        return $this->fk_medic;
    }

    public function getFk_patients()
    {
        return $this->fk_patients;
    }

    public function getCreate_at()
    {
        return $this->create_at;
    }

    public function getUpdate_at()
    {
        return $this->update_at;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setTellphone($tellphone)
    {
        $this->tellphone = $tellphone;
    }
    public function setValue($value)
    {
        $this->value = $value;
    }
    public function setType($type)
    {
        $this->type = $type;
    }

    public function setReason($reason)
    {
        $this->reason = $reason;
    }

    public function setSituation($situation)
    {
        $this->situation = $situation;
    }

    public function setColor($color)
    {
        $this->color = $color;
    }

    public function setStart($start)
    {
        $this->start = $start;
    }

    public function setEnd($end)
    {
        $this->end = $end;
    }

    public function setFk_medic($fk_medic)
    {
        $this->fk_medic = $fk_medic;
    }

    public function setFk_patients($fk_patients)
    {
        $this->fk_patients = $fk_patients;
    }

    public function setCreate_at($create_at)
    {
        $this->create_at = $create_at;
    }

    public function setUpdate_at($update_at)
    {
        $this->update_at = $update_at;
    }
}
