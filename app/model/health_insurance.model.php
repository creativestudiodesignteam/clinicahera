<?php 

class Health_insurance {

    private $id;
    private $name;
    private $email;
    private $site;
    private $cnpj;
    private $login;
    private $password;
    private $tellphone_commercial;
    private $tellphone_residential;
    private $table_references;
    private $register_ans;
    private $fk_addresses;
    private $create_at;
    private $update_at;

    public function getId() {
        return $this->id; 
    }

    public function getName() {
        return $this->name; 
    }

    public function getEmail() {
        return $this->email; 
    }

    public function getSite() {
        return $this->site; 
    }

    public function getCnpj() {
        return $this->cnpj; 
    }

    public function getLogin() {
        return $this->login; 
    }

    public function getPassword() {
        return $this->password; 
    }

    public function getTellphone_commercial() {
        return $this->tellphone_commercial; 
    }

    public function getTellphone_residential() {
        return $this->tellphone_residential; 
    }

    public function getTable_references() {
        return $this->table_references; 
    }

    public function getRegister_ans() {
        return $this->register_ans; 
    }

    public function getFk_addresses() {
        return $this->fk_addresses; 
    }

    public function getCreate_at() {
        return $this->create_at; 
    }

    public function getUpdate_at() {
        return $this->update_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setName($name) {
        $this->name = $name; 
    }

    public function setEmail($email) {
        $this->email = $email; 
    }

    public function setSite($site) {
        $this->site = $site; 
    }

    public function setCnpj($cnpj) {
        $this->cnpj = $cnpj; 
    }

    public function setLogin($login) {
        $this->login = $login; 
    }

    public function setPassword($password) {
        $this->password = $password; 
    }

    public function setTellphone_commercial($tellphone_commercial) {
        $this->tellphone_commercial = $tellphone_commercial; 
    }

    public function setTellphone_residential($tellphone_residential) {
        $this->tellphone_residential = $tellphone_residential; 
    }

    public function setTable_references($table_references) {
        $this->table_references = $table_references; 
    }

    public function setRegister_ans($register_ans) {
        $this->register_ans = $register_ans; 
    }

    public function setFk_addresses($fk_addresses) {
        $this->fk_addresses = $fk_addresses; 
    }

    public function setCreate_at($create_at) {
        $this->create_at = $create_at; 
    }

    public function setUpdate_at($update_at) {
        $this->update_at = $update_at; 
    }


}