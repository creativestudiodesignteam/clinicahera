<?php 

class Consultancies {

    private $id;
    private $name;
    private $fk_clinics;
    private $create_at;
    private $update_at;

    public function getId() {
        return $this->id; 
    }

    public function getName() {
        return $this->name; 
    }

    public function getFk_clinics() {
        return $this->fk_clinics; 
    }

    public function getCreate_at() {
        return $this->create_at; 
    }

    public function getUpdate_at() {
        return $this->update_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setName($name) {
        $this->name = $name; 
    }

    public function setFk_clinics($fk_clinics) {
        $this->fk_clinics = $fk_clinics; 
    }

    public function setCreate_at($create_at) {
        $this->create_at = $create_at; 
    }

    public function setUpdate_at($update_at) {
        $this->update_at = $update_at; 
    }


}