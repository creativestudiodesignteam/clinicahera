<?php 

class Group {

    private $id;
    private $name;
    private $created_at;
    private $update_at;

    public function getId() {
        return $this->id; 
    }

    public function getName() {
        return $this->name; 
    }

    public function getCreated_at() {
        return $this->created_at; 
    }

    public function getUpdate_at() {
        return $this->update_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setName($name) {
        $this->name = $name; 
    }

    public function setCreated_at($created_at) {
        $this->created_at = $created_at; 
    }

    public function setUpdate_at($update_at) {
        $this->update_at = $update_at; 
    }


}