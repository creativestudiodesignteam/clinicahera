<?php 

class Office {

    private $id;
    private $dayWeek;
    private $timeStart;
    private $timeEnd;
    private $fk_medics;
    private $create_at;
    private $update_at;

    public function getId() {
        return $this->id; 
    }

    public function getDayWeek() {
        return $this->dayWeek; 
    }

    public function getTimeStart() {
        return $this->timeStart; 
    }

    public function getTimeEnd() {
        return $this->timeEnd; 
    }

    public function getFk_medics() {
        return $this->fk_medics; 
    }

    public function getCreate_at() {
        return $this->create_at; 
    }

    public function getUpdate_at() {
        return $this->update_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setDayWeek($dayWeek) {
        $this->dayWeek = $dayWeek; 
    }

    public function setTimeStart($timeStart) {
        $this->timeStart = $timeStart; 
    }

    public function setTimeEnd($timeEnd) {
        $this->timeEnd = $timeEnd; 
    }

    public function setFk_medics($fk_medics) {
        $this->fk_medics = $fk_medics; 
    }

    public function setCreate_at($create_at) {
        $this->create_at = $create_at; 
    }

    public function setUpdate_at($update_at) {
        $this->update_at = $update_at; 
    }


}