<?php 

class Patients {

    private $id;
    private $name;
    private $email;
    private $rg;
    private $cpf;
    private $emitting_organ;
    private $sexo;
    private $birth_place;
    private $birthday;
    private $groups_references;
    private $situation;
    private $specialty;
    private $tellphone_commercial;
    private $tellphone_residential;
    private $name_message;
    private $cellphone;
    private $fk_addresses;
    private $fk_medic;
    private $fk_plan;
    private $create_at;
    private $update_at;

    public function getId() {
        return $this->id; 
    }

    public function getName() {
        return $this->name; 
    }

    public function getEmail() {
        return $this->email; 
    }

    public function getRg() {
        return $this->rg; 
    }

    public function getCpf() {
        return $this->cpf; 
    }

    public function getEmitting_organ() {
        return $this->emitting_organ; 
    }

    public function getSexo() {
        return $this->sexo; 
    }

    public function getBirth_place() {
        return $this->birth_place; 
    }

    public function getBirthday() {
        return $this->birthday; 
    }

    public function getGroups_references() {
        return $this->groups_references; 
    }

    public function getSituation() {
        return $this->situation; 
    }

    public function getSpecialty() {
        return $this->specialty; 
    }

    public function getTellphone_commercial() {
        return $this->tellphone_commercial; 
    }

    public function getTellphone_residential() {
        return $this->tellphone_residential; 
    }

    public function getName_message() {
        return $this->name_message; 
    }

    public function getCellphone() {
        return $this->cellphone; 
    }

    public function getFk_addresses() {
        return $this->fk_addresses; 
    }

    public function getFk_medic() {
        return $this->fk_medic; 
    }

    public function getFk_plan() {
        return $this->fk_plan; 
    }

    public function getCreate_at() {
        return $this->create_at; 
    }

    public function getUpdate_at() {
        return $this->update_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setName($name) {
        $this->name = $name; 
    }

    public function setEmail($email) {
        $this->email = $email; 
    }

    public function setRg($rg) {
        $this->rg = $rg; 
    }

    public function setCpf($cpf) {
        $this->cpf = $cpf; 
    }

    public function setEmitting_organ($emitting_organ) {
        $this->emitting_organ = $emitting_organ; 
    }

    public function setSexo($sexo) {
        $this->sexo = $sexo; 
    }

    public function setBirth_place($birth_place) {
        $this->birth_place = $birth_place; 
    }

    public function setBirthday($birthday) {
        $this->birthday = $birthday; 
    }

    public function setGroups_references($groups_references) {
        $this->groups_references = $groups_references; 
    }

    public function setSituation($situation) {
        $this->situation = $situation; 
    }

    public function setSpecialty($specialty) {
        $this->specialty = $specialty; 
    }

    public function setTellphone_commercial($tellphone_commercial) {
        $this->tellphone_commercial = $tellphone_commercial; 
    }

    public function setTellphone_residential($tellphone_residential) {
        $this->tellphone_residential = $tellphone_residential; 
    }

    public function setName_message($name_message) {
        $this->name_message = $name_message; 
    }

    public function setCellphone($cellphone) {
        $this->cellphone = $cellphone; 
    }

    public function setFk_addresses($fk_addresses) {
        $this->fk_addresses = $fk_addresses; 
    }

    public function setFk_medic($fk_medic) {
        $this->fk_medic = $fk_medic; 
    }

    public function setFk_plan($fk_plan) {
        $this->fk_plan = $fk_plan; 
    }

    public function setCreate_at($create_at) {
        $this->create_at = $create_at; 
    }

    public function setUpdate_at($update_at) {
        $this->update_at = $update_at; 
    }


}