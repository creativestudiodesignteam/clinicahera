<?php 

class Hospital {

    private $id;
    private $name;
    private $email;
    private $reason_social;
    private $cnpj;
    private $tellphone;
    private $fk_addresses;
    private $create_at;
    private $update_at;

    public function getId() {
        return $this->id; 
    }

    public function getName() {
        return $this->name; 
    }

    public function getEmail() {
        return $this->email; 
    }

    public function getReason_social() {
        return $this->reason_social; 
    }

    public function getCnpj() {
        return $this->cnpj; 
    }

    public function getTellphone() {
        return $this->tellphone; 
    }

    public function getFk_addresses() {
        return $this->fk_addresses; 
    }

    public function getCreate_at() {
        return $this->create_at; 
    }

    public function getUpdate_at() {
        return $this->update_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setName($name) {
        $this->name = $name; 
    }

    public function setEmail($email) {
        $this->email = $email; 
    }

    public function setReason_social($reason_social) {
        $this->reason_social = $reason_social; 
    }

    public function setCnpj($cnpj) {
        $this->cnpj = $cnpj; 
    }

    public function setTellphone($tellphone) {
        $this->tellphone = $tellphone; 
    }

    public function setFk_addresses($fk_addresses) {
        $this->fk_addresses = $fk_addresses; 
    }

    public function setCreate_at($create_at) {
        $this->create_at = $create_at; 
    }

    public function setUpdate_at($update_at) {
        $this->update_at = $update_at; 
    }


}