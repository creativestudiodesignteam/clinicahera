<?php 

class Table_tuss {

    private $id;
    private $cod;
    private $group_tuss;
    private $sub_group;
    private $procedure_tuss;
    private $create_at;
    private $update_at;

    public function getId() {
        return $this->id; 
    }

    public function getCod() {
        return $this->cod; 
    }

    public function getGroup_tuss() {
        return $this->group_tuss; 
    }

    public function getSub_group() {
        return $this->sub_group; 
    }

    public function getProcedure_tuss() {
        return $this->procedure_tuss; 
    }

    public function getCreate_at() {
        return $this->create_at; 
    }

    public function getUpdate_at() {
        return $this->update_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setCod($cod) {
        $this->cod = $cod; 
    }

    public function setGroup_tuss($group_tuss) {
        $this->group_tuss = $group_tuss; 
    }

    public function setSub_group($sub_group) {
        $this->sub_group = $sub_group; 
    }

    public function setProcedure_tuss($procedure_tuss) {
        $this->procedure_tuss = $procedure_tuss; 
    }

    public function setCreate_at($create_at) {
        $this->create_at = $create_at; 
    }

    public function setUpdate_at($update_at) {
        $this->update_at = $update_at; 
    }


}