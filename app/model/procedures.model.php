<?php 

class Procedures {

    private $id;
    private $name;
    private $value;
    private $date;
    private $hours;
    private $duration;
    private $table_tuss;
    private $situation;
    private $obs;
    private $dateSendDocs;
    private $material;
    private $indication;
    private $cid_10;
    private $type_charge;
    private $fk_team;
    private $fk_hospital;
    private $fk_patient;
    private $fk_medics;
    private $create_at;
    private $update_at;

    public function getId() {
        return $this->id; 
    }

    public function getName() {
        return $this->name; 
    }

    public function getValue() {
        return $this->value; 
    }

    public function getDate() {
        return $this->date; 
    }

    public function getHours() {
        return $this->hours; 
    }

    public function getDuration() {
        return $this->duration; 
    }

    public function getTable_tuss() {
        return $this->table_tuss; 
    }

    public function getSituation() {
        return $this->situation; 
    }

    public function getObs() {
        return $this->obs; 
    }

    public function getDateSendDocs() {
        return $this->dateSendDocs; 
    }

    public function getMaterial() {
        return $this->material; 
    }

    public function getIndication() {
        return $this->indication; 
    }

    public function getCid_10() {
        return $this->cid_10; 
    }

    public function getType_charge() {
        return $this->type_charge; 
    }

    public function getFk_team() {
        return $this->fk_team; 
    }

    public function getFk_hospital() {
        return $this->fk_hospital; 
    }

    public function getFk_patient() {
        return $this->fk_patient; 
    }

    public function getFk_medics() {
        return $this->fk_medics; 
    }

    public function getCreate_at() {
        return $this->create_at; 
    }

    public function getUpdate_at() {
        return $this->update_at; 
    }

    public function setId($id) {
        $this->id = $id; 
    }

    public function setName($name) {
        $this->name = $name; 
    }

    public function setValue($value) {
        $this->value = $value; 
    }

    public function setDate($date) {
        $this->date = $date; 
    }

    public function setHours($hours) {
        $this->hours = $hours; 
    }

    public function setDuration($duration) {
        $this->duration = $duration; 
    }

    public function setTable_tuss($table_tuss) {
        $this->table_tuss = $table_tuss; 
    }

    public function setSituation($situation) {
        $this->situation = $situation; 
    }

    public function setObs($obs) {
        $this->obs = $obs; 
    }

    public function setDateSendDocs($dateSendDocs) {
        $this->dateSendDocs = $dateSendDocs; 
    }

    public function setMaterial($material) {
        $this->material = $material; 
    }

    public function setIndication($indication) {
        $this->indication = $indication; 
    }

    public function setCid_10($cid_10) {
        $this->cid_10 = $cid_10; 
    }

    public function setType_charge($type_charge) {
        $this->type_charge = $type_charge; 
    }

    public function setFk_team($fk_team) {
        $this->fk_team = $fk_team; 
    }

    public function setFk_hospital($fk_hospital) {
        $this->fk_hospital = $fk_hospital; 
    }

    public function setFk_patient($fk_patient) {
        $this->fk_patient = $fk_patient; 
    }

    public function setFk_medics($fk_medics) {
        $this->fk_medics = $fk_medics; 
    }

    public function setCreate_at($create_at) {
        $this->create_at = $create_at; 
    }

    public function setUpdate_at($update_at) {
        $this->update_at = $update_at; 
    }


}