<?php
include 'app/model/scheduling.model.php';
include 'app/controller/schedulingDAO.php';
include 'app/model/medics.model.php';
include 'app/controller/medicsDAO.php';
include 'app/model/procedures.model.php';
include 'app/controller/proceduresDAO.php';
$scheduling = new Scheduling();
$schedulingDAO = new SchedulingDAO($db);
$procedures = new Procedures();
$proceduresDAO = new ProceduresDAO($db);
$medics = new Medics();
$medicsDAO = new MedicsDAO($db);

$listSchedule = $schedulingDAO->getByConfirmed();
$listProcedures = $proceduresDAO->getByConfirmed();
$resultado = 0;

/*$valorSomarProcedure = 0;
foreach ($listProcedures as $procedure) {
  $valorProcedure = explode('R$ ', $procedure['value']);
  $resultado = $resultado + (int) $valorProcedure[1];
}
echo ($resultado);*/

?>
<div class="bg-title">
  <h3 class="page-title">
    <i class="fa fa-user-md"></i> Financeiro
    <i class="fa fa-angle-right"></i> <span class="text-primary">Listagem</span>
    <a class="btn btn-default" style="padding: 5px 10px;width: 32px;" href="medics/create"><i class="fa fa-plus"></i></a>
  </h3>
</div>
<div class="row">
  <div class="col-md-12">
    <table class="infos-table" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Departamento</th>
          <th>Datas</th>
          <th>Valor</th>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach ($listProcedures as $procedure) {
          $valorProcedure = explode('R$ ', $procedure['value']);
          $resultado = $resultado + (int) $valorProcedure[1];
        ?>
          <tr>
            <td><?= $procedure['id'] ?></td>
            <td>Procedimentos</td>
            <td><?= $procedure['date'] ?></td>
            <td><?= $procedure['value'] ?></td>
          </tr>
        <?php } ?>
        <?php
        $resultadoSchedule = 0;
        foreach ($listSchedule as $schedule) {
          $valorSchedule = explode('R$ ', $schedule['value']);
          $resultadoSchedule = $resultadoSchedule + (int) $valorSchedule[1];
        ?>
          <tr>
            <td><?= $schedule['id'] ?></td>
            <td>Agendamento</td>
            <td><?= $schedule['start'] ?> até <?= $schedule['end'] ?></td>
            <td><?= $schedule['value'] ?></td>
          </tr>
        <?php } ?>
      </tbody>
      </tr>
    </table>
  </div>
</div>
<div class="row">
  <div class="white-box">
    <p>Valor total: <strong> R$ <?= $resultado + $resultadoSchedule ?></strong></p>
  </div>
</div>