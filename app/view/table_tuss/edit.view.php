<?php
include 'app/model/table_tuss.model.php';
include 'app/controller/table_tussDAO.php';
$table_tuss = new Table_tuss();
$table_tussDAO = new Table_tussDAO($db);


if (!empty($_GET['table_tuss_id'])) {
  $info = $table_tussDAO->getById($_GET['table_tuss_id']);

  $notify = array('', '');
  if (!empty($info)) {
    if (isset($_POST['table_tuss'])) {
      $form = array(
        $_POST['table_tuss'],
      );
      var_dump($form[0]);
      $table_tuss->setId($info['id']);
      $table_tuss->setCod($form[0]['cod']);
      $table_tuss->setGroup_tuss($form[0]['group_tuss']);
      $table_tuss->setSub_group($form[0]['sub_group']);
      $table_tuss->setProcedure_tuss($form[0]['procedure_tuss']);

      $table_tussDAO->table_tuss = $table_tuss;

      $table_tussDAO->update();

      $url->redirect('table_tuss/manage');
    }

?>
    <script src="public/assets/js/bundleselect.js"></script>
    <script src="public/assets/js/multipleselect.min.js"></script>

    <link rel="stylesheet" href="public/assets/css/multipleselect.css" />



    <div class="bg-title">
      <h3 class="page-title">
        <i class="fa fa-newspaper-o"></i> Procedimentos
        <i class="fa fa-angle-right"></i> <span class="text-primary">Adicionar</span>
        <a class="btn btn-default" style="padding: 5px 10px;width: 32px;" href="table_tuss/create"><i class="fa fa-plus"></i></a>
      </h3>
    </div>

    <div class="white-box">
      <form method="post">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3 class="page-title">Dados principais</h3>
          </div>
          <!-- //ID	Código	Grupo	Sub Grupo	Procedimento -->
          <div class="col-md-6">
            <div class="form-group">
              <label>Código *</label>
              <input type="text" class="form-control" name="table_tuss[cod]" value="<?= $info['cod'] ?>" placeholder="Digite o Código">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Grupo *</label>
              <input type="text" class="form-control" name="table_tuss[group_tuss]" value="<?= $info['group_tuss'] ?>" placeholder="Digite o Grupo">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Sub Grupo *</label>
              <input type="text" class="form-control" name="table_tuss[sub_group]" value="<?= $info['sub_group'] ?>" placeholder="Digite o sub grupo ">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Procedimento *</label>
              <input type="text" class="form-control" name="table_tuss[procedure_tuss]" value="<?= $info['procedure_tuss'] ?>" placeholder="Digite o procedimento">
            </div>
          </div>
          <div class="col-md-12" style="margin-top: 10px">
            <div class="form-group">
              <button type="submit" name="create" class="text-white btn btn-primary">Cadastrar</button>
            </div>
          </div>


          <!-- namepacientemedicohospitaltable_tusssituationobsdatehoursduration  -->
          <!-- //name value date hours duration table_tuss situation obs dateSendDocs material 
indication cid-10 type_charge fk_hospital fk_patients fk_medics -->
      </form>
    </div>
    <script>
      $(document).ready(function() {
        const table_references_dados = [{
            'codigo': '20101198',
            'name': "TEste 1 |"
          },
          {
            'codigo': '2114545',
            'name': "Teste 2 | "
          },
        ]
        for (var i = 0; table_references_dados.length > i; i++) {
          $('#table_references').append('<option value="' + table_references_dados[i]['codigo'] + '"> ' + table_references_dados[i]['name'] + ' </option>');
        }

      });

      $("#table_references").on("change", function() {
        var descricao = $("#table_references option:selected").text();
        var value = $(this).val();
        $("#register_ans").val(value)
        alert(descricao);
        $("#table_tuss").val(descricao)

      })

      $(document).ready(function() {
        $('#date_time').mask('00/00/0000 00:00:00');
        $('#cnpj').mask('00.000.000/0000-00')
        $('#crm').mask('0000 0000-0');
        $('#rg').mask('0000000000-0');
        $('#postcode').mask('00000-000');
        $('#value').mask('R$ 0000000000000000');
        $('#phone_residential').mask('(00) 0000-0000');
        $('#phone_commercial').mask('(00) 0000-0000');
        $('#phone_us').mask('(00) 0 0000-0000');
        $('#cpf').mask('000.000.000-00');

      });
    </script>
<?php

  } else {
    $url->redirect('error');
  }
} else {
  $url->redirect('home');
}
?>