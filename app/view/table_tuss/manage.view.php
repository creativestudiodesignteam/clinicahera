<?php
include 'app/model/table_tuss.model.php';
include 'app/controller/table_tussDAO.php';

$table_tuss = new Table_tuss();
$table_tussDAO = new Table_tussDAO($db);
$list = $table_tussDAO->listAll();

if (!empty($_GET['remove_id'])) {
  $table_tuss->setId($_GET['remove_id']);
  $table_tussDAO->table_tuss = $table_tuss;
  $table_tussDAO->delete();
  $url->redirect('table_tuss/manage');
}
//ID	Código	Grupo	Sub Grupo	Procedimento
?>
<div class="bg-title">
  <h3 class="page-title">
    <i class="fa fa-newspaper-o"></i> Procedimentos
    <i class="fa fa-angle-right"></i> <span class="text-primary">Gerenciar</span>
    <a class="btn btn-default" style="padding: 5px 10px;width: 32px;" href="table_tuss/create"><i class="fa fa-plus"></i></a>
  </h3>
</div>
<div class="row">
  <div class="col-md-12">
    <table class="infos-table" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Código</th>
          <th>Grupo</th>
          <th>Sub Grupo</th>
          <th>Procedimento</th>
          <th style="width: 64px;">Mais</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($list as $l) { ?>
          <tr>
            <td><?= $l['id'] ?></td>
            <td><?= $l['cod'] ?></td>
            <td><?= $l['group_tuss'] ?></td>
            <td><?= $l['sub_group'] ?></td>
            <td><?= $l['procedure_tuss'] ?></td>
            <td>
              <div class="row">
                <div class="col-md-5">
                  <a class="btn btn-default" style="padding: 5px;width: 32px;" href="./table_tuss/edit&table_tuss_id=<?= $l['id'] ?>"><i class="fa fa-edit"></i></a>
                </div>
                <div class="col-md-5">
                  <a style="padding: 5px;width: 32px;" href="./table_tuss/manage&remove_id=<?= $l['id'] ?>" class="btn btn-danger"><i class="fa fa-times"></i></a>
                </div>
              </div>
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>