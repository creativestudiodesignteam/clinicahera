<?php
include 'app/model/procedures.model.php';
include 'app/controller/proceduresDAO.php';
include 'app/model/health_insurance.model.php';
include 'app/controller/health_insuranceDAO.php';
include 'app/model/team_procedures.model.php';
include 'app/controller/team_proceduresDAO.php';
$team_procedures = new Team_procedures();
$team_proceduresDAO = new Team_proceduresDAO($db);

$health_insurance = new Health_insurance();
$health_insuranceDAO = new Health_insuranceDAO($db);

$procedures = new Procedures();
$proceduresDAO = new ProceduresDAO($db);

$list = $proceduresDAO->listAll();
if (isset($_POST['team'])) {
  $form = array(
    $_POST['team'],
  );
  $team_procedures->setName($form[0]['name']);
  $team_procedures->setSpecialty($form[0]['specialty']);
  $team_procedures->setPercentage($form[0]['percent']);
  $team_procedures->setFk_procedure($form[0]['fk_procedure']);
  $team_proceduresDAO->team_procedures = $team_procedures;
  $fk_team = $team_proceduresDAO->insert();
  if (!empty($fk_team)) {
    $procedures->setId($form[0]['fk_procedure']);
    $proceduresDAO->procedures = $procedures;
    $proceduresDAO->update_team();
  }
  $url->redirect('procedures/manage');
}
if (!empty($_GET['remove_id'])) {
  $procedures->setId($_GET['remove_id']);
  $proceduresDAO->procedures = $procedures;
  $proceduresDAO->delete();
  $url->redirect('procedures/manage');
}
?>

<div class="bg-title">
  <h3 class="page-title">
    <i class="fa fa-newspaper-o"></i> Procedimentos
    <i class="fa fa-angle-right"></i> <span class="text-primary">Gerenciar</span>
    <a class="btn btn-default" style="padding: 5px 10px;width: 32px;" href="procedures/create"><i class="fa fa-plus"></i></a>
  </h3>
</div>

<div class="row">
  <div class="col-md-12">
    <table class="infos-table" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nome</th>
          <th>valor</th>
          <th>Data</th>
          <th>Situação</th>
          <th style="width: 64px;">Mais</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($list as $l) {
          if ($l['situation'] == 'Operado' and $l['fk_team'] == false) {
        ?>
            <input type="hidden" value="active?<?= $l['id'] ?>?<?= $l['name'] ?> " id="modal-active">
          <?php } ?>

          <tr>
            <td><?= $l['id'] ?></td>
            <td><?= $l['name'] ?></td>
            <td><?= $l['value'] ?></td>
            <td><?= $l['date'] ?></td>
            <td><?= $l['situation'] ?></td>
            <td>
              <div class="row">
                <div class="col-md-5">
                  <a class="btn btn-default" style="padding: 5px;width: 32px;" href="./procedures/edit&procedures_id=<?= $l['id'] ?>"><i class="fa fa-edit"></i></a>
                </div>
                <div class="col-md-5">
                  <a style="padding: 5px;width: 32px;" href="./procedures/manage&remove_id=<?= $l['id'] ?>" class="btn btn-danger"><i class="fa fa-times"></i></a>
                </div>
              </div>
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>

  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="modal" id="team_create" tabindex="-1" role="dialog">
      <form method="post">
        <div class="modal-dialog  modal-xl" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h2 class="modal-title"><strong>Cadastro da Equipe</strong></h2>
              <h4 class="modal-title" id="nameProcedure"></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body team-modal">
              <input type="hidden" id="fk_procedure" class="form-control" name="team[fk_procedure]">
              <div class="row">
                <div class="col-md-2" id='dep_fc' style="padding-top:20px;">
                  <button type="button" class="btn btn-danger text-dark remove"><i class="fa fa-trash" style="color: white"></i></button>
                </div>
                <div class="col-md-3">
                  <div class="form-group">

                    <label for="patients">Nome</label>
                    <input type="text" class="form-control" name="team[name]" placeholder="Digite o Nome ">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="patients">Especialidade</label>
                    <input type="text" class="form-control" name="team[specialty]" placeholder="Digite o Nome ">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label for="patients">Porcentagem</label>
                    <input type="text" class="form-control percent" name="team[percent]" placeholder="Digite o Nome ">
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
              <button type="button" class="btn btn-primary" id="addinpts">Adicionar</button>
              <button type="submit" class="btn btn-primary">Cadastrar</button>
            </div>
          </div>
      </form>
    </div>
  </div>

</div>





<script>
  $(document).ready(function() {
    var active = $('#modal-active').val();
    active = active.split('?');
    if (active[0] == 'active') {
      document.getElementById('fk_procedure').value = active[1];
      $("#nameProcedure").append("<strong>" + active[2] + "</strong>");
      $('#team_create').modal('show');
    }
  });

  $("#addinpts").click(function() {
    var newRow = "<div class='row'><div class='col-md-2' id='dep_fc' style='padding-top:20px;'><button type='button' class='btn btn-danger text-dark remove'><i class='fa fa-trash' style='color: white'></i></button></div><div class='col-md-3'><div class='form-group'><label for='patients'>Nome</label><input type='text' class='form-control' name='team[name]' placeholder='Digite o Nome '></div></div><div class='col-md-3'><div class='form-group'><label for='patients'>Especialidade</label><input type='text' class='form-control' name='team[name]' placeholder='Digite o Nome '></div></div><div class='col-md-3'><div class='form-group'><label for='patients'>Porcentagem</label><input type='text' class='form-control' name='team[name]' placeholder='Digite o Nome '></div></div></div>";
    $(".team-modal").append(newRow);
  });

  $(document).on('click', 'button.remove', function() {
    $(this).closest('.row').remove();
  });
  $(document).ready(function() {
    $('#date_time').mask('00/00/0000 00:00:00');
    $('#cnpj').mask('00.000.000/0000-00')
    $('#crm').mask('0000 0000-0');
    $('#rg').mask('0000000000-0');
    $('#postcode').mask('00000-000');
    $('#phone_residential').mask('(00) 0000-0000');
    $('#phone_commercial').mask('(00) 0000-0000');
    $('#phone_us').mask('(00) 0 0000-0000');
    $('.percent').mask('000%', {
      reverse: true
    })

  });
</script>