<?php
include 'app/model/procedures.model.php';
include 'app/controller/proceduresDAO.php';
include 'app/model/patients.model.php';
include 'app/controller/patientsDAO.php';
include 'app/model/medics.model.php';
include 'app/controller/medicsDAO.php';
include 'app/model/hospital.model.php';
include 'app/controller/hospitalDAO.php';
include 'app/model/table_tuss.model.php';
include 'app/controller/table_tussDAO.php';
include 'app/model/tuss_procedure.model.php';
include 'app/controller/tuss_procedureDAO.php';
$tuss_procedure = new Tuss_procedure();
$tuss_procedureDAO = new Tuss_procedureDAO($db);

$table_tuss = new Table_tuss();
$table_tussDAO = new Table_tussDAO($db);

$procedures = new Procedures();
$proceduresDAO = new ProceduresDAO($db);

$patients = new Patients();
$patientsDAO = new PatientsDAO($db);

$medics = new Medics();
$medicsDAO = new MedicsDAO($db);

$hospital = new Hospital();
$hospitalDAO = new HospitalDAO($db);

$listTable = $table_tussDAO->listAll();
$listhospital = $hospitalDAO->listAll();
$listmedics = $medicsDAO->listAll();
$listPatients = $patientsDAO->listAll();

if (!empty($_GET['procedures_id'])) {
  $info = $proceduresDAO->getById($_GET['procedures_id']);
  $listTable = $table_tussDAO->listAll();
  $listhospital = $hospitalDAO->listAll();
  $listmedics = $medicsDAO->listAll();
  $listPatients = $patientsDAO->listAll();


  $tussTable = $tuss_procedureDAO->listAll();

  $tussExists = $tuss_procedureDAO->getByIdProcedure($info['id']);
  $notify = array('', '');
  if (!empty($info)) {
    if (isset($_POST['procedures'])) {
      $form = array(
        $_POST['procedures'],
        $_POST['multiselect'],
      );
      $procedures->setId($info['id']);
      $procedures->setName($form[0]['name']);
      $procedures->setDate($form[0]['date']);
      $procedures->setDateSendDocs($form[0]['dateSendDocs']);
      $procedures->setHours($form[0]['hours']);
      $procedures->setDuration($form[0]['duration']);
      $procedures->setMaterial($form[0]['material']);
      $procedures->setValue($form[0]['valueT']);
      $procedures->setIndication($form[0]['indication']);
      $procedures->setSituation($form[0]['situation']);
      $procedures->setObs($form[0]['obs']);
      $procedures->setCid_10($form[0]['cid_10']);
      $procedures->setTable_tuss(null);
      $procedures->setType_charge($form[0]['type_charge']);
      $procedures->setFk_hospital($form[0]['fk_hospital']);
      $procedures->setFk_medics($form[0]['fk_medics']);
      $procedures->setFk_patient($form[0]['fk_patients']);

      $proceduresDAO->procedures = $procedures;

      $proceduresDAO->update();

      if (!empty($info['id'])) {


        foreach ($tussTable as $table) {
          $tuss_procedure->setId($table['id']);
          $tuss_procedureDAO->tuss_procedure = $tuss_procedure;
          $tuss_procedureDAO->delete();
        }
        foreach ($form[1] as $tuss) {
          $tuss_procedure->setFk_procedure($info['id']);
          $tuss_procedure->setFk_tuss($tuss);
          $tuss_procedureDAO->tuss_procedure = $tuss_procedure;
          $tuss_procedureDAO->insert();
        }
        $url->redirect('procedures/manage&create=successes');
      }

      $url->redirect('procedures/manage');
    }



?>
    <script src="public/assets/js/bundleselect.js"></script>
    <script src="public/assets/js/multipleselect.min.js"></script>

    <link rel="stylesheet" href="public/assets/css/multipleselect.css" />



    <div class="bg-title">
      <h3 class="page-title">
        <i class="fa fa-newspaper-o"></i> Procedimentos
        <i class="fa fa-angle-right"></i> <span class="text-primary">Adicionar</span>
        <a class="btn btn-default" style="padding: 5px 10px;width: 32px;" href="procedures/create"><i class="fa fa-plus"></i></a>
      </h3>
    </div>

    <div class="white-box">
      <form method="post">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3 class="page-title">Dados principais</h3>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Hospital *</label>
              <select name="procedures[fk_hospital]" id="fk_hospital" class="form-control">
                <?php if (!empty($info['fk_hospital'])) { ?>
                  <?php $listHospitalT = $hospitalDAO->getById($info['fk_hospital']) ?>
                  <option selected value="<?= $info['fk_hospital'] ?>">-- <?= $listHospitalT['name'] ?> --</option>
                <?php } else { ?>
                  <option selected disabled>-- Selecione uma opção --</option>
                <?php } ?>
                <?php foreach ($listhospital as $hospitalList) { ?>
                  <option value="<?= $hospitalList['id'] ?>"><?= $hospitalList['name'] ?></option>
                <?php } ?>

              </select>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Nome *</label>
              <input type="text" class="form-control" name="procedures[name]" value="<?= !empty($info['name']) ? $info['name'] : "" ?>" placeholder="Digite o nome do procedimento">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Pacientes *</label>
              <select name="procedures[fk_patients]" id="fk_patients" class="form-control">
                <?php if (!empty($info['fk_patient'])) { ?>
                  <?php $listPatient = $patientsDAO->getById($info['fk_patient']) ?>
                  <option selected value="<?= $info['fk_patient'] ?>">-- <?= $listPatient['name'] ?> --</option>
                <?php } else { ?>
                  <option selected disabled>-- Selecione uma opção --</option>
                <?php } ?>

                <?php foreach ($listPatients as $patientsList) { ?>
                  <option value="<?= $patientsList['id'] ?>"><?= $patientsList['name'] ?></option>
                <?php } ?>

              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Médico Responsavel *</label>
              <select name="procedures[fk_medics]" id="fk_medics" class="form-control">

                <?php if (!empty($info['fk_medics'])) { ?>
                  <?php $listMedics = $medicsDAO->getById($info['fk_medics']) ?>
                  <option selected value="<?= $info['fk_medics'] ?>">-- <?= $listMedics['name'] ?> --</option>
                <?php } else { ?>
                  <option selected disabled>-- Selecione uma opção --</option>
                <?php } ?>


                <?php foreach ($listmedics as $medicsList) { ?>
                  <option value="<?= $medicsList['id'] ?>"><?= $medicsList['name'] ?></option>
                <?php } ?>

              </select>
            </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3 class="page-title">Dados Procedimentos</h3>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Table Tuss*</label>
              <select class="form-control selectpicker" name="multiselect[]" id="" multiple data-live-search="true">

                <?php foreach ($tussExists as $tuss) {
                  $result = $table_tussDAO->getById($tuss['fk_tuss']); 
                  
                  ?>
                  <option selected value="<?= $result['id'] ?>">-- <?= $result['cod'] ?> | <?= $result['procedure_tuss'] ?> --</option>

                <?php } ?>
                <?php foreach ($listTable as $table) {
                ?>
                  <option value="<?= $table['id'] ?>"> <?= $table['cod'] ?> | <?= $table['procedure_tuss'] ?></option>
                <?php } ?>
              </select>

            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Data do Procedimentos</label>
              <input type="date" class="form-control" value="<?= !empty($info['date']) ? $info['date'] : "" ?>" name="procedures[date]">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Hora do Procedimentos</label>
              <input type="time" class="form-control" value="<?= !empty($info['hours']) ? $info['hours'] : "00:00" ?>" name="procedures[hours]">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Duração do Procedimento</label>
              <input type="time" class="form-control" value="<?= !empty($info['duration']) ? $info['duration'] : "00:00" ?>" name="procedures[duration]">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Situação*</label>
              <select name="procedures[situation]" class="form-control">

                <option selected disabled> -- Selecione uma Opção --</option>
                <?php if (!empty($info['situation'])) { ?>
                  <option selected value="<?= $info['situation'] ?>">-- <?= $info['situation'] ?> --</option>
                <?php } else { ?>
                  <option selected disabled>-- Selecione uma opção --</option>
                <?php } ?>

                <option value="Data Virtual">Data Virtual</option>
                <option value="Documentos Enviados">Documentos Enviados</option>
                <option value="Agendado - Aguardando Autorizado">Agendado - Aguardando Autorizado</option>
                <option value="Autorizado">Autorizado</option>
                <option value="Cancelado">Cancelado</option>
                <option value="Operado">Operado</option>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Data de Envio dos Documentos</label>
              <input type="date" class="form-control" value="<?= !empty($info['dateSendDocs']) ? $info['dateSendDocs'] : "" ?>" name="procedures[dateSendDocs]">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Indicação Clinica</label>
              <input type="text" class="form-control" value="<?= !empty($info['indication']) ? $info['indication'] : "" ?>" name="procedures[indication]" placeholder="Digite a indicação Clinica">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Cid-10</label>
              <input type="text" class="form-control" value="<?= !empty($info['cid_10']) ? $info['cid_10'] : "" ?>" name="procedures[cid_10]" placeholder="Digite o Cid-10">
            </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3 class="page-title">Dados Financeiros</h3>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Valor do procedimento *</label>
              <input type="text" class="form-control" id="value" value="<?= !empty($info['value']) ? $info['value'] : "" ?>" name="procedures[valueT]" placeholder="Digite o valor do procedimento">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Tipo de Pagamento</label>
              <select name="procedures[type_charge]" class="form-control">

                <option selected disabled> -- Selecione uma Opção --</option>
                <?php if (!empty($info['type_charge'])) { ?>
                  <option selected value="<?= $info['type_charge'] ?>">-- <?= $info['type_charge'] ?> --</option>
                <?php } else { ?>
                  <option selected disabled>-- Selecione uma opção --</option>
                <?php } ?>

                <option value="Cartão de Débito">Cônvenio</option>
                <option value="Avista">Avista</option>
                <option value="Cartão de Débito">Cartão de Débito</option>
                <option value="Cartão de Crédito">Cartão de Crédito</option>
              </select>
            </div>
          </div>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3 class="page-title">Descrição</h3>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Materiais (OPME)</label>
              <input type="text" class="form-control" name="procedures[material]" value="<?= !empty($info['material']) ? $info['material'] : "" ?>" placeholder="Digite a indicação Clinica">
            </div>
          </div>

          <div class="col-md-12">
            <label>Descrição*</label>
            <textarea name="procedures[obs]" class="form-control" cols="30" rows="5">
              <?= !empty($info['obs']) ? $info['obs'] : "" ?>
            </textarea>
          </div>

          <div class="col-md-12" style="margin-top: 10px">
            <div class="form-group">
              <button type="submit" name="create" class="text-white btn btn-primary">Cadastrar</button>
            </div>
          </div>


          <!-- namepacientemedicohospitaltable_tusssituationobsdatehoursduration  -->
          <!-- //name value date hours duration table_tuss situation obs dateSendDocs material 
indication cid-10 type_charge fk_hospital fk_patients fk_medics -->
      </form>
    </div>

    <script>
      $(document).ready(function() {
        const table_references_dados = [{
            'codigo': '20101198',
            'name': "TEste 1 |"
          },
          {
            'codigo': '2114545',
            'name': "Teste 2 | "
          },
        ]
        for (var i = 0; table_references_dados.length > i; i++) {
          $('#table_references').append('<option value="' + table_references_dados[i]['codigo'] + '"> ' + table_references_dados[i]['name'] + ' </option>');
        }

      });

      $("#table_references").on("change", function() {
        var descricao = $("#table_references option:selected").text();
        var value = $(this).val();
        $("#register_ans").val(value)
        alert(descricao);
        $("#table_tuss").val(descricao)

      })

      $(document).ready(function() {
        $('#date_time').mask('00/00/0000 00:00:00');
        $('#cnpj').mask('00.000.000/0000-00')
        $('#crm').mask('0000 0000-0');
        $('#rg').mask('0000000000-0');
        $('#postcode').mask('00000-000');
        $('#value').mask('R$ 0000000000000000');
        $('#phone_residential').mask('(00) 0000-0000');
        $('#phone_commercial').mask('(00) 0000-0000');
        $('#phone_us').mask('(00) 0 0000-0000');
        $('#cpf').mask('000.000.000-00');

      });
    </script>
<?php

  } else {
    $url->redirect('error');
  }
} else {
  $url->redirect('home');
}
?>