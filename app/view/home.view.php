 <!-- Inicio da body -->
 <?php
  include 'app/model/medics.model.php';
  include 'app/controller/medicsDAO.php';

  $medics = new Medics();
  $medicsDAO = new MedicsDAO($db);
  include 'app/model/patients.model.php';
  include 'app/controller/patientsDAO.php';

  $patients = new Patients();
  $patientsDAO = new PatientsDAO($db);


  include 'app/model/scheduling.model.php';
  include 'app/controller/schedulingDAO.php';

  $scheduling = new Scheduling();
  $schedulingDAO = new SchedulingDAO($db);

  $listScheduling = $schedulingDAO->listAll();
  $listMedics = $medicsDAO->listAll();
  $listPatients = $patientsDAO->listAll();
  //var_dump($_POST);
  if (isset($_POST['schedule-edit'])) {
    $form = array(
      $_POST['schedule-edit'],
    );

    //var_dump($form[0]);
    $dateStart = $form[0]['dateStart'] . 'T' . $form[0]['timeStart'];
    $dateEnd = $form[0]['dateEnd'] . 'T' . $form[0]['timeEnd'];
    $scheduling->setId($form[0]['id']);
    $scheduling->setFk_medic($form[0]['medics']);
    $scheduling->setFk_patients($form[0]['patients']);
    $scheduling->setEmail($form[0]['email']);
    $scheduling->setValue($form[0]['value']);
    $scheduling->setStart($dateStart);
    $scheduling->setEnd($dateEnd);
    $scheduling->setType($form[0]['type']);
    $scheduling->setTellphone($form[0]['tellphone']);
    $scheduling->setColor($form[0]['color']);
    $scheduling->setReason($form[0]['reason']);
    $scheduling->setSituation($form[0]['situation']);
    $schedulingDAO->scheduling = $scheduling;

    $schedulingDAO->update();
    //$url->redirect('home&edit=success');
  }
  if (isset($_POST['schedule'])) {
    $form = array(
      $_POST['schedule'],
    );
    //var_dump($form[0]);
    $dateStart = $form[0]['dateStart'] . 'T' . $form[0]['timeStart'];
    $dateEnd = $form[0]['dateEnd'] . 'T' . $form[0]['timeEnd'];
    $scheduling->setFk_medic($form[0]['medics']);
    $scheduling->setFk_patients($form[0]['patients']);
    $scheduling->setEmail($form[0]['email']);
    $scheduling->setValue($form[0]['value']);
    $scheduling->setStart($dateStart);
    $scheduling->setEnd($dateEnd);
    $scheduling->setType($form[0]['type']);
    $scheduling->setTellphone($form[0]['tellphone']);
    $scheduling->setColor($form[0]['color']);
    $scheduling->setReason($form[0]['reason']);
    $scheduling->setSituation($form[0]['situation']);
    $schedulingDAO->scheduling = $scheduling;
    $schedulingDAO->insert();
    $url->redirect('home&create=success');
  }
  ?>

 <link href='public/packages/core/main.css' rel='stylesheet' />
 <link href='public/packages/daygrid/main.css' rel='stylesheet' />
 <link href='public/packages/timegrid/main.css' rel='stylesheet' />
 <link href='public/packages/list/main.css' rel='stylesheet' />
 <script src='public/packages/core/main.js'></script>
 <script src='public/packages/interaction/main.js'></script>
 <script src='public/packages/daygrid/main.js'></script>
 <script src='public/packages/timegrid/main.js'></script>
 <script src='public/packages/list/main.js'></script>
 <script src='public/packages/core/locales/pt-br.js'></script>

 <script>
   document.addEventListener('DOMContentLoaded', function() {
     var date_Now = new Date();
     var calendarEl = document.getElementById('calendar');

     var calendar = new FullCalendar.Calendar(calendarEl, {
       plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],
       locale: 'pt-br',
       header: {
         left: 'prev,next today',
         center: 'title',
         right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
       },
       defaultView: 'timeGridWeek',
       defaultDate: date_Now,
       navLinks: true, // can click day/week names to navigate views
       eventLimit: true, // allow "more" link when too many events
       selectable: true,
       duration: '00:30:00',
       select: function(info) {
         timeStart = info.startStr.split('T')[1]
         timeStart = timeStart.split('-')[0]
         timeEnd = info.endStr.split('T')[1]
         timeEnd = timeEnd.split('-')[0]
         $("#dateStart").val(info.endStr.split('T')[0]);
         $("#timeStart").val(timeStart);
         $("#dateEnd").val(info.endStr.split('T')[0]);
         $("#timeEnd").val(timeEnd);
         $('#schedule-create').modal('show');

       },

       eventClick: function(info) {
         info.jsEvent.preventDefault();
         $(".id").val(info.event.id)
         $(".medic").val(info.event.extendedProps.medics)
         $(".medics option:contains('" + info.event.extendedProps.medics + "')").attr('selected', true);

         $(".patient").val(info.event.title)
         $(".patients option:contains('" + info.event.title + "')").attr('selected', true);

         $(".situation").val(info.event.extendedProps.situation)
         //$(".situations option:contains('" + info.event.extendedProps.situation + "')").attr('selected', true);

         $(".email").val(info.event.extendedProps.email)
         $(".tellphone").val(info.event.extendedProps.tellphone)

         $(".color-situation").val(info.event.backgroundColor)

         $(".reason").val(info.event.extendedProps.reason)

         $(".type").val(info.event.extendedProps.type)
         typeParse = info.event.extendedProps.type;

         $('.types').append($('<option>', {
           selected: true,
           disabled: true,
           value: '-- ' + info.event.extendedProps.type + ' --',
           text: info.event.extendedProps.type
         }));
         
         $('.situations').append($('<option>', {
           selected: true,
           disabled: true,
           value: '-- ' + info.event.extendedProps.situation + ' --',
           text: info.event.extendedProps.situation
         }));


         //types


         $(".value-list").val(info.event.extendedProps.value)
         //console.log(info.event.extendedProps);
         //console.log($(".types option:contains('Períodio')"));



         $(".color-squared").css('color', info.event.backgroundColor)
         $(".dateStart").val(info.event.extendedProps.dateStart.split('T')[0].split(' ')[0])
         $(".timeStart").val(info.event.extendedProps.dateStart.split('T')[0].split(' ')[1])
         $(".dateEnd").val(info.event.extendedProps.dateEnd.split('T')[0].split(' ')[0])
         $(".timeEnd").val(info.event.extendedProps.dateEnd.split('T')[0].split(' ')[1])

         //dateStart dateEnd reason type situation color-situation-select
         $('#list #id').text(info.event.id);
         $('#list').modal('show');
       },
       events: [

         <?php foreach ($listScheduling as $event) {

            $mediclist = $medicsDAO->getById($event['fk_medic']);
            $patientslist = $patientsDAO->getById($event['fk_patients']);

          ?>
           /**
           recebe os dados do agendamento
            */
           {
             "id": "<?= $event['id'] ?>",
             "title": "<?= $patientslist['name'] ?>",
             "medics": "<?= $mediclist['name'] ?>",
             "email": "<?= $event['email'] ?> ",
             "tellphone": "<?= $event['tellphone'] ?> ",
             "situation": "<?= $event['situation'] ?> ",
             "type": "<?= $event['type'] ?> ",
             "reason": "<?= $event['reason'] ?> ",
             "color": "<?= $event['color'] ?>",
             "dateStart": "<?= $event['start'] ?>",
             "dateEnd": "<?= $event['end'] ?>",
             "value": "<?= $event['value'] ?>",
             "start": "<?= $event['start'] ?>",
             "end": "<?= $event['end'] ?>",
           },
         <?php } ?>

       ],
     });

     calendar.render();
   });
 </script>

 <div class="row bg-title">
   <div class="bg-title">
     <h3 class="page-title">
       <i class="fa fa-wheelchair"></i> Agendamento
       <i class="fa fa-angle-right"></i> <span class="text-primary">Listagem</span>
     </h3>
   </div>
 </div>

 <div class="modals-used">
   <div class="modal" id="list" tabindex="-1" role="dialog">
     <div class="modal-dialog" role="document">
       <div class="modal-content">
         <div class="modal-header">
           <h2 class="modal-title"><strong>Detalhes do Agendamento</strong></h2>
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true">&times;</span>
           </button>
         </div>
         <div class="modal-body">
           <div class="hidden">
             <input type="hidden" name="id" id="id">
           </div>
           <div class="listForm">
             <div class="row">
               <div class="col-md-12">
                 <div class="form-group">
                   <label for="patients">Médico</label>
                   <input type="text" name="medics" placeholder="Medic" class="form-control medic" disabled="">
                 </div>
               </div>
               <div class="col-md-12">
                 <div class="form-group">
                   <label for="patients">Pacientes</label>
                   <input type="text" name="patients" placeholder="Pacientes" class="form-control patient" disabled="">
                 </div>
               </div>
               <div class="col-md-12">
                 <div class="form-group">
                   <label for="patients">Email</label>
                   <input type="text" name="email" placeholder="Pacientes" class="form-control email" disabled="">
                 </div>
               </div>
               <div class="col-md-6">
                 <div class="form-group">
                   <label for="startDate">Data Inicial</label>
                   <input type="date" name="dateStart" class="form-control dateStart" disabled="">
                 </div>
               </div>
               <div class="col-md-6">
                 <div class="form-group">
                   <label for="startDate">Horário Inicial</label>
                   <input type="time" name="timeStart" class="form-control timeStart" disabled="">
                 </div>
               </div>

               <div class="col-md-6">
                 <div class="form-group">
                   <label for="startDate">Data de termino</label>
                   <input type="date" name="dateEnd" class="form-control dateEnd" disabled="">
                 </div>
               </div>
               <div class="col-md-6">
                 <div class="form-group">
                   <label for="startDate">Horário de Termino</label>
                   <input type="time" name="timeEnd" class="form-control timeEnd" disabled="">
                 </div>
               </div>
               <div class="col-md-12">
                 <div class="form-group">
                   <label for="">Valor (R$)</label>
                   <input type="text" class="form-control value-list" name="value" placeholder="Digite o Valor" disabled="">
                 </div>
               </div>

               <div class="col-md-12">
                 <div class="form-group">
                   <label for="patients">Telefone</label>
                   <input type="text" name="tellphone" placeholder="Telefone" class="form-control tellphone" disabled="">
                 </div>
               </div>
               <div class="col-md-12">
                 <div class="form-group">
                   <label for="">Motivo</label>
                   <input type="text" class="form-control reason" name="reason" placeholder="Digite o motivo do agendamento" disabled="">
                 </div>
               </div>
               <div class="col-md-12">
                 <label for="">Tipo</label>
                 <div class="form-group">
                   <input type="text" class="form-control type" name="type" placeholder="Digite o motivo do agendamento" disabled="">
                 </div>
               </div>
               <div class="col-md-12">
                 <label for="">Situação</label>
                 <i class="fa fa-square color-squared" aria-hidden="true"></i>
                 <input type="hidden" name="color" class="color-squared">
                 <input type="text" class="form-control situation" name="situation" placeholder="Digite o motivo do agendamento" disabled="">
               </div>


             </div>
             <div class="modal-footer">
               <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
               <button type="button" class="btn btn-warning btn-cancelar">Editar</button>
             </div>
           </div>
           <div class="editForm">
             <form method="post">
               <div class="modal-body">
                 <div class="row">
                   <div class="col-md-12">
                     <div class="form-group">
                       <input type="hidden" name="schedule-edit[id]" class="id">
                       <label for="patients">Médico</label>
                       <select name="schedule-edit[medics]" class="form-control medics">
                         <option selected disabled>-- Selecione uma opção --</option>
                         <?php foreach ($listMedics as $medicsList) { ?>
                           <option value="<?= $medicsList['id'] ?>"><?= $medicsList['name'] ?></option>
                         <?php } ?>
                       </select>
                     </div>
                   </div>
                   <div class="col-md-12">
                     <div class="form-group">
                       <label for="patients">Pacientes</label>
                       <select name="schedule-edit[patients]" class="form-control patients">
                         <option selected disabled>-- Selecione uma opção --</option>
                         <?php foreach ($listPatients as $patientsList) { ?>
                           <option value="<?= $patientsList['id'] ?>"><?= $patientsList['name'] ?></option>
                         <?php } ?>
                       </select>
                     </div>
                   </div>
                   <div class="col-md-12">
                     <div class="form-group">
                       <label for="Email">Email</label>
                       <input type="text" name="schedule-edit[email]" placeholder="Email" class="form-control email">
                     </div>
                   </div>
                   <div class="col-md-6">
                     <div class="form-group">
                       <label for="startDate">Data Inicial</label>
                       <input type="date" name="schedule-edit[dateStart]" placeholder="Date Start" class="form-control dateStart">
                     </div>
                   </div>
                   <div class="col-md-6">
                     <div class="form-group">
                       <label for="startDate">Horário Inicial</label>
                       <input type="time" name="schedule-edit[timeStart]" placeholder="Date Start" class="form-control timeStart">
                     </div>
                   </div>
                   <div class="col-md-6">
                     <div class="form-group">
                       <label for="startDate">Data de termino</label>
                       <input type="date" name="schedule-edit[dateEnd]" placeholder="Date Start" class="form-control dateEnd">
                     </div>
                   </div>
                   <div class="col-md-6">
                     <div class="form-group">
                       <label for="startDate">Horário de Termino</label>
                       <input type="time" name="schedule-edit[timeEnd]" placeholder="Date Start" class="form-control timeEnd">
                     </div>
                   </div>
                   <div class="col-md-12">
                     <div class="form-group">
                       <label for="">Valor (R$)</label>
                       <input type="text" class="form-control value-mask value-list" name="schedule-edit[value]" placeholder="Digite o Valor">
                     </div>
                   </div>

                   <div class="col-md-12">
                     <div class="form-group">
                       <label for="patients">Telefone</label>
                       <input type="text" name="schedule-edit[tellphone]" placeholder="Telefone" class="form-control phone_us tellphone">
                     </div>
                   </div>
                   <div class="col-md-12">
                     <div class="form-group">
                       <label for="">Motivo</label>
                       <input type="text" class="form-control reason" name="schedule-edit[reason]" placeholder="Digite o motivo do agendamento">
                     </div>
                   </div>
                   <div class="col-md-12">
                     <label for="">Tipo</label>
                     <div class="form-group">
                       <select name="schedule-edit[types]" class="form-control types">
                         <option selected disabled>-- selecione uma opção --</option>
                         <option value="Consulta">Consulta</option>
                         <option value="Periódico">Periódico</option>
                         <option value="Retorno">Retorno</option>
                         <option value="Periódico">Histeroscopia</option>
                         <option value="Periódico">Pré-Natal</option>
                         <option value="Outro">Outro</option>
                       </select>
                     </div>
                   </div>
                   <div class="col-md-12">
                     <label for="">Situação</label>
                     <i class="fa fa-square color-squared" aria-hidden="true"></i>
                     <input type="hidden" name="schedule-edit[color]" class="color-situation">
                     <div class="form-group">
                       <select name="schedule-edit[situation]" class="form-control situations">
                         <option selected disabled>-- selecione uma opção --</option>
                         <option value="Atendido">Atendido</option>
                         <option value="Cliente Chegou">Cliente Chegou</option>
                         <option value="Confirmado">Confirmado</option>
                         <option value="Confirmar">Confirmar</option>
                         <option value="Desmarcou">Desmarcou</option>
                         <option value="Em atendimento">Em atendimento</option>
                         <option value="Faltou">Faltou</option>
                       </select>
                     </div>
                   </div>

                 </div>
               </div>
               <div class="modal-footer">
                 <button type="button" class="btn btn-danger btn-cancelar">Cancelar</button>
                 <button type="submit" class="btn btn-success btn-editar">Editar</button>
               </div>
             </form>
           </div>
           <script>
             $('.btn-cancelar').on('click', function() {
               $('.listForm').slideToggle();
               $('.editForm').slideToggle();
             })
           </script>

         </div>
       </div>
     </div>

   </div>
   <div class="modal" id="schedule-create" tabindex="-1" role="dialog">
     <form method="post">
       <div class="modal-dialog" role="document">
         <div class="modal-content">
           <div class="modal-header">
             <h2 class="modal-title"><strong>Detalhes do Agendamento</strong></h2>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
             </button>
           </div>
           <div class="modal-body">
             <div class="row">
               <div class="col-md-10">
                 <div class="form-group">
                   <label for="patients">Médico</label>
                   <select name="schedule[medics]" class="form-control">
                     <option selected disabled>-- Selecione uma opção --</option>
                     <?php foreach ($listMedics as $medicsList) { ?>
                       <option value="<?= $medicsList['id'] ?>"><?= $medicsList['name'] ?></option>
                     <?php } ?>
                   </select>
                 </div>
               </div>

               <div class="col-2" style="padding-top: 20px">
                 <a href="medics/create" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i></a>
               </div>
               <div class="col-md-10">
                 <div class="form-group">
                   <label for="patients">Pacientes</label>
                   <select name="schedule[patients]" class="form-control">
                     <option selected disabled>-- Selecione uma opção --</option>
                     <?php foreach ($listPatients as $patientsList) { ?>
                       <option value="<?= $patientsList['id'] ?>"><?= $patientsList['name'] ?></option>
                     <?php } ?>
                   </select>
                 </div>
               </div>
               <div class="col-2" style="padding-top: 35px">
                 <a href="patients/create" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i></a>
               </div>
               <div class="col-md-12">
                 <div class="form-group">
                   <label for="patients">Email</label>
                   <input type="text" name="schedule[email]" placeholder="Email" class="form-control">
                 </div>
               </div>
               <div class="col-md-6">
                 <div class="form-group">
                   <label for="startDate">Data Inicial</label>
                   <input type="date" name="schedule[dateStart]" id="dateStart" placeholder="Date Start" class="form-control">
                 </div>
               </div>
               <div class="col-md-6">
                 <div class="form-group">
                   <label for="startDate">Horário Inicial</label>
                   <input type="time" name="schedule[timeStart]" id="timeStart" placeholder="Date Start" class="form-control">
                 </div>
               </div>
               <div class="col-md-6">
                 <div class="form-group">
                   <label for="startDate">Data de termino</label>
                   <input type="date" name="schedule[dateEnd]" id="dateEnd" placeholder="Date Start" class="form-control">
                 </div>
               </div>
               <div class="col-md-6">
                 <div class="form-group">
                   <label for="startDate">Horário de Termino</label>
                   <input type="time" name="schedule[timeEnd]" id="timeEnd" placeholder="Date Start" class="form-control">
                 </div>
               </div>
               <div class="col-md-12">
                 <div class="form-group">
                   <label for="">Valor (R$)</label>
                   <input type="text" class="form-control value-mask" name="schedule[value]" placeholder="Digite o Valor">
                 </div>
               </div>
               <div class="col-md-12">
                 <div class="form-group">
                   <label for="patients">Telefone</label>
                   <input type="text" name="schedule[tellphone]" placeholder="Telefone" class="form-control phone_us">
                 </div>
               </div>
               <div class="col-md-12">
                 <div class="form-group">
                   <label for="">Motivo</label>
                   <input type="text" class="form-control" name="schedule[reason]" placeholder="Digite o motivo do agendamento">
                 </div>
               </div>
               <div class="col-md-12">
                 <label for="">Tipo</label>
                 <div class="form-group">
                   <select name="schedule[type]" class="form-control">
                     <option selected disabled>-- selecione uma opção --</option>
                     <option value="Consulta">Consulta</option>
                     <option value="Periódico">Periódico</option>
                     <option value="Retorno">Retorno</option>
                     <option value="Periódico">Histeroscopia</option>
                     <option value="Periódico">Pré-Natal</option>
                     <option value="Outro">Outro</option>
                   </select>
                 </div>
               </div>
               <div class="col-md-12">
                 <label for="">Situação</label>
                 <i class="fa fa-square color-squared" aria-hidden="true"></i>
                 <input type="hidden" name="schedule[color]" class="color-situation ">
                 <div class="form-group">
                   <select name="schedule[situation]" class="form-control situations">
                     <option selected disabled>-- selecione uma opção --</option>
                     <option value="Atendido">Atendido</option>
                     <option value="Cliente Chegou">Cliente Chegou</option>
                     <option value="Confirmado">Confirmado</option>
                     <option value="Confirmar">Confirmar</option>
                     <option value="Desmarcou">Desmarcou</option>
                     <option value="Em atendimento">Em atendimento</option>
                     <option value="Faltou">Faltou</option>
                   </select>
                 </div>
               </div>


             </div>
           </div>
           <div class="modal-footer">
             <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
             <button type="submit" class="btn btn-primary">Cadastrar</button>
           </div>
         </div>
     </form>
   </div>
 </div>


 <div class="white-box">
   <div class="row">
     <div class="col-md-12">
       <div id='calendar'></div>
     </div>
   </div>

 </div>


 <script>
   $(".situations").on("change", function() {
     var value = $(this).val(); // aqui vc pega cada valor selecionado com o this
     color = {
       'Atendido': '#453EFF',
       'Cliente Chegou': '#2BB31F',
       'Confirmado': '#93CC98',
       'Confirmar': '#FF3286',
       'Desmarcou': '#FFC0BF',
       'Em atendimento': '#A4FFF9',
       'Faltou': '#FF3E38',
     }
     $('.color-situation').val(color[value])
     $('.color-squared').css('color', color[value])
   })
 </script>


 <script>
   $(document).ready(function() {
     $('#crm').mask('0000 0000-0');
     $('#rg').mask('0000000000-0');
     $('.phone_us').mask('(00) 0 0000-0000');
     $('.phone_us_edit').mask('(00) 0 0000-0000');
     $('#cpf').mask('000.000.000-00')
     $('.value-mask').mask('R$ 00000000000000');

   });
 </script>