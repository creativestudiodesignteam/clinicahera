<?php
require('app/core/login.class.php');
require('app/core/password.class.php');

$login = new Login();
$password = new Password();

if (isset($_SESSION['id'])) {
  $url->redirect('home');
}
if (!empty($_POST['email']) && !empty($_POST['password'])) {
  $login->email = $login->filter($_POST['email']);
  $login->password = $password->encrypt($login->filter($_POST['password']));
  $login->join();
}
?>

<body class="login">
  <div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-8" style="padding-top: 10%;">
      <div class="form-login">
        <div class="panel-default">
          <div class="panel-heading">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-6 p-0">
                  <a href="#">Clínica Hera</a>
                </div>
                <div class="col-md-6 text-right p-0">
                  <ul class="list-unstyled list-inline">
                    <li><a class="-active" href="login">Login</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="white-box" style="border-bottom-right-radius: 50px;border-bottom-left-radius: 50px">
            <h2 class="text-center">Seja bem-vindo</h2>
            <form class="form-horizontal" method="POST">

              <div class="form-group">
                <label for="email" class="col-md-4 control-label">E-mail</label>

                <div class="col-md-6">
                  <input id="email" type="email" class="form-control" name="email" required autofocus>
                </div>
              </div>

              <div class="form-group">
                <label for="password" class="col-md-4 control-label">Senha</label>
                <div class="col-md-6">
                  <input id="password" type="password" class="form-control" name="password" required>
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="remember"> Lembrar de mim
                    </label>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                  <button type="submit" class="btn btn-primary">
                    Login
                  </button>
                  <a class="btn btn-link" href="auth/forgot-password">
                    Esqueceu sua senha?
                  </a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>


</body>