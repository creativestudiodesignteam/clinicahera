<?php
include 'app/model/consultancies.model.php';
include 'app/controller/consultanciesDAO.php';
$consultancies = new Consultancies();
$consultanciesDAO = new ConsultanciesDAO($db);

include 'app/model/clinics.model.php';
include 'app/controller/clinicsDAO.php';
$clinics = new Clinics();
$clinicsDAO = new ClinicsDAO($db);
$list = $consultanciesDAO->listAll();


if (!empty($_GET['remove_id'])) {
  $consultancies->setId($_GET['remove_id']);
  $consultanciesDAO->consultancies = $consultancies;
  $consultanciesDAO->delete();
  $url->redirect('consultancies/manage');
}
?>

<div class="bg-title">
  <h3 class="page-title">
    <i class="fa fa-medkit"></i> Consultorios
    <i class="fa fa-angle-right"></i> <span class="text-primary">Listagem</span>
    <a class="btn btn-default" style="padding: 5px 10px;width: 32px;" href="consultancies/create"><i class="fa fa-plus"></i></a>
  </h3>
</div>
<div class="row">
  <div class="col-md-12">
    <table class="infos-table" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nomes</th>
          <th>Clinicas</th>
          <th style="width: 64px;">Mais</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($list as $l) { 
          $clinics_fk = $clinicsDAO->getById($l['fk_clinics']);
          ?>
          <tr>
            <td><?= $l['id'] ?></td>
            <td><?= $l['name'] ?></td>
            <td><?= $clinics_fk['name'] ?></td>
            <td>
              <div class="row">
                <div class="col-md-5">
                  <a class="btn btn-default" style="padding: 5px;width: 32px;" href="./consultancies/edit&consultancies_id=<?= $l['id'] ?>"><i class="fa fa-edit"></i></a>
                </div>
                <div class="col-md-5">
                  <a style="padding: 5px;width: 32px;" href="./consultancies/manage&remove_id=<?= $l['id'] ?>" class="btn btn-danger"><i class="fa fa-times"></i></a>
                </div>
              </div>
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>