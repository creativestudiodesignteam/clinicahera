<?php
include 'app/model/clinics.model.php';
include 'app/controller/clinicsDAO.php';
include 'app/model/consultancies.model.php';
include 'app/controller/consultanciesDAO.php';

$consultancies = new Consultancies();
$consultanciesDAO = new ConsultanciesDAO($db);

$clinics = new Clinics();
$clinicsDAO = new ClinicsDAO($db);

$notify = array('', '');
$listClinics = $clinicsDAO->listAll();
//postcode state city neighborhood street number complement

if (isset($_POST['consultancies'])) {
  $form = array(
    $_POST['consultancies'],

  );
  $consultancies->setName($form[0]['name']);
  $consultancies->setFk_clinics($form[0]['fk_clinics']);
  $consultanciesDAO->consultancies = $consultancies;
  $consultanciesDAO->insert();
  $url->redirect('consultancies/manage&create=successes');
}
?>
<div class="bg-title">
  <h3 class="page-title">
    <i class="fa fa-medkit"></i> Consultorios
    <i class="fa fa-angle-right"></i> <span class="text-primary">Adicionar</span>
    <a class="btn btn-default" style="padding: 5px 10px;width: 32px;" href="consultancies/create"><i class="fa fa-plus"></i></a>
  </h3>
</div>
<div class="white-box">
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h3 class="page-title">Cadastrar clinica</h3>
    </div>
  </div>
  <form method="post">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label>Nome *</label>
          <input type="text" class="form-control" name="consultancies[name]" placeholder="Digite o nome da clinica">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Clinica *</label>
          <select name="consultancies[fk_clinics]" id="fk_clinics" class="form-control">
            <option disabled selected>-- selecione uma opção --</option>
            <?php foreach ($listClinics as $clinicslist) { ?>
              <option value="<?= $clinicslist['id'] ?>"><?= $clinicslist['name'] ?></option>
            <?php } ?>
          </select>
        </div>
      </div>

      <div class="col-md-12">
        <div class="form-group">
          <button type="submit" name="create" class="text-white btn btn-primary">Cadastrar</button>
        </div>
      </div>
  </form>
</div>
<script>
  $(document).ready(function() {
    $('#date_time').mask('00/00/0000 00:00:00');
    $('#cnpj').mask('00.000.000/0000-00')
    $('#crm').mask('0000 0000-0');
    $('#rg').mask('0000000000-0');
    $('#postcode').mask('00000-000');
    $('#phone_residential').mask('(00) 0000-0000');
    $('#phone_commercial').mask('(00) 0000-0000');
    $('#phone_us').mask('(00) 0 0000-0000');
    $('#cpf').mask('000.000.000-00')

  });
</script>