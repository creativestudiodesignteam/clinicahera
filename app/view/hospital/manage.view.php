<?php
include 'app/model/hospital.model.php';
include 'app/controller/hospitalDAO.php';
$hospital = new Hospital();
$hospitalDAO = new HospitalDAO($db);
$list = $hospitalDAO->listAll();


if (!empty($_GET['remove_id'])) {
  $hospital->setId($_GET['remove_id']);
  $hospitalDAO->hospital = $hospital;
  $hospitalDAO->delete();
  $url->redirect('hospital/manage');
}
?>

<div class="bg-title">
  <h3 class="page-title">
    <i class="fa fa-h-square"></i> Hospitais
    <i class="fa fa-angle-right"></i> <span class="text-primary">Gerenciar</span>
    <a class="btn btn-default" style="padding: 5px 10px;width: 32px;" href="hospital/create"><i class="fa fa-plus"></i></a>
  </h3>
</div>
<div class="row">
  <div class="col-md-12">
    <table class="infos-table" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nome</th>
          <th>Email</th>
          <th style="width: 64px;">Mais</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($list as $l) { ?>
          <tr>
            <td><?= $l['id'] ?></td>
            <td><?= $l['name'] ?></td>
            <td><?= $l['email'] ?></td>
            <td>
              <div class="row">
                <div class="col-md-5">
                  <a class="btn btn-default" style="padding: 5px;width: 32px;" href="./hospital/edit&hospital_id=<?= $l['id'] ?>"><i class="fa fa-edit"></i></a>
                </div>
                <div class="col-md-5">
                  <a style="padding: 5px;width: 32px;" href="./hospital/manage&remove_id=<?= $l['id'] ?>" class="btn btn-danger"><i class="fa fa-times"></i></a>
                </div>
              </div>
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>