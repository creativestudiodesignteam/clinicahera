<?php
include 'app/model/hospital.model.php';
include 'app/controller/hospitalDAO.php';
include 'app/model/addresses.model.php';
include 'app/controller/addressesDAO.php';
$addresses = new Addresses();
$addressesDAO = new AddressesDAO($db);

$hospital = new Hospital();
$hospitalDAO = new HospitalDAO($db);

$notify = array('', '');

//postcode state city neighborhood street number complement

if (isset($_POST['hospital'], $_POST['addresses'])) {
  $form = array(
    $_POST['hospital'],
    $_POST['addresses'],
  );

  $addresses->setPostcode($form[1]['postcode']);
  $addresses->setState($form[1]['state']);
  $addresses->setCity($form[1]['city']);
  $addresses->setNeighborhood($form[1]['neighborhood']);
  $addresses->setStreet($form[1]['street']);
  $addresses->setNumber($form[1]['number']);
  $addresses->setComplement($form[1]['complement']);
  $addressesDAO->addresses = $addresses;
  $fk_addresses = $addressesDAO->insert();

  if (empty($fk_addresses)) {
    $components->notify('danger', '<b>Dados incorretos!</b> Endereço não cadastrado .');
  }

  $hospital->setName($form[0]['name']);
  $hospital->setCnpj($form[0]['cnpj']);
  $hospital->setEmail($form[0]['email']);
  $hospital->setReason_social($form[0]['reason_social']);
  $hospital->setTellphone($form[0]['tellphone']);
  $hospital->setFk_addresses($fk_addresses);
  $hospitalDAO->hospital = $hospital;
  $hospitalDAO->insert();
  $url->redirect('hospital/managee&create=successes');
}
?>
<div class="bg-title">
  <h3 class="page-title">
    <i class="fa fa-h-square"></i> Hospital
    <i class="fa fa-angle-right"></i> <span class="text-primary">Adicionar</span>
    <a class="btn btn-default" style="padding: 5px 10px;width: 32px;" href="hospital/create"><i class="fa fa-plus"></i></a>
  </h3>
</div>
<div class="white-box">
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h3 class="page-title">Cadastrar clinica</h3>
    </div>
  </div>
  <form method="post">
    <div class="row">
      <div class="col-md-4">
        <div class="form-group">
          <label>Nome *</label>
          <input type="text" class="form-control" name="hospital[name]" placeholder="Digite o nome da clinica">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Razão Social *</label>
          <input type="text" class="form-control" name="hospital[reason_social]" placeholder="Digite a Razão Social">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>CNPJ *</label>
          <input type="text" class="form-control" name="hospital[cnpj]" id="cnpj" placeholder="00.000.000./0000-00">
        </div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3 class="page-title">Contatos</h3>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Email *</label>
          <input type="email" class="form-control" name="hospital[email]" placeholder="example@example.com">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Telefone</label>
          <input type="text" class="form-control" name="hospital[tellphone]" id="phone_residential" placeholder="(00) 0000-0000">
        </div>
      </div>


      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3 class="page-title">Endereço</h3>
      </div>
      <!-- //postcode state city neighborhood street number complement -->
      <div class="col-md-4">
        <div class="form-group">
          <label>Estados</label>
          <select name="addresses[state]" class="form-control">
            <option selected disabled>Estados</option>
            <option value="AC">Acre</option>
            <option value="AL">Alagoas</option>
            <option value="AP">Amapá</option>
            <option value="AM">Amazonas</option>
            <option value="BA">Bahia</option>
            <option value="CE">Ceará</option>
            <option value="DF">Distrito Federal</option>
            <option value="ES">Espírito Santo</option>
            <option value="GO">Goiás</option>
            <option value="MA">Maranhão</option>
            <option value="MT">Mato Grosso</option>
            <option value="MS">Mato Grosso do Sul</option>
            <option value="MG">Minas Gerais</option>
            <option value="PA">Pará</option>
            <option value="PB">Paraíba</option>
            <option value="PR">Paraná</option>
            <option value="PE">Pernambuco</option>
            <option value="PI">Piauí</option>
            <option value="RJ">Rio de Janeiro</option>
            <option value="RN">Rio Grande do Norte</option>
            <option value="RS">Rio Grande do Sul</option>
            <option value="RO">Rondônia</option>
            <option value="RR">Roraima</option>
            <option value="SC">Santa Catarina</option>
            <option value="SP">São Paulo</option>
            <option value="SE">Sergipe</option>
            <option value="TO">Tocantins</option>
          </select>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Cidade</label>
          <input type="text" class="form-control" class="form-control" name="addresses[city]" placeholder="Digite o nome da sua cidade">
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label>CEP</label>
          <input type="text" class="form-control" class="form-control" name="addresses[postcode]" id="postcode" placeholder="00000-000">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Bairro</label>
          <input type="text" class="form-control" class="form-control" name="addresses[neighborhood]" placeholder="Digite o nome do seu bairro">
        </div>
      </div>
      <div class="col-md-5">
        <div class="form-group">
          <label>Rua</label>
          <input type="text" class="form-control" name="addresses[street]" placeholder="Digite o nome da sua rua">
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>Numero</label>
          <input type="text" class="form-control" name="addresses[number]" placeholder="Digite o numero da sua casa">
        </div>
      </div>


      <div class="col-md-12">
        <div class="form-group">
          <label>Complemento</label>
          <input type="text" class="form-control" name="addresses[complement]" placeholder="Complemento">
        </div>
      </div>

      <div class="col-md-12">
        <div class="form-group">
          <button type="submit" name="create" class="text-white btn btn-primary">Cadastrar</button>
        </div>
      </div>

  </form>
</div>
<script>
  $(document).ready(function() {
    $('#date_time').mask('00/00/0000 00:00:00');
    $('#cnpj').mask('00.000.000/0000-00')
    $('#crm').mask('0000 0000-0');
    $('#rg').mask('0000000000-0');
    $('#postcode').mask('00000-000');
    $('#phone_residential').mask('(00) 0000-0000');
    $('#phone_commercial').mask('(00) 0000-0000');
    $('#phone_us').mask('(00) 0 0000-0000');
    $('#cpf').mask('000.000.000-00')

  });
</script>