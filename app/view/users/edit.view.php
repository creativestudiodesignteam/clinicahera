<?php

if (empty($_GET['users_id'])) {
  include 'app/model/user.model.php';
  include 'app/controller/userDAO.php';
  include 'app/model/addresses.model.php';
  include 'app/controller/addressesDAO.php';
  include 'app/model/group.model.php';
  include 'app/controller/groupDAO.php';
  require('app/core/password.class.php');
  $password = new Password();
  $user = new User();
  $userDAO = new UserDAO($db);
  $addresses = new Addresses();
  $addressesDAO = new AddressesDAO($db);
  $group = new Group();
  $groupDAO = new GroupDAO($db);

  $info = $userDAO->getById($_GET['user_id']);


  $listGroup = $groupDAO->listAll();
  $groupGet = $groupDAO->getById($info['fk_group']);
  $addressesGet = $addressesDAO->getById($info['fk_addresses']);


  $notify = array('', '');
  if (!empty($info)) {
    if (isset($_POST['users'], $_POST['addresses'])) {
      $form = array(
        $_POST['users'],
        $_POST['addresses']
      );
      var_dump($info);
      if (!empty($form[0]['old-password']) and !empty($form[0]['new-password']) and  !empty($form[0]['confirm-password'])) {
        $oldpassword = $password->encrypt($form[0]['old-password']);
        $newpassword = $password->encrypt($form[0]['new-password']);
        $confirmpassword = $password->encrypt($form[0]['confirm-password']);

        if ($oldpassword == $info['password'] && $newpassword === $confirmpassword) {
          $password_hash = $newpassword;
        } else {
          $password_hash = $info['password'];
        }
        var_dump($newpassword);
      }


      if (empty($form[0]['old-password']) and empty($form[0]['new-password']) and  empty($form[0]['confirm-password'])) {
      }




      $addresses->setId($info['fk_addresses']);
      $addresses->setPostcode($form[1]['postcode']);
      $addresses->setState($form[1]['state']);
      $addresses->setCity($form[1]['city']);
      $addresses->setNeighborhood($form[1]['neighborhood']);
      $addresses->setStreet($form[1]['street']);
      $addresses->setNumber($form[1]['number']);
      $addresses->setComplement($form[1]['complement']);
      $addressesDAO->addresses = $addresses;
      $fk_addresses = $addressesDAO->update();

      $user->setId($info['id']);
      $user->setName($form[0]['name']); //
      $user->setEmail($form[0]['email']); //
      $user->setCellphone($form[0]['cellphone']); //
      $user->setTellphone($form[0]['tellphone']); //
      $user->setPassword($password_hash); //
      $user->setFk_group($form[0]['fk_group']); //
      $user->setFk_addresses($info['fk_addresses']); //
      $userDAO->user = $user;
      $userDAO->update();

      //$url->redirect('users/manage&edit=succsess');
    }






?>
    <div class="bg-title">
      <h3 class="page-title">
        <i class="fa fa-user"></i> Usuários
        <i class="fa fa-angle-right"></i> <span class="text-primary">Adicionar</span>
        <a class="btn btn-default" style="padding: 5px 10px;width: 32px;" href="clinics/create"><i class="fa fa-plus"></i></a>
      </h3>
    </div>
    <div class="white-box">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h3 class="page-title">Atualizar Usuarios</h3>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

        </div>
      </div>
      <form method="post">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Name</label>
              <input type="text" class="form-control" name="users[name]" value="<?= $info['name'] ?>" placeholder="Digite seu nome" required>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Grupo</label>
              <select name="users[fk_group]" class="form-control">
                <option selected value="<?= $info['fk_group'] ?>">-- <?= $groupGet['name'] ?> --</option>
                <?php foreach ($listGroup as $list) { ?>
                  <option value="<?= $list['id'] ?>"><?= $list['name'] ?></option>
                <?php } ?>

              </select>
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Email</label>
              <input type="email" class="form-control" name="users[email]" value="<?= $info['email'] ?>" placeholder="email@email.com" required>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Celular</label>
              <input type="text" class="form-control" name="users[cellphone]" value="<?= $info['cellphone'] ?>" id="phone_us" placeholder="(00)0 0000-0000" required>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Telefone</label>
              <input type="text" class="form-control" name="users[tellphone]" value="<?= $info['tellphone'] ?>" id="phone_residential" placeholder="(00) 0000-0000" required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Senha Antiga</label>
              <input type="password" class="form-control" name="users[old-password]" placeholder="Digite sua senha atual">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Nova Senha</label>
              <input type="password" class="form-control" name="users[new-password]" placeholder="Digite sua senha nova">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Confirmar Senha</label>
              <input type="password" class="form-control" name="users[confirm-password]" placeholder="Confirme sua senha">
            </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3 class="page-title">Endereço</h3>
          </div>
          <!-- //postcode state city neighborhood street number complement -->
          <div class="col-md-4">
            <div class="form-group">
              <label>Estados</label>
              <select name="addresses[state]" class="form-control">
                <option selected value="<?= $addressesGet['state'] ?>">-- <?= $addressesGet['state'] ?> --</option>
                <option value="AC">Acre</option>
                <option value="AL">Alagoas</option>
                <option value="AP">Amapá</option>
                <option value="AM">Amazonas</option>
                <option value="BA">Bahia</option>
                <option value="CE">Ceará</option>
                <option value="DF">Distrito Federal</option>
                <option value="ES">Espírito Santo</option>
                <option value="GO">Goiás</option>
                <option value="MA">Maranhão</option>
                <option value="MT">Mato Grosso</option>
                <option value="MS">Mato Grosso do Sul</option>
                <option value="MG">Minas Gerais</option>
                <option value="PA">Pará</option>
                <option value="PB">Paraíba</option>
                <option value="PR">Paraná</option>
                <option value="PE">Pernambuco</option>
                <option value="PI">Piauí</option>
                <option value="RJ">Rio de Janeiro</option>
                <option value="RN">Rio Grande do Norte</option>
                <option value="RS">Rio Grande do Sul</option>
                <option value="RO">Rondônia</option>
                <option value="RR">Roraima</option>
                <option value="SC">Santa Catarina</option>
                <option value="SP">São Paulo</option>
                <option value="SE">Sergipe</option>
                <option value="TO">Tocantins</option>
              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Cidade</label>
              <input type="text" class="form-control" class="form-control" name="addresses[city]" value="<?= $addressesGet['city'] ?>" placeholder="Digite o nome da sua cidade">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label>CEP</label>
              <input type="text" class="form-control" class="form-control" name="addresses[postcode]" value="<?= $addressesGet['postcode'] ?>" id="postcode" placeholder="00000-000">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Bairro</label>
              <input type="text" class="form-control" class="form-control" name="addresses[neighborhood]" value="<?= $addressesGet['neighborhood'] ?>" placeholder="Digite o nome do seu bairro">
            </div>
          </div>
          <div class="col-md-5">
            <div class="form-group">
              <label>Rua</label>
              <input type="text" class="form-control" name="addresses[street]" value="<?= $addressesGet['street'] ?>" placeholder="Digite o nome da sua rua">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label>Numero</label>
              <input type="text" class="form-control" name="addresses[number]" value="<?= $addressesGet['number'] ?>" placeholder="Digite o numero da sua casa">
            </div>
          </div>


          <div class="col-md-12">
            <div class="form-group">
              <label>Complemento</label>
              <input type="text" class="form-control" name="addresses[complement]" value="<?= $addressesGet['complement'] ?>" placeholder="Complemento">
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <input type="submit" name="create" class="btn btn-primary">
            </div>
          </div>
        </div>
      </form>
    </div>
<?php
  } else {
    $url->redirect('error');
  }
} else {
  $url->redirect('home');
}
?>
<script>
  $(document).ready(function() {
    $('#date_time').mask('00/00/0000 00:00:00');
    $('#cnpj').mask('00.000.000/0000-00')
    $('#crm').mask('0000 0000-0');
    $('#rg').mask('0000000000-0');
    $('#postcode').mask('00000-000');
    $('#phone_residential').mask('(00) 0000-0000');
    $('#value_unid_reference').mask('R$ 0000000000000000');

    $('#phone_commercial').mask('(00) 0000-0000');
    $('#phone_us').mask('(00) 0 0000-0000');
    $('#cpf').mask('000.000.000-00')

  });
</script>