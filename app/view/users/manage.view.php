<?php
include 'app/model/user.model.php';
include 'app/controller/userDAO.php';
$user = new User();
$userDAO = new UserDAO($db);
$list = $userDAO->listAll();
if (!empty($_GET['user_id'])) {
  $user->setId($_GET['user_id']);
  $userDAO->user = $user;
  $userDAO->delete();
  $url->redirect('users/manage');
}
?>

<div class="bg-title">
    <h3 class="page-title">
        <i class="fa fa-user"></i> Usuários
        <i class="fa fa-angle-right"></i> <span class="text-primary">Gerenciar</span>
        <a class="btn btn-default" style="padding: 5px 10px;width: 32px;" href="users/create"><i class="fa fa-plus"></i></a>
    </h3>
</div>
<div class="row">
    <div class="col-md-12">
        <table class="infos-table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nome</th>
                    <th>E-mail</th>
                    <th style="width: 64px;">Mais</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($list as $l) { ?>
                    <tr>
                        <td><?= $l['id'] ?></td>
                        <td><?= $l['name'] ?></td>
                        <td><?= $l['email'] ?></td>
                        
                        <td>
                            <div class="row">
                                <div class="col-md-5">
                                  <a class="btn btn-default" style="padding: 5px;width: 32px;" href="./users/edit&user_id=<?= $l['id'] ?>"><i class="fa fa-edit"></i></a>
                                </div>
                                <div class="col-md-5">
                                  <a style="padding: 5px;width: 32px;" href="./users/manage&user_id=<?= $l['id'] ?>" class="btn btn-danger"><i class="fa fa-times"></i></a>
                                </div>
                          </div>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>