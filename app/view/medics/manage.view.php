<?php

include 'app/model/medics.model.php';
include 'app/controller/medicsDAO.php';

include 'app/model/addresses.model.php';
include 'app/controller/addressesDAO.php';

include 'app/model/clinics.model.php';
include 'app/controller/clinicsDAO.php';

$clinics = new Clinics();
$clinicsDAO = new ClinicsDAO($db);

$medics = new Medics();
$medicsDAO = new MedicsDAO($db);

$listClinics = $clinicsDAO->listAll();

$addresses = new Addresses();
$addressesDAO = new AddressesDAO($db);

$list = $medicsDAO->listAll();

if (!empty($_GET['remove_id'])) {
  $medics->setId($_GET['remove_id']);
  $medicsDAO->medics = $medics;
  $medicsDAO->delete();
  $url->redirect('medics/manage&remove=successes');
}
?>

<div class="bg-title">
  <h3 class="page-title">
    <i class="fa fa-user-md"></i> Médicos
    <i class="fa fa-angle-right"></i> <span class="text-primary">Gerenciar</span>
    <a class="btn btn-default" style="padding: 5px 10px;width: 32px;" href="medics/create"><i class="fa fa-plus"></i></a>
  </h3>
</div>
<div class="row">
  <div class="col-md-12">
    <table class="infos-table" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nome</th>
          <th>Conselho</th>
          <th style="width: 64px;">Mais</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($list as $l) {
        ?>
          <tr>
            <td><?= $l['id'] ?></td>
            <td><?= $l['name'] ?></td>
            <td><?= $l['council_initials'] ?></td>
            <td>
              <a class="btn btn-default" style="padding: 5px;width: 32px;" href="./medics/edit&medics_id=<?= $l['id'] ?>"><i class="fa fa-edit"></i></a>
              <a style="padding: 5px;width: 32px;" href="./medics/manage&remove_id=<?= $l['id'] ?>" class="btn btn-danger"><i class="fa fa-times"></i></a>
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>