<?php

include 'app/model/medics.model.php';
include 'app/controller/medicsDAO.php';

include 'app/model/addresses.model.php';
include 'app/controller/addressesDAO.php';

include 'app/model/clinics.model.php';
include 'app/controller/clinicsDAO.php';

include 'app/model/office.model.php';
include 'app/controller/officeDAO.php';

include 'app/model/bank_account.model.php';
include 'app/controller/bank_accountDAO.php';

$clinics = new Clinics();
$clinicsDAO = new ClinicsDAO($db);

$office = new Office();
$officeDAO = new OfficeDAO($db);

$medics = new Medics();
$medicsDAO = new MedicsDAO($db);

$bank_account = new Bank_account();
$bank_accountDAO = new Bank_accountDAO($db);

$listClinics = $clinicsDAO->listAll();
$addresses = new Addresses();
$addressesDAO = new AddressesDAO($db);

$notify = array('', '');



if (isset($_POST['medic'], $_POST['addresses'], $_POST['bank'])) {
  $form = array(
    $_POST['medic'],
    $_POST['addresses'],
    $_POST['bank'],
  );

  $bank_account->setName($form[2]['name']);
  $bank_account->setNumber($form[2]['number']);
  $bank_account->setAgency($form[2]['agency']);
  $bank_account->setAccount($form[2]['account']);
  $bank_accountDAO->bank_account = $bank_account;
  $fk_bank = $bank_accountDAO->insert();

  $addresses->setPostcode($form[1]['postcode']);
  $addresses->setState($form[1]['state']);
  $addresses->setCity($form[1]['city']);
  $addresses->setNeighborhood($form[1]['neighborhood']);
  $addresses->setStreet($form[1]['street']);
  $addresses->setNumber($form[1]['number']);
  $addresses->setComplement($form[1]['complement']);
  $addressesDAO->addresses = $addresses;
  $fk_addresses = $addressesDAO->insert();

  if (empty($fk_addresses)) {
    return $components->notify('danger', '<b>Dados incorretos!</b> Endereço não cadastrado .');
  }
  //id name time cpf rg birthday specialty council_number council_initials council_uf sexo email tellphone  cellphone  fk_addresses fk_clinics 
  $medics->setName($form[0]['name']);
  $medics->setCnpj($form[0]['cnpj']);
  $medics->setReason($form[0]['reason']);
  $medics->setTime($form[0]['time']);
  $medics->setCpf($form[0]['cpf']);
  $medics->setRg($form[0]['rg']);
  $medics->setBirthday($form[0]['birthday']);
  $medics->setSpecialty($form[0]['specialty']);
  $medics->setCouncil_number($form[0]['council_number']);
  $medics->setCouncil_initials($form[0]['council_initials']);
  $medics->setCouncil_uf($form[0]['council_uf']);
  $medics->setSexo($form[0]['sexo']);
  $medics->setEmail($form[0]['email']);
  $medics->setCellphone($form[0]['cellphone']);
  $medics->setTellphone($form[0]['tellphone']);
  $medics->setFk_clinics($form[0]['fk_clinics']);
  $medics->setFk_addresses($fk_addresses);
  $medics->setFk_bank($fk_bank);
  $medicsDAO->medics = $medics;
  $fk_medic = $medicsDAO->insert();

  if (isset($_POST['dayweek'], $_POST['timestart'], $_POST['timeend'])) {
    $hours = [
      "dayweek" => $_POST['dayweek'],
      "timestart" => $_POST['timestart'],
      "timeend" => $_POST['timeend'],
    ];
    for ($i = 0; sizeof($hours['dayweek']) > $i; $i++) {
      $office->setDayWeek($hours['dayweek'][$i]);
      $office->setTimeStart($hours['timestart'][$i]);
      $office->setTimeEnd($hours['timeend'][$i]);
      $office->setFk_medics($fk_medic);
      $officeDAO->office = $office;
      $officeDAO->insert();
      // $url->redirect('medics/manage?msg-create=success');
    }
  } else {
    // $url->redirect('medics/manage');
  }
}



?>

<div class="bg-title">
  <h3 class="page-title">
    <i class="fa fa-user-md"></i> Médicos
    <i class="fa fa-angle-right"></i> <span class="text-primary">Adicionar</span>
    <a class="btn btn-default" style="padding: 5px 10px;width: 32px;" href="medics/create"><i class="fa fa-plus"></i></a>
  </h3>
</div>
<!-- Modal -->
<form method="POST">
  <div class="modal bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title">Horário do Médico</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="hours-modal">
            <div class="row">
              <div class="col-md-2" id='dep_fc' style="padding-top:20px;">
                <button type="button" class="btn btn-danger text-dark remove"><i class="fa fa-trash" style="color: white"></i></button>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Dia da semana</label>
                  <select name="dayweek[]" id="dayweek" class="form-control">
                    <option selected disabled>-- Escolha uma opção -- </option>
                    <option value="Segunda">Segunda</option>
                    <option value="Terça">Terça</option>
                    <option value="Quarta">Quarta</option>
                    <option value="Quinta">Quinta</option>
                    <option value="Sexta">Sexta</option>
                    <option value="Sábado">Sábado</option>
                    <option value="Domingo">Domingo</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Inicio do expediente</label>
                  <input type="time" class="form-control" name="timestart[]" value="00:00">
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Final do expediente</label>
                  <input type="time" class="form-control" name="timeend[]" value="00:00">
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Fechar</button>
          <button type="button" class="btn btn-success" id="addinpts">Adicionar</button>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="white-box">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <h4 class="page-title">Cadastro de Médicos</h4>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target=".bd-example-modal-lg">Configurar Horários</button>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label for="name">Email do médico</label>*
              <input class="form-control" type="email" name="medic[email]" id="email" placeholder="exemplo@exemplo.com" required>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="name">Nome do médico</label>*
              <input class="form-control" type="text" name="medic[name]" id="name" placeholder="Digite o nome do médico" required>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Clinicas</label>
              <select name="medic[fk_clinics]" class="form-control">
                <option disabled selected>-- Escolha uma opção --</option>
                <?php foreach ($listClinics as $l) { ?>
                  <option value="<?= $l['id'] ?>"><?= $l['name'] ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="council">Duração da consulta (em minutos)</label>*
              <select class="form-control" name="medic[time]" id="time" required>
                <option disabled selected>-- Escolha uma opção --</option>
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="30">30</option>
                <option value="40">40</option>
                <option value="50">50</option>
                <option value="60">60</option>
                <option value="70">70</option>
                <option value="80">80</option>
                <option value="90">90</option>
                <option value="100">100</option>
              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="birthday">Data de nascimento</label>*
              <input class="form-control" type="date" name="medic[birthday]" id="birthday" required placeholder="00-00-0000">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h4 class="page-title">Dados Principais</h4>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label for="rg">Razão Social</label>*
              <input class="form-control" type="text" id="reason" name="medic[reason]" id="reason" placeholder="Digite a Razão social" required>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="rg">CNPJ</label>*
              <input class="form-control cnpj" type="text" name="medic[cnpj]" id="cnpj" placeholder="00.000.000/0000-00" required>
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="cpf">CPF</label>*
              <input class="form-control" type="text" id="cpf" name="medic[cpf]" id="cpf" required placeholder="000.000.000-0">
            </div>
            <!-- //name time cpf rg birthday specialty council_number council_initials council_uf sexo email tellphone cellphone fk_addresses fk_office fk_userclinics -->
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label for="rg">RG</label>*
              <input class="form-control" type="text" id="rg" name="medic[rg]" id="rg" placeholder="0000000000-0" required>
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label for="cellphone">Celular</label>*
              <input class="form-control" type="text" id="phone_us" name="medic[cellphone]" id="cellphone" required placeholder="(00) 0 0000-0000">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label for="tellphone">Telefone</label>*
              <input class="form-control" type="text" name="medic[tellphone]" id="phone_residential" required placeholder="(00) 0 0000-0000">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label for="specialty">Especialidade</label>*
              <input class="form-control" type="text" id="specialty" name="medic[specialty]" required placeholder="Digite a especialidade">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label for="council_number">Numero do Conselho</label>*
              <input class="form-control" type="text" id="council_number" name="medic[council_number]" required placeholder="0000 0000-0">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="council_number">Sigla do Conselho</label>*
              <input class="form-control" type="text" id="council_initials" name="medic[council_initials]" required placeholder="Ex: CRM">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Estados</label>
              <select name="medic[council_uf]" class="form-control">
                <option disabled selected>-- Escolha uma opção --</option>
                <option value="AC">AC</option>
                <option value="AL">AL</option>
                <option value="AP">AP</option>
                <option value="AM">AM</option>
                <option value="BA">BA</option>
                <option value="CE">CE</option>
                <option value="DF">DF</option>
                <option value="ES">ES</option>
                <option value="GO">GO</option>
                <option value="MA">MA</option>
                <option value="MT">MT</option>
                <option value="MS">MS</option>
                <option value="MG">MG</option>
                <option value="PA">PA</option>
                <option value="PB">PB</option>
                <option value="PR">PR</option>
                <option value="PE">PE</option>
                <option value="PI">PI</option>
                <option value="RJ">RJ</option>
                <option value="RN">RN</option>
                <option value="RS">RS</option>
                <option value="RO">RO</option>
                <option value="RR">RR</option>
                <option value="SC">SC</option>
                <option value="SP">SP</option>
                <option value="SE">SE</option>
                <option value="TO">TO</option>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Sexo</label>
              <select name="medic[sexo]" class="form-control">
                <option disabled selected>-- Escolha uma opção --</option>
                <option value="Masculino">Masculino</option>
                <option value="Feminino">Feminino</option>
              </select>
            </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h4 class="page-title">Dados Bancarios</h4>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Nome</label>
              <input type="text" class="form-control" class="form-control" name="bank[name]" placeholder="Digite o nome do banco">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Numero Bancário</label>
              <input type="text" class="form-control" class="form-control" name="bank[number]" placeholder="Digite o numero do banco">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Agência</label>
              <input type="text" class="form-control agency" class="form-control" name="bank[agency]" placeholder="Digite a agencia">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Conta</label>
              <input type="text" class="form-control account" class="form-control" name="bank[account]" placeholder="Digite a Conta">
            </div>
          </div>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h4 class="page-title">Endereço</h4>
          </div>
          <!-- //postcode state city neighborhood street number complement -->
          <div class="col-md-4">
            <div class="form-group">
              <label>Estados</label>
              <select name="addresses[state]" class="form-control">
                <option selected disabled>Estados</option>
                <option value="AC">Acre</option>
                <option value="AL">Alagoas</option>
                <option value="AP">Amapá</option>
                <option value="AM">Amazonas</option>
                <option value="BA">Bahia</option>
                <option value="CE">Ceará</option>
                <option value="DF">Distrito Federal</option>
                <option value="ES">Espírito Santo</option>
                <option value="GO">Goiás</option>
                <option value="MA">Maranhão</option>
                <option value="MT">Mato Grosso</option>
                <option value="MS">Mato Grosso do Sul</option>
                <option value="MG">Minas Gerais</option>
                <option value="PA">Pará</option>
                <option value="PB">Paraíba</option>
                <option value="PR">Paraná</option>
                <option value="PE">Pernambuco</option>
                <option value="PI">Piauí</option>
                <option value="RJ">Rio de Janeiro</option>
                <option value="RN">Rio Grande do Norte</option>
                <option value="RS">Rio Grande do Sul</option>
                <option value="RO">Rondônia</option>
                <option value="RR">Roraima</option>
                <option value="SC">Santa Catarina</option>
                <option value="SP">São Paulo</option>
                <option value="SE">Sergipe</option>
                <option value="TO">Tocantins</option>
              </select>
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label>Cidade</label>
              <input type="text" class="form-control" class="form-control" name="addresses[city]" placeholder="Digite o nome da sua cidade">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label>CEP</label>
              <input type="text" class="form-control" class="form-control" name="addresses[postcode]" id="postcode" placeholder="00000-000">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label>Bairro</label>
              <input type="text" class="form-control" class="form-control" name="addresses[neighborhood]" placeholder="Digite o nome do seu bairro">
            </div>
          </div>

          <div class="col-md-5">
            <div class="form-group">
              <label>Rua</label>
              <input type="text" class="form-control" name="addresses[street]" placeholder="Digite o nome da sua rua">
            </div>
          </div>

          <div class="col-md-3">
            <div class="form-group">
              <label>Numero</label>
              <input type="text" class="form-control" name="addresses[number]" placeholder="Digite o numero da sua casa">
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <label>Complemento</label>
              <input type="text" class="form-control" name="addresses[complement]" placeholder="Complemento">
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <button class="btn btn-primary" type="submit">Cadastrar</button>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</form>
<script>
  $("#addinpts").click(function() {
    var newRow = "<div class='row'><div class='col-md-2' id='dep_fc' style='padding-top:20px;'><button type='button' class='btn btn-danger text-dark remove'><i class='fa fa-trash' style='color: white'></i></button></div><div class='col-md-3'><div class='form-group'><label>Dia da semana</label><select name='dayweek[]' id='dayweek' class='form-control'><option selected disabled>-- Escolha uma opção -- </option><option value='Segunda'>Segunda</option><option value='Terça'>Terça</option><option value='Quarta'>Quarta</option><option value='Quinta'>Quinta</option><option value='Sexta'>Sexta</option><option value='Sábado'>Sábado</option><option value='Domingo'>Domingo</option></select></div></div><div class='col-md-3'><div class='form-group'><label>Inicio do expediente</label><input type='time' class='form-control' name='timestart[]' value='00:00'></div></div><div class='col-md-3'><div class='form-group'><label>Final do expediente</label><input type='time' class='form-control' name='timeend[]' value='00:00'></div></div></div>";
    $(".hours-modal").append(newRow);
  });

  $(document).on('click', 'button.remove', function() {
    $(this).closest('.row').remove();
  });
  $(document).ready(function() {
    $('#council_number').mask('0000 0000-0');
    $('#date_time').mask('00/00/0000 00:00:00');
    $('#cnpj').mask('00.000.000/0000-00')
    $('#rg').mask('0000000000-0');
    $('.agency').mask('0000000');
    $('.account').mask('000000000000');
    $('#postcode').mask('00000-000');
    $('#phone_residential').mask('(00) 0000-0000');
    $('#phone_commercial').mask('(00) 0000-0000');
    $('#phone_us').mask('(00) 0 0000-0000');
    $('#cpf').mask('000.000.000-00')

  });
</script>