<?php
include 'app/model/health_insurance.model.php';
include 'app/controller/health_insuranceDAO.php';
include 'app/model/addresses.model.php';
include 'app/controller/addressesDAO.php';
$addresses = new Addresses();
$addressesDAO = new AddressesDAO($db);

$health_insurance = new Health_insurance();
$healthDAO = new Health_insuranceDAO($db);

if (!empty($_GET['health_id'])) {


  $info = $healthDAO->getById($_GET['health_id']);

  $listAddresses = $addressesDAO->getById($info['fk_addresses']);
  if (empty($listAddresses)) {
    $listAddresses = null;
  }
  $notify = array('', '');

  if (!empty($info)) {
    if (isset($_POST['healths'], $_POST['addresses'])) {
      $form = array(
        $_POST['healths'],
        $_POST['addresses'],
      );
      $addresses->setId($info['fk_addresses']);
      $addresses->setPostcode($form[1]['postcode']);
      $addresses->setState($form[1]['state']);
      $addresses->setCity($form[1]['city']);
      $addresses->setNeighborhood($form[1]['neighborhood']);
      $addresses->setStreet($form[1]['street']);
      $addresses->setNumber($form[1]['number']);
      $addresses->setComplement($form[1]['complement']);
      $addressesDAO->addresses = $addresses;
      $addressesDAO->update();


      $health_insurance->setId($info['id']);
      $health_insurance->setName($form[0]['name']);
      $health_insurance->setEmail($form[0]['email']);
      $health_insurance->setSite($form[0]['site']);
      $health_insurance->setCnpj($form[0]['cnpj']);
      $health_insurance->setLogin($form[0]['login']);
      $health_insurance->setPassword($form[0]['password']);
      $health_insurance->setTellphone_commercial($form[0]['tellphone_commercial']);
      $health_insurance->setTellphone_residential($form[0]['tellphone']);
      $health_insurance->setTable_references($form[0]['table_references_send']);
      $health_insurance->setRegister_ans($form[0]['table_references']);
      $health_insurance->setFk_addresses($info['fk_addresses']);
      $healthDAO->health_insurance = $health_insurance;
      $healthDAO->update();
      $url->redirect('health/manage');
    }

?>
    <div class="bg-title">
      <h3 class="page-title">
        <i class="fa fa-heartbeat"></i>Cônvenio
        <i class="fa fa-angle-right"></i> <span class="text-primary">Atualizar</span>
        <a class="btn btn-default" style="padding: 5px 10px;width: 32px;" href="companies/create"><i class="fa fa-plus"></i></a>
      </h3>
    </div>
    <div class="white-box">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h3 class="page-title">Editar Convênio</h3>
        </div>
      </div>
      <form method="post">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label>Nome *</label>
              <input type="text" class="form-control" name="healths[name]" value="<?= $info['name'] ?>" placeholder="Digite o nome da clinica">
            </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3 class="page-title">Dados de Contato</h3>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Email</label>
              <input type="email" class="form-control" name="healths[email]" value="<?= $info['email'] ?>" placeholder="example@example.com">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>CNPJ</label>
              <input type="text" class="form-control" id="cnpj" name="healths[cnpj]" value="<?= $info['cnpj'] ?>" placeholder="00.000.000/0000-00">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Telefone Comercial</label>
              <input type="text" class="form-control" name="healths[tellphone_commercial]" value="<?= $info['tellphone_commercial'] ?>" id="phone_commercial" placeholder="(00) 0 0000-0000">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Telefone Residencial</label>
              <input type="text" class="form-control" name="healths[tellphone]" value="<?= $info['tellphone'] ?>" id="phone_residential" placeholder="(00) 0000-0000">
            </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3 class="page-title">Login</h3>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Site</label>
              <input type="text" class="form-control" name="healths[site]" value="<?= $info['site'] ?>" placeholder="www.example.ex">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Login</label>
              <input type="text" class="form-control" name="healths[login]" id="login" value="<?= $info['login'] ?>" placeholder="Digite o login">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Senha</label>
              <input type="text" class="form-control" name="healths[password]" id="password" value="<?= $info['password'] ?>" placeholder="Digite sua senha">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Tabela de Referencias</label>
              <input type="hidden" name="healths[table_references_send]" id="table_references_send">
              <select class="form-control" id="table_references" name="healths[table_references]">
                <option selected value="<?= $info['register_ans'] ?>">-- <?= $info['table_references'] ?> --</option>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Código</label>
              <input type="text" class="form-control" value="<?= $info['register_ans'] ?>" name="healths[register_ans]" disabled id="register_ans" placeholder="Digite o registro ANS">
            </div>
          </div>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3 class="page-title">Endereço</h3>
          </div>
          <!-- //postcode state city neighborhood street number complement -->
          <div class="col-md-4">
            <div class="form-group">
              <label>Estados</label>
              <select name="addresses[state]" class="form-control">
                <option selected value="<?= $listAddresses['state'] ?>">-- <?= $listAddresses['state'] ?> --</option>
                <option value="AC">AC</option>
                <option value="AL">AL</option>
                <option value="AP">AP</option>
                <option value="AM">AM</option>
                <option value="BA">BA</option>
                <option value="CE">CE</option>
                <option value="DF">DF</option>
                <option value="ES">ES</option>
                <option value="GO">GO</option>
                <option value="MA">MA</option>
                <option value="MT">MT</option>
                <option value="MS">MS</option>
                <option value="MG">MG</option>
                <option value="PA">PA</option>
                <option value="PB">PB</option>
                <option value="PR">PR</option>
                <option value="PE">PE</option>
                <option value="PI">PI</option>
                <option value="RJ">RJ</option>
                <option value="RN">RN</option>
                <option value="RS">RS</option>
                <option value="RO">RO</option>
                <option value="RR">RR</option>
                <option value="SC">SC</option>
                <option value="SP">SP</option>
                <option value="SE">SE</option>
                <option value="TO">TO</option>
              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Cidade</label>
              <input type="text" class="form-control" class="form-control" name="addresses[city]" value="<?= $listAddresses['city'] ?>" placeholder="Digite o nome da sua cidade">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label>CEP</label>
              <input type="text" class="form-control" class="form-control" name="addresses[postcode]" value="<?= $listAddresses['postcode'] ?>" id="postcode" placeholder="00000-000">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Bairro</label>
              <input type="text" class="form-control" class="form-control" name="addresses[neighborhood]" value="<?= $listAddresses['neighborhood'] ?>" placeholder="Digite o nome do seu bairro">
            </div>
          </div>
          <div class="col-md-5">
            <div class="form-group">
              <label>Rua</label>
              <input type="text" class="form-control" name="addresses[street]" value="<?= $listAddresses['street'] ?>" placeholder="Digite o nome da sua rua">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label>Numero</label>
              <input type="text" class="form-control" name="addresses[number]" value="<?= $listAddresses['number'] ?>" placeholder="Digite o numero da sua casa">
            </div>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Complemento</label>
              <input type="text" class="form-control" name="addresses[complement]" value="<?= $listAddresses['complement'] ?>" placeholder="Complemento">
            </div>
          </div>


          <div class="col-md-12">
            <div class="form-group">
              <button type="submit" name="create" class="text-white btn btn-primary">Cadastrar</button>
            </div>
          </div>

      </form>
    </div>


    <script>
      $(document).ready(function() {
        const table_references_dados = [{
            'codigo': '20101198',
            'name': "Teste e adaptação de lente de contato (sessão) - binocular"
          },
          {
            'codigo': '2114545',
            'name': "Teste e adaptação de lente de contato (sessão) - teste"
          },
        ]
        for (var i = 0; table_references_dados.length > i; i++) {
          $('#table_references').append('<option value="' + table_references_dados[i]['codigo'] + '"> ' + table_references_dados[i]['name'] + ' </option>');
        }
      });

      $("#table_references").on("change", function() {
        var descricao = $("#table_references option:selected").text();
        var value = $(this).val();
        $("#register_ans").val(value)
        $("#table_references_send").val(descricao)

      })
      $(document).ready(function() {
        $('#date_time').mask('00/00/0000 00:00:00');
        $('#cnpj').mask('00.000.000/0000-00')
        $('#crm').mask('0000 0000-0');
        $('#rg').mask('0000000000-0');
        $('#postcode').mask('00000-000');
        $('#phone_residential').mask('(00) 0000-0000');
        $('#phone_commercial').mask('(00) 0000-0000');
        $('#phone_us').mask('(00) 0 0000-0000');
        $('#cpf').mask('000.000.000-00');
        $('#value_unid_reference').mask('R$ 0000000000000000');

      });
    </script>
<?php

  } else {
    $url->redirect('error');
  }
} else {
  $url->redirect('home');
}
?>