<?php
include 'app/model/health_insurance.model.php';
include 'app/controller/health_insuranceDAO.php';


$health_insurance = new Health_insurance();
$health_insuranceDAO = new Health_insuranceDAO($db);
$list = $health_insuranceDAO->listAll();


if (!empty($_GET['remove_id'])) {
  $health_insurance->setId($_GET['remove_id']);
  $health_insuranceDAO->health_insurance = $health_insurance;
  $health_insuranceDAO->delete();
  $url->redirect('health/manage&remove=confirmed');
}
?>

<div class="bg-title">
  <h3 class="page-title">
    <i class="fa fa-heartbeat"></i> Convênios
    <i class="fa fa-angle-right"></i> <span class="text-primary">Gerenciar</span>
    <a class="btn btn-default" style="padding: 5px 10px;width: 32px;" href="health/create"><i class="fa fa-plus"></i></a>
  </h3>
</div>
<div class="row">
  <div class="col-md-12">
    <table class="infos-table" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nome</th>
          <th style="width: 64px;">Mais</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($list as $l) { ?>
          <tr>
            <td><?= $l['id'] ?></td>
            <td><?= $l['name'] ?></td>
            <td>
              <div class="row">
                <div class="col-md-5">
                  <a class="btn btn-default" style="padding: 5px;width: 32px;" href="./health/edit&health_id=<?= $l['id'] ?>"><i class="fa fa-edit"></i></a>
                </div>
                <div class="col-md-5">
                  <a style="padding: 5px;width: 32px;" href="./health/manage&remove_id=<?= $l['id'] ?>" class="btn btn-danger"><i class="fa fa-times"></i></a>
                </div>
              </div>
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>