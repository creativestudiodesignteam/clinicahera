<?php
include 'app/model/scheduling.model.php';
include 'app/controller/schedulingDAO.php';
$scheduling = new Scheduling();
$schedulingDAO = new SchedulingDAO($db);

include 'app/model/patients.model.php';
include 'app/controller/patientsDAO.php';
$patients = new Patients();
$patientsDAO = new PatientsDAO($db);

include 'app/model/health_insurances.model.php';
include 'app/controller/health_insurancesDAO.php';
$plan = new Health_insurances();
$planDAO = new Health_insurancesDAO($db);

include 'app/model/services.model.php';
include 'app/controller/servicesDAO.php';
$services = new Services();
$servicesDAO = new ServicesDAO($db);

include 'app/model/medics.model.php';
include 'app/controller/medicsDAO.php';
$medics = new Medics();
$medicsDAO = new MedicsDAO($db);

include 'app/model/users.model.php';
include 'app/controller/usersDAO.php';
$users = new Users;
$usersDAO = new UsersDAO($db);

$list = $schedulingDAO->listAll();

if (!empty($_GET['remove_id'])) {
  $scheduling->setId($_GET['remove_id']);
  $schedulingDAO->scheduling = $scheduling;
  $schedulingDAO->delete();
  $url->redirect('clinics/manage');
}

?>

<div class="bg-title">
  <h3 class="page-title">
    <i class="fa fa-calendar"></i> Agendamentos
    <i class="fa fa-angle-right"></i> <span class="text-primary">Listagem</span>
    <a class="btn btn-default" href="home?scheduling" style="padding: 5px 10px;width: 35px;"><i class="fa fa-plus"></i></a>
  </h3>
</div>
<div class="row">
  <div class="col-md-12">
    <table class="infos-table" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Medicos</th>
          <th>Pacientes</th>
          <th>date</th>
          <th>status</th>
          <th style="width: 64px;">Mais</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($list as $l) {
          $listPat = $patientsDAO->getById($l['patients_id']);
          $listMed = $medicsDAO->getById($l['medics_id']);
          $listUser = $usersDAO->getById($listMed['user_id']);

        ?>
          <tr>
            <td><?= $l['id'] ?></td>
            <td><?= $listUser['name'] ?></td>
            <td><?= $listPat['name'] ?></td>
            <td><?= $l['date'] ?></td>
            <?php if ($l['cancel_at'] == 1) { ?>
              <td>Cancelada</td>
            <?php } elseif ($l['confirm_at'] == 1) { ?>
              <td>Confirmado</td>
            <?php } else { ?>
              <td>Pendente</td>
            <?php } ?>
            <td>
              <div class="row">
                <div class="col-md-5">
                  <a class="btn btn-default" style="padding: 5px;width: 32px;" href="./scheduling/edit&scheduling_id=<?= $l['id'] ?>"><i class="fa fa-edit"></i></a>
                </div>
                <div class="col-md-5">
                  <a style="padding: 5px;width: 32px;" href="./scheduling/manage&remove_id=<?= $l['id'] ?>" class="btn btn-danger"><i class="fa fa-times"></i></a>
                </div>
              </div>
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>