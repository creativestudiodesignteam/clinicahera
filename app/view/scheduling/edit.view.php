<?php
include 'app/model/clinics.model.php';
include 'app/controller/clinicsDAO.php';
$clinics = new Clinics();
$clinicsDAO = new ClinicsDAO($db);
include 'app/model/scheduling.model.php';
include 'app/controller/schedulingDAO.php';
$scheduling = new Scheduling();
$schedulingDAO = new SchedulingDAO($db);

include 'app/model/patients.model.php';
include 'app/controller/patientsDAO.php';
$patients = new Patients();
$patientsDAO = new PatientsDAO($db);

include 'app/model/health_insurances.model.php';
include 'app/controller/health_insurancesDAO.php';
$plan = new Health_insurances();
$planDAO = new Health_insurancesDAO($db);

include 'app/model/services.model.php';
include 'app/controller/servicesDAO.php';
$services = new Services();
$servicesDAO = new ServicesDAO($db);

include 'app/model/medics.model.php';
include 'app/controller/medicsDAO.php';
$medics = new Medics();
$medicsDAO = new MedicsDAO($db);

include 'app/model/users.model.php';
include 'app/controller/usersDAO.php';
$users = new Users();
$usersDAO = new UsersDAO($db);

$listMed = $medicsDAO->listAll();
$listS = $schedulingDAO->listAll();
$listP = $patientsDAO->listAll();
$listPlan = $planDAO->listAll();
$listServ = $servicesDAO->listAll();
$listAgenda = $schedulingDAO->getSchedulingNow();

if (!empty($_GET['scheduling_id'])) {


  $info = $schedulingDAO->getById($_GET['scheduling_id']);

  $notify = array('', '');

  if (!empty($info)) {
    if (isset($_POST['medics_id'], $_POST['patients'], $_POST['plan'], $_POST['services'], $_POST['date'])) {
      $form = array(
        $_POST['medics_id'],
        $_POST['patients'],
        $_POST['plan'],
        $_POST['services'],
        $_POST['date'],
      );
      $scheduling->setId($info['id']);
      $scheduling->setMedics_id($form[0]);
      $scheduling->setPatients_id($form[1]);
      $scheduling->setPlan_id($form[2]);
      $scheduling->setServices_id($form[3]);
      $scheduling->setDate($form[4]);
      $schedulingDAO->scheduling = $scheduling;
      $schedulingDAO->update();
      $url->redirect('scheduling/manage');
    }

?>
    <div class="bg-title">
      <h3 class="page-title">
        <i class="fa fa-hospital-o"></i> Clinicas
        <i class="fa fa-angle-right"></i> <span class="text-primary">Listagem</span>
        <a class="btn btn-default" style="padding: 5px 10px;width: 32px;" href="companies/create"><i class="fa fa-plus"></i></a>
      </h3>
    </div>
    <div class="white-box">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h3 class="page-title">Editar clinica</h3>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

        </div>
      </div>
      <form method="post">


        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label for="Médicos">Médicos</label>
                <select class="form-control" name="medics_id" id="medics_id">
                  <option disabled>Selecionar Médico</option>
                  <?php
                  $medicsL = $medicsDAO->getById($info['medics_id']);
                  $userL = $usersDAO->getById($medicsL['user_id']);
                  ?>
                  <option name="<?= $userL['id'] ?>" selected value="<?= $userL['id'] ?>"> <?= $userL['name'] ?></option>

                  <?php foreach ($listMed as $l) {
                    $userL = $usersDAO->getById($l['user_id']);
                  ?>
                    <option name="<?= $l['id'] ?>" value="<?= $l['id'] ?>"><?= $userL['name'] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="row">

            <div class="col-md-8">
              <div class="form-group">
                <label>Pacientes</label>
                <select class="form-control" id="patients" name="patients">
                  <option selected disabled>Selecionar Paciente</option>
                  <?php
                  $patientsL = $patientsDAO->getById($info['patients_id']);

                  ?>
                  <option name="<?= $patientsL['id'] ?>" selected value="<?= $patientsL['id'] ?>"> <?= $patientsL['name'] ?></option>

                  <?php foreach ($listP as $l) { ?>
                    <option name="<?= $l['id'] ?>" value="<?= $l['id'] ?>" id="<?= $l['id'] ?>"><?= $l['name'] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="col-md-1">
              <label style="margin-top:30px;">ou</label>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <a href="patients/create" class="btn btn-primary" style="margin-top:24px;"><i class="fa fa-user-plus"></i></a>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>plano de saúde</label>
                <select class="form-control" name="plan" id="plan">
                  <option selected disabled>Selecionar Plano de saúde</option>
                  <?php
                  $planL = $planDAO->getById($info['plan_id']);

                  ?>
                  <option name="<?= $planL['id'] ?>" selected value="<?= $planL['id'] ?>"> <?= $planL['plan'] ?></option>

                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Serviço</label>
                <select class="form-control" name="services">
                  <option selected disabled>Selecionar Serviço</option>
                  <?php
                  $servL = $servicesDAO->getById($info['services_id']);

                  ?>
                  <option name="<?= $servL['id'] ?>" selected value="<?= $servL['id'] ?>"> <?= $servL['name'] ?> | R$ <?= $servL['value'] ?></option>

                  <?php foreach ($listServ as $l) { ?>
                    <option name="<?= $l['id'] ?>" value="<?= $l['id'] ?>"><?= $l['name'] ?> | R$ <?= $l['value'] ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <?php
                $data = explode(" ", $info['date']);
                $data = $data[0] . "T" . $data[1];
                ?>
                <label>Data do agendamento</label>
                <input type="datetime-local" class="form-control" name="date" format="" id="date" value="<?= $data ?>">
              </div>
            </div>
            <div class="col-md-6" id="div-telefone" name="div-telefone">
              <label for="">Telefone</label>
              <h5 id="text-cellphone"></h5>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <button type="submit" name="create" value="Cadastrar" class="btn btn-primary">Editar</button>
              </div>
            </div>
          </div>
      </form>
    </div>
<?php

  } else {
    $url->redirect('error');
  }
} else {
  $url->redirect('home');
}
?>