<?php

include 'app/model/addresses.model.php';
include 'app/controller/addressesDAO.php';
$addresses = new Addresses();
$addressesDAO = new AddressesDAO($db);

include 'app/model/patients.model.php';
include 'app/controller/patientsDAO.php';
$patientes = new patients();
$patientsDAO = new patientsDAO($db);

include 'app/model/medics.model.php';
include 'app/controller/medicsDAO.php';
$medics = new Medics();
$medicsDAO = new MedicsDAO($db);

include 'app/model/health_insurance.model.php';
include 'app/controller/health_insuranceDAO.php';
$health_insurances = new Health_insurance();
$health_insurancesDAO = new Health_insuranceDAO($db);

include 'app/model/categories.model.php';
include 'app/controller/categoriesDAO.php';
$categories = new Categories();
$categoriesDAO = new CategoriesDAO($db);

include 'app/model/plan.model.php';
include 'app/controller/planDAO.php';
$plan = new Plan();
$planDAO = new PlanDAO($db);


$notify = array('', '');

$listHealth = $health_insurancesDAO->listAll();

$listCategories = $categoriesDAO->listAll();
$listMedicos = $medicsDAO->listAll();


if (isset($_POST['patients'])) {
  $form = array(
    $_POST['patients'],
    $_POST['addresses'],
    $_POST['plan'],
  );

  $plan->setName($form[2]['name']);
  $plan->setAccommodation($form[2]['accomodation']);
  $plan->setWallet($form[2]['wallet']);
  $plan->setProduct($form[2]['product']);
  $plan->setFk_health($form[2]['fk_health']);
  $planDAO->plan = $plan;
  $fk_plan = $planDAO->insert();

  $addresses->setPostcode($form[1]['postcode']);
  $addresses->setState($form[1]['state']);
  $addresses->setCity($form[1]['city']);
  $addresses->setNeighborhood($form[1]['neighborhood']);
  $addresses->setStreet($form[1]['street']);
  $addresses->setNumber($form[1]['number']);
  $addresses->setComplement($form[1]['complement']);
  $addressesDAO->addresses = $addresses;
  $fk_addresses = $addressesDAO->insert();


  $patientes->setName($form[0]['name']);
  $patientes->setEmail($form[0]['email']);
  $patientes->setCpf($form[0]['cpf']);
  $patientes->setRg($form[0]['rg']);
  $patientes->setEmitting_organ($form[0]['emitting_organ']);
  $patientes->setSexo($form[0]['sexo']);
  $patientes->setBirth_place($form[0]['birth_place']);
  $patientes->setBirthday($form[0]['birthday']);
  $patientes->setGroups_references($form[0]['groups_references']);
  $patientes->setSituation($form[0]['situation']);
  $patientes->setSpecialty($form[0]['specialty']);
  $patientes->setTellphone_commercial($form[0]['tellphone_commercial']);
  $patientes->setTellphone_residential($form[0]['tellphone_residential']);
  $patientes->setCellphone($form[0]['cellphone']);
  $patientes->setName_message($form[0]['name_message']);
  $patientes->setFk_medic($form[0]['fk_medic']);
  $patientes->setFk_addresses($fk_addresses);
  $patientes->setFk_plan($fk_plan);



  $patientsDAO->patients = $patientes;

  $patientsDAO->insert();

  $url->redirect('patients/manage&create=successes');
}

?>


<div class="bg-title">
  <h3 class="page-title">
    <i class="fa fa-hospital-o"></i> Pacientes
    <i class="fa fa-angle-right"></i> <span class="text-primary">Cadastro</span>
    <a class="btn btn-default" style="padding: 5px 10px;width: 32px;" href="patients/create"><i class="fa fa-plus"></i></a>
  </h3>
</div>
<div class="white-box">
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h3 class="page-title">Cadastrar clinica</h3>
    </div>
  </div>

  <form method="post">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label>Medicos</label>
          <select name="patients[fk_medic]" class="form-control">
            <option selected disabled>-- Selecione uma opção --</option>
            <?php foreach ($listMedicos as $list) { ?>
              <option value="<?= $list['id'] ?>"><?= $list['name'] ?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Nome *</label>
          <input type="text" class="form-control" name="patients[name]" placeholder="Digite o nome da clinica">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Email *</label>
          <input type="email" class="form-control" name="patients[email]" placeholder="example@example.com">
        </div>
      </div>

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3 class="page-title">Dados Pessoais</h3>
      </div>

      <!-- //tellphone_residential tellphone_commercial cellphone tellphone_message name_message-->

      <div class="col-md-4">
        <div class="form-group">
          <label>Grupo</label>
          <select name="patients[groups_references]" class="form-control">
            <option selected disabled>-- Selecione uma opção --</option>
            <option value="Definitivo">Definitivo</option>
            <option value="Pré-tratamento">Pré-tratamento</option>
            <option value="Excluido">Excluido</option>
            <option value="Contenção">Contenção</option>
            <option value="Clinico">Clinico</option>
          </select>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Situação</label>
          <select name="patients[situation]" class="form-control">
            <option selected disabled>-- Selecione uma opção --</option>
            <option value="1° Consulta">1° Consulta</option>
            <option value="1° Revisão">1° Revisão</option>
            <option value="2° Revisão">2° Revisão</option>
            <option value="3° Revisão">3° Revisão</option>
            <option value="4° Revisão">4° Revisão</option>
            <option value="5° Revisão">5° Revisão</option>
            <option value="Abandono">Abandono</option>
            <option value="Acompanhamento">Acompanhamento</option>
            <option value="Advogado">Advogado</option>
            <option value="Alta">Alta</option>
            <option value="Alta Temporaria">Alta Temporaria</option>
            <option value="Andamento">Andamento</option>
            <option value="Parcelas em atraso">Parcelas em atraso</option>
            <option value="Convênio em andamento">Convênio em andamento</option>
            <option value="Convênio Finalizou">Convênio Finalizou</option>
            <option value="Pericia">Pericia</option>
            <option value="Convênio Aprovação">Convênio Aprovação</option>
            <option value="Em contenção">Em contenção</option>
            <option value="Em tratamento">Em tratamento</option>
            <option value="Faltando documento">Faltando documento</option>
            <option value="Finalizou Devedor">Finalizou Devedor</option>
            <option value="Finalizou removeu">Finalizou removeu</option>
            <option value="Inativo">Inativo</option>
            <option value="Não finalizou">Não finalizou</option>
            <option value="SPC">SPC</option>
          </select>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Especialidade</label>
          <select name="patients[specialty]" class="form-control">
            <option selected disabled>-- Selecione uma opção --</option>
            <?php foreach ($listCategories as $categories) {
            ?>
              <option value="<?= $categories['name'] ?>"><?= $categories['name'] ?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Orgão Emissor</label>
          <input type="text" class="form-control" name="patients[emitting_organ]" placeholder="Ex:SP">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>CPF</label>
          <input type="text" class="form-control" name="patients[cpf]" placeholder="000.000.000.00" id="cpf">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>RG</label>
          <input type="text" class="form-control" name="patients[rg]" placeholder="00000000000-0" id="rg">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Sexo</label>
          <select name="patients[sexo]" class="form-control">
            <option selected disabled>-- Selecione uma opção --</option>
            <option value="masculino">Masculino</option>
            <option value="feminino">Feminino</option>
          </select>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Local de Nascimento</label>
          <input type="text" class="form-control" name="patients[birth_place]" placeholder="Ex: São paulo">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Data de nascimento</label>
          <input type="date" class="form-control" name="patients[birthday]" placeholder="patients nascimento" value="2000-01-01">
        </div>
      </div>

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3 class="page-title">Contatos</h3>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label>Nome para contato</label>
          <input type="text" class="form-control" name="patients[name_message]" placeholder="Digite seu nome para contato">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Telefone Residencial</label>
          <input type="text" class="form-control" name="patients[tellphone_residential]" id="phone_residential" placeholder="(00) 0000-0000">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Telefone Comercial</label>
          <input type="text" class="form-control" name="patients[tellphone_commercial]" id="phone_commercial" placeholder="(00) 0000-0000">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Celular</label>
          <input type="text" class="form-control" name="patients[cellphone]" id="phone_us" placeholder="(00) 0 0000-0000">
        </div>
      </div>

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3 class="page-title">Dados do Plano</h3>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <label>Cônvenio</label>
          <select class="form-control" name="plan[fk_health]" id="accomodation">
            <option selected disabled> -- Selecione uma opção --</option>
            <?php foreach ($listHealth as $listHeal) { ?>
              <option value="<?= $listHeal['id'] ?>"><?= $listHeal['name'] ?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Nome</label>
          <input type="text" class="form-control" class="form-control" name="plan[name]" placeholder="Digite o nome do plano">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Acomodação</label>
          <select class="form-control" name="plan[accomodation]" id="accomodation">
            <option selected disabled> -- Selecione uma opção --</option>
            <option value="Enfermaria">Enfermaria</option>
            <option value="Apartamento">Apartamento</option>
          </select>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Número da Carteirinha</label>
          <input type="text" class="form-control" class="form-control" name="plan[wallet]" placeholder="Digite o numero da carteirinha">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Produto (CID-10)</label>
          <input type="text" class="form-control" class="form-control" name="plan[product]" placeholder="Digite codigo do produto">
        </div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h3 class="page-title">Endereço</h3>
      </div>
      <!-- //postcode state city neighborhood street number complement -->
      <div class="col-md-4">
        <div class="form-group">
          <label>Estados</label>
          <select name="addresses[state]" class="form-control">
            <option selected disabled>Estados</option>
            <option value="AC">Acre</option>
            <option value="AL">Alagoas</option>
            <option value="AP">Amapá</option>
            <option value="AM">Amazonas</option>
            <option value="BA">Bahia</option>
            <option value="CE">Ceará</option>
            <option value="DF">Distrito Federal</option>
            <option value="ES">Espírito Santo</option>
            <option value="GO">Goiás</option>
            <option value="MA">Maranhão</option>
            <option value="MT">Mato Grosso</option>
            <option value="MS">Mato Grosso do Sul</option>
            <option value="MG">Minas Gerais</option>
            <option value="PA">Pará</option>
            <option value="PB">Paraíba</option>
            <option value="PR">Paraná</option>
            <option value="PE">Pernambuco</option>
            <option value="PI">Piauí</option>
            <option value="RJ">Rio de Janeiro</option>
            <option value="RN">Rio Grande do Norte</option>
            <option value="RS">Rio Grande do Sul</option>
            <option value="RO">Rondônia</option>
            <option value="RR">Roraima</option>
            <option value="SC">Santa Catarina</option>
            <option value="SP">São Paulo</option>
            <option value="SE">Sergipe</option>
            <option value="TO">Tocantins</option>
          </select>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Cidade</label>
          <input type="text" class="form-control" class="form-control" name="addresses[city]" placeholder="Digite o nome da sua cidade">
        </div>
      </div>

      <div class="col-md-4">
        <div class="form-group">
          <label>CEP</label>
          <input type="text" class="form-control" class="form-control" name="addresses[postcode]" id="postcode" placeholder="00000-000">
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-group">
          <label>Bairro</label>
          <input type="text" class="form-control" class="form-control" name="addresses[neighborhood]" placeholder="Digite o nome do seu bairro">
        </div>
      </div>
      <div class="col-md-5">
        <div class="form-group">
          <label>Rua</label>
          <input type="text" class="form-control" name="addresses[street]" placeholder="Digite o nome da sua rua">
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <label>Numero</label>
          <input type="text" class="form-control" name="addresses[number]" placeholder="Digite o numero da sua casa">
        </div>
      </div>


      <div class="col-md-12">
        <div class="form-group">
          <label>Complemento</label>
          <input type="text" class="form-control" name="addresses[complement]" placeholder="Complemento">
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <button type="submit" name="create" class="text-white btn btn-primary">Cadastrar</button>
        </div>
      </div>

  </form>
</div>
<script>
  $(document).ready(function() {
    $('#date_time').mask('00/00/0000 00:00:00');
    $('#cnpj').mask('00.000.000/0000-00')
    $('#crm').mask('0000 0000-0');
    $('#rg').mask('0000000000-0');
    $('#postcode').mask('00000-000');
    $('#phone_residential').mask('(00) 0000-0000');
    $('#phone_commercial').mask('(00) 0000-0000');
    $('#phone_us').mask('(00) 0 0000-0000');
    $('#cpf').mask('000.000.000-00')

  });
</script>