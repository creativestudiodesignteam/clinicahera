<?php


include 'app/model/patients.model.php';
include 'app/controller/patientsDAO.php';
$patients = new Patients();
$patientsDAO = new PatientsDAO($db);
$list = $patientsDAO->listAll();


if (!empty($_GET['patients_id'])) {
  $patients->setId($_GET['patients_id']);
  $patientsDAO->patients = $patients;
  $patientsDAO->delete();
}
?>


<div class="bg-title">
  <h3 class="page-title">
    <i class="fa fa-wheelchair"></i> Pacientes
    <i class="fa fa-angle-right"></i> <span class="text-primary">Listagem</span>
    <a class="btn btn-default" style="padding: 5px 10px;width: 32px;" href="patients/create"><i class="fa fa-plus"></i></a>
  </h3>
</div>
<div class="row">
  <div class="col-md-12">
    <table class="infos-table" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nome</th>
          <th>E-mail</th>
          <th>Telefone</th>
          <th style="width: 64px;">Mais</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($list as $l) { ?>
          <tr>
            <td><?= $l['id'] ?></td>
            <td><?= $l['name'] ?></td>
            <td><?= $l['email'] ?></td>
            <td><?= $l['cellphone'] ?></td>
            <td>
              <div class="row">
                <div class="col-md-5">
                  <a class="btn btn-default" style="padding: 5px;width: 32px;" href="./patients/edit&patients_id=<?= $l['id'] ?>"><i class="fa fa-edit"></i></a>
                </div>
                <div class="col-md-5">
                  <a style="padding: 5px;width: 32px;" href="./patients/manage&patients_id=<?= $l['id'] ?>" class="btn btn-danger"><i class="fa fa-times"></i></a>
                </div>
              </div>
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>