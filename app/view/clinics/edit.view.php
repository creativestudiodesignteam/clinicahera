<?php
include 'app/model/clinics.model.php';
include 'app/controller/clinicsDAO.php';
include 'app/model/addresses.model.php';
include 'app/controller/addressesDAO.php';
$addresses = new Addresses();
$addressesDAO = new AddressesDAO($db);

$clinics = new Clinics();
$clinicsDAO = new ClinicsDAO($db);

if (!empty($_GET['clinics_id'])) {


  $info = $clinicsDAO->getById($_GET['clinics_id']);
  $listAddresses = $addressesDAO->getById($info['fk_addresses']);

  $notify = array('', '');

  if (!empty($info)) {
    if (isset($_POST['clinics'], $_POST['addresses'])) {
      $form = array(
        $_POST['clinics'],
        $_POST['addresses'],
      );
      $addresses->setId($info['fk_addresses']);
      $addresses->setPostcode($form[1]['postcode']);
      $addresses->setState($form[1]['state']);
      $addresses->setCity($form[1]['city']);
      $addresses->setNeighborhood($form[1]['neighborhood']);
      $addresses->setStreet($form[1]['street']);
      $addresses->setNumber($form[1]['number']);
      $addresses->setComplement($form[1]['complement']);
      $addressesDAO->addresses = $addresses;
      $fk_addresses = $addressesDAO->update();

      $clinics->setId($info['id']);
      $clinics->setName($form[0]['name']);
      $clinics->setCnpj($form[0]['cnpj']);
      $clinics->setEmail($form[0]['email']);
      $clinics->setCellphone($form[0]['cellphone']);
      $clinics->setTellphone($form[0]['tellphone']);
      $clinics->setFk_addresses($info['fk_addresses']);
      $clinicsDAO->clinics = $clinics;
      $clinicsDAO->update();
      $url->redirect('clinics/manage&edit=successes');
    }

?>
    <div class="bg-title">
      <h3 class="page-title">
        <i class="fa fa-hospital-o"></i> Clínicas
        <i class="fa fa-angle-right"></i> <span class="text-primary">Atualizar</span>
        <a class="btn btn-default" style="padding: 5px 10px;width: 32px;" href="companies/create"><i class="fa fa-plus"></i></a>
      </h3>
    </div>
    <div class="white-box">
      <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
          <h3 class="page-title">Editar clinica</h3>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

        </div>
      </div>
      <form method="post">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>Nome *</label>
              <input type="text" class="form-control" name="clinics[name]" value="<?= $info['name'] ?>" placeholder="Digite o nome da clinica">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>CNPJ *</label>
              <input type="text" class="form-control" name="clinics[cnpj]" id="cnpj" value="<?= $info['cnpj'] ?>" placeholder="00.000.000./0000-00">
            </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3 class="page-title">Contatos</h3>
          </div>
          <div class="col-md-12">
            <div class="form-group">
              <label>Email *</label>
              <input type="email" class="form-control" name="clinics[email]" value="<?= $info['email'] ?>" placeholder="example@example.com">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Celular</label>
              <input type="text" class="form-control" name="clinics[cellphone]" id="phone_us" value="<?= $info['cellphone'] ?>" placeholder="(00) 0 0000-0000">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label>Telefone</label>
              <input type="text" class="form-control" name="clinics[tellphone]" id="phone_residential" value="<?= $info['tellphone'] ?>" placeholder="(00) 0000-0000">
            </div>
          </div>


          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h3 class="page-title">Endereço</h3>
          </div>
          <!-- //postcode state city neighborhood street number complement -->
          <div class="col-md-4">
            <div class="form-group">
              <label>Estados</label>
              <select name="addresses[state]" class="form-control">
                <option selected value="<?= $listAddresses['state'] ?>">-- <?= $listAddresses['state'] ?> --</option>
                <option value="AC">AC</option>
                <option value="AL">AL</option>
                <option value="AP">AP</option>
                <option value="AM">AM</option>
                <option value="BA">BA</option>
                <option value="CE">CE</option>
                <option value="DF">DF</option>
                <option value="ES">ES</option>
                <option value="GO">GO</option>
                <option value="MA">MA</option>
                <option value="MT">MT</option>
                <option value="MS">MS</option>
                <option value="MG">MG</option>
                <option value="PA">PA</option>
                <option value="PB">PB</option>
                <option value="PR">PR</option>
                <option value="PE">PE</option>
                <option value="PI">PI</option>
                <option value="RJ">RJ</option>
                <option value="RN">RN</option>
                <option value="RS">RS</option>
                <option value="RO">RO</option>
                <option value="RR">RR</option>
                <option value="SC">SC</option>
                <option value="SP">SP</option>
                <option value="SE">SE</option>
                <option value="TO">TO</option>
              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Cidade</label>
              <input type="text" class="form-control" class="form-control" name="addresses[city]" value="<?= $listAddresses['city'] ?>" placeholder="Digite o nome da sua cidade">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label>CEP</label>
              <input type="text" class="form-control" class="form-control" name="addresses[postcode]" value="<?= $listAddresses['postcode'] ?>" id="postcode" placeholder="00000-000">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label>Bairro</label>
              <input type="text" class="form-control" class="form-control" name="addresses[neighborhood]" value="<?= $listAddresses['neighborhood'] ?>" placeholder="Digite o nome do seu bairro">
            </div>
          </div>
          <div class="col-md-5">
            <div class="form-group">
              <label>Rua</label>
              <input type="text" class="form-control" name="addresses[street]" value="<?= $listAddresses['street'] ?>" placeholder="Digite o nome da sua rua">
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label>Numero</label>
              <input type="text" class="form-control" name="addresses[number]" value="<?= $listAddresses['number'] ?>" placeholder="Digite o numero da sua casa">
            </div>
          </div>


          <div class="col-md-12">
            <div class="form-group">
              <label>Complemento</label>
              <input type="text" class="form-control" name="addresses[complement]" value="<?= $listAddresses['complement'] ?>" placeholder="Complemento">
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <button type="submit" name="create" class="text-white btn btn-primary">Cadastrar</button>
            </div>
          </div>

      </form>
    </div>
    <script>
      $(document).ready(function() {
        $('#date_time').mask('00/00/0000 00:00:00');
        $('#cnpj').mask('00.000.000/0000-00')
        $('#crm').mask('0000 0000-0');
        $('#rg').mask('0000000000-0');
        $('#postcode').mask('00000-000');
        $('#phone_residential').mask('(00) 0000-0000');
        $('#phone_commercial').mask('(00) 0000-0000');
        $('#phone_us').mask('(00) 0 0000-0000');
        $('#cpf').mask('000.000.000-00')

      });
    </script>
<?php

  } else {
    $url->redirect('error');
  }
} else {
  $url->redirect('home');
}
?>