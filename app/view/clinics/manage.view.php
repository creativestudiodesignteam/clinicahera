<?php
include 'app/model/clinics.model.php';
include 'app/controller/clinicsDAO.php';
$clinics = new Clinics();
$clinicsDAO = new ClinicsDAO($db);
$list = $clinicsDAO->listAll();


if (!empty($_GET['remove_id'])) {
  $clinics->setId($_GET['remove_id']);
  $clinicsDAO->clinics = $clinics;
  $clinicsDAO->delete();
  $url->redirect('clinics/manage');
}
?>

<div class="bg-title">
  <h3 class="page-title">
    <i class="fa fa-hospital-o"></i> Clínicas
    <i class="fa fa-angle-right"></i> <span class="text-primary">Gerenciar</span>
    <a class="btn btn-default" style="padding: 5px 10px;width: 32px;" href="clinics/create"><i class="fa fa-plus"></i></a>
  </h3>
</div>
<div class="row">
  <div class="col-md-12">
    <table class="infos-table" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nome</th>
          <th style="width: 64px;">Mais</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($list as $l) { ?>
          <tr>
            <td><?= $l['id'] ?></td>
            <td><?= $l['name'] ?></td>
            <td>
              <div class="row">
                <div class="col-md-5">
                  <a class="btn btn-default" style="padding: 5px;width: 32px;" href="./clinics/edit&clinics_id=<?= $l['id'] ?>"><i class="fa fa-edit"></i></a>
                </div>
                <div class="col-md-5">
                  <a style="padding: 5px;width: 32px;" href="./clinics/manage&remove_id=<?= $l['id'] ?>" class="btn btn-danger"><i class="fa fa-times"></i></a>
                </div>
              </div>
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>