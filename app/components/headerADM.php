<?php
global $url;
date_default_timezone_set('America/Sao_Paulo'); // Hora oficial do Brasil.

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?= $url->title(); ?></title>
    <base href="https://<?= $_SERVER['SERVER_NAME'] ?>/ClinicaHera/">
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <script src="public/assets/js/script.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="public/assets/css/app.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css" rel="stylesheet" />

</head>

<?php if (isset($_SESSION['id'])) { ?>

    <body class="home">
        <div class="page-wrapper">
            <div class="container-fluid">
                <nav class="navbar navbar-fixed-top">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i class="fa fa-bars" aria-hidden="true"></i></button>
                            <a class="navbar-brand" href="home">Clínica Hera</a>
                        </div>
                    </div>
                </nav>

                <div class="row">
                    <div class="col-sm-3 col-md-2 sidebar">
                        <div id="navbar" class="navbar-collapse collapse teste">
                            <ul class="nav nav-sidebar">
                                <li class="header">Navegação</li>
                                <li><a href="home"><i class="fa fa-home"></i> <strong>Home</strong> </a></li>
                                <li><a href="patients/manage"><i class="fa fa-wheelchair"></i><strong>Pacientes</strong></a></li>
                                <li class="header">Cadastros</li>
                                <li><a href="hospital/manage"><i class="fa fa-h-square"></i> <strong>Hospital</strong></a></li>
                                <li><a href="users/manage"><i class="fa fa-user"></i> <strong>Usuários</strong></a></li>
                                <li><a href="clinics/manage"><i class="fa fa-hospital-o"></i><strong>Clínicas</strong></a></li>
                                <li><a href="health/manage"><i class="fa fa-heartbeat"></i> <strong>Convênios</strong></a></li>
                                <li><a href="medics/manage"><i class="fa fa-user-md"></i><strong>Médicos</strong></a></li>
                                <li><a href="consultancies/manage"><i class="fa fa-medkit"></i> <strong>Consultórios</strong></a></li>
                                <li><a href="procedures/manage"><i class="fa fa-newspaper-o"></i> <strong>Procedimentos</strong></a></li>
                                <li><a href="financial/manage"><i class="fa fa-money" aria-hidden="true"></i> <strong>Financeiro</strong></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php }
        /*
             <li><a href="services/manage"><i class="fa fa-bookmark-o"></i><strong>Serviços</strong></a></li>
                                <li><a href="financial/manage"><i class="fa fa-usd"></i><strong>Financeiro</strong></a></li>
            
            */

            ?>