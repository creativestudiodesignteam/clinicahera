<?php
// Made by Guilherme Girão (https://github.com/guilhermegirao)

class URL extends Title {
    
    private $link = '';
    private $dir  = 'app/view/';
    private $admin = array('home', 'services/create' , 'services/edit', 'services/manage', 'consultancies/create', 'consultancies/edit', 'consultancies/manage', 'clinics/create', 'clinics/edit', 'clinics/manage', 'companies/create', 'companies/edit', 'companies/manage', 'medics/create', 'medics/edit', 'medics/manage', 'patients/create', 'patients/edit', 'patients/manage', 'users/create', 'users/edit', 'users/manage', 'scheduling/edit', 'scheduling/manage', 'financial/manage');
    private $medic = array('home', 'medics/edit', 'medics/manage', );
    private $user = array('home');

    public function __construct () {
        $get = 'page';

        if (empty($_GET[$get])) $_GET[$get] = 'home';

        $this->link = $_GET[$get];

        if (!file_exists($this->dir.$this->link.'.view.php')) $this->link = 'error';
    }

    public function page () {
        global $db, $url, $components;

        if (empty($_SESSION['id'])) {
            $this->link = 'login';
        }

        /*if (isset($_SESSION['fk_group']) && $_SESSION['fk_group'] == 1 && !in_array($this->link, $this->admin)) {
            $this->link = 'errorpermission';
        }

        if (isset($_SESSION['fk_group']) && $_SESSION['fk_group'] == 2 && !in_array($this->link, $this->medic)) {
            $this->link = 'errorpermission';
        }

        if (isset($_SESSION['fk_group']) && $_SESSION['fk_group'] == 3 && !in_array($this->link, $this->user)) {
            $this->link = 'errorpermission';
        }*/

        include ($this->dir.$this->link.'.view.php');
    }

    public function redirect ($page) {
        echo '<script>window.location = "./'.$page.'"</script>';
    }

    public function title () {
        return $this->pageTitle($this->link);
    }

    public function active ($page) {
        if ($this->link == $page) {
            return 'active';
        } else {
            return '';
        }
    }
    
    public function filter ($string) {
        $string = htmlspecialchars(addslashes(strip_tags($string)));
        return $string;
    }

}
