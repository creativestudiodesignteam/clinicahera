<?php
// Made by Guilherme Girão (https://github.com/guilhermegirao)

class Title
{

    public $titles = array(
        'error' => 'ERROR!',
        'errorpermission' => 'ERROR!',
        'home'  => 'Página Inicial',
        'login' => 'Entrar',

        'services/create' => 'Cadastrar Serviços',
        'services/edit' => 'Editar Serviços',
        'services/manage' => 'Gerenciar Serviços',

        'consultancies/edit' => 'Editar Consultas',
        'consultancies/create' => 'Cadastrar Consultas',
        'consultancies/manage' => 'Gerenciar Consultas',

        'clinics/create' => 'Cadastrar Clínicas',
        'clinics/edit' => 'Editar Clínicas',
        'clinics/manage' => 'Gerenciar Clínicas',

        'companies/create' => 'Cadastrar Companhias de saúde',
        'companies/edit' => 'Editar Companhias de saúde',
        'companies/manage' => 'Gerenciar Companhias de saúde',

        'medics/create' => 'Cadastrar Médicos',
        'medics/edit' => 'Editar Médicos',
        'medics/manage' => 'Gerenciar Médicos',

        'patients/create' => 'Cadastrar Pacientes',
        'patients/edit' => 'Editar Pacientes',
        'patients/manage' => 'Gerenciar Pacientes',

        'users/create' => 'Cadastrar Usuários',
        'users/edit' => 'Editar Usuários',
        'users/manage' => 'Gerenciar Usuários',
        
        'health/create' => 'Cadastrar Convênios',
        'health/edit' => 'Editar Convênios',
        'health/manage' => 'Gerenciar Convênios',
        
        'procedures/create' => 'Cadastrar Procedimentos',
        'procedures/edit' => 'Editar Procedimentos',
        'procedures/manage' => 'Gerenciar Procedimentos',

        'hospital/create' => 'Cadastrar Hospital',
        'hospital/edit' => 'Editar Hospital',
        'hospital/manage' => 'Gerenciar Hospital',

        'scheduling/edit' => 'Editar Agendamento',
        'scheduling/manage' => 'Gerenciar Agendamentos',
        'financial/manage' => 'Gerenciar Financeiro',

        'table_tuss/edit' => 'Editar Tabela Tuss',
        'table_tuss/manage' => 'Gerenciar Tabela Tuss',
        'table_tuss/manage' => 'Gerenciar Tabela Tuss',

        'calendar' => 'Calendário',        
    );

    public function pageTitle($key)
    {
        if (array_key_exists($key, $this->titles)) {
            return $this->titles[$key];
        } else {
            return $this->titles['error'];
        }
    }
}
