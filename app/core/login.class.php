<?php

class Login
{

    public $email;
    public $password;

    public function join()
    {
        global $db, $url, $components;
        $request = $db->prepare('SELECT * FROM `user` WHERE email = :email && password = :password');

        $request->bindValue(':email', $this->email);
        $request->bindValue(':password', $this->password);
        $request->execute();
        if (!isset($_SESSION['id'])) {
            if ($request->rowCount() == 1) {
                $value = $request->fetch(PDO::FETCH_ASSOC);
                $_SESSION['id'] = $value['id'];
                $_SESSION['name'] = $value['name'];
                $_SESSION['email'] = $value['email'];
                //$_SESSION['fk_group'] = $value['fk_group'];
                $url->redirect('home');
            } else {
                $components->notify('danger', '<b>Dados incorretos!</b> Por favor, insira as credenciais corretas.');
            }
        }
    }
    
    public function filter($string)
    {
        $string = htmlspecialchars(addslashes(strip_tags($string)));
        return $string;
    }
}
