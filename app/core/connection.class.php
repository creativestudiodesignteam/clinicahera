<?php

class Connection
{

    private $data = array(
        /*'host' => 'localhost',
        'database' => 'hera_admin',
        'username' => 'root',
        'password' => '',*/

       'host' => 'localhost',
        'database' => 'devdesig_heracl-admin',
        'username' => 'devdesig_admin_t',
        'password' => 'admin',
    );

    public function connect()
    {
        $pdo = new PDO(
            'mysql:dbname=' . $this->data['database'] .
                '; charset=utf8; host' . $this->data['host'],
            $this->data['username'],
            $this->data['password']
        );

        return $pdo;
    }
}
