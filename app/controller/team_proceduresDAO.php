<?php

class Team_proceduresDAO
{

    public $team_procedures;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `team_procedures` (name, specialty, percentage, fk_procedure) VALUES (:name, :specialty, :percentage,:fk_procedure)");

        $insert->bindValue(":name", $this->team_procedures->getName());
        $insert->bindValue(":specialty", $this->team_procedures->getSpecialty());
        $insert->bindValue(":percentage", $this->team_procedures->getPercentage());
        $insert->bindValue(":fk_procedure", $this->team_procedures->getFk_procedure());

        if ($insert->execute()) {
            return $this->db->lastInsertId();
        } else {
            return false;
        }
    }

    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `team_procedures` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }
    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `team_procedures`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `team_procedures` WHERE `id` = :id");
        $delete->bindValue(":id", $this->team_procedures->getId());

        $delete->execute();
    }

    public function update()
    {
        $update = $this->db->prepare("UPDATE  `team_procedures` SET name = :name, specialty = :specialty, fk_procedure = :fk_procedure percentage = :percentage, create_at = :create_at, update_at = :update_at WHERE id = :id");

        $update->bindValue(":id", $this->team_procedures->getId());
        $update->bindValue(":name", $this->team_procedures->getName());
        $update->bindValue(":specialty", $this->team_procedures->getSpecialty());
        $update->bindValue(":percentage", $this->team_procedures->getPercentage());
        $update->bindValue(":fk_procedure", $this->team_procedures->getFk_procedure());
        $update->bindValue(":create_at", $this->team_procedures->getCreate_at());
        $update->bindValue(":update_at", $this->team_procedures->getUpdate_at());

        $update->execute();
    }
    
}
