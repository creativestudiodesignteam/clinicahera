<?php

class Health_insuranceDAO
{

    public $health_insurance;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `health_insurance` (name, email, site, cnpj, login, password, tellphone_commercial, tellphone_residential, table_references, register_ans, fk_addresses) VALUES (:name, :email, :site, :cnpj, :login, :password, :tellphone_commercial, :tellphone_residential, :table_references, :register_ans, :fk_addresses)");

        $insert->bindValue(":name", $this->health_insurance->getName());
        $insert->bindValue(":email", $this->health_insurance->getEmail());
        $insert->bindValue(":site", $this->health_insurance->getSite());
        $insert->bindValue(":cnpj", $this->health_insurance->getCnpj());
        $insert->bindValue(":login", $this->health_insurance->getLogin());
        $insert->bindValue(":password", $this->health_insurance->getPassword());
        $insert->bindValue(":tellphone_commercial", $this->health_insurance->getTellphone_commercial());
        $insert->bindValue(":tellphone_residential", $this->health_insurance->getTellphone_residential());
        $insert->bindValue(":table_references", $this->health_insurance->getTable_references());
        $insert->bindValue(":register_ans", $this->health_insurance->getRegister_ans());
        $insert->bindValue(":fk_addresses", $this->health_insurance->getFk_addresses());

        $insert->execute();
    }
    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `health_insurance` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }

    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `health_insurance`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `health_insurance` WHERE `id` = :id");
        $delete->bindValue(":id", $this->health_insurance->getId());

        $delete->execute();
    }

    public function update()
    {
        $update = $this->db->prepare("UPDATE  `health_insurance` SET name = :name, email = :email, site = :site, cnpj = :cnpj, login = :login, password = :password, tellphone_commercial = :tellphone_commercial, tellphone_residential = :tellphone_residential, table_references = :table_references, register_ans = :register_ans, fk_addresses = :fk_addresses, create_at = :create_at, update_at = :update_at WHERE id = :id");

        $update->bindValue(":id", $this->health_insurance->getId());
        $update->bindValue(":name", $this->health_insurance->getName());
        $update->bindValue(":email", $this->health_insurance->getEmail());
        $update->bindValue(":site", $this->health_insurance->getSite());
        $update->bindValue(":cnpj", $this->health_insurance->getCnpj());
        $update->bindValue(":login", $this->health_insurance->getLogin());
        $update->bindValue(":password", $this->health_insurance->getPassword());
        $update->bindValue(":tellphone_commercial", $this->health_insurance->getTellphone_commercial());
        $update->bindValue(":tellphone_residential", $this->health_insurance->getTellphone_residential());
        $update->bindValue(":table_references", $this->health_insurance->getTable_references());
        $update->bindValue(":register_ans", $this->health_insurance->getRegister_ans());
        $update->bindValue(":fk_addresses", $this->health_insurance->getFk_addresses());
        $update->bindValue(":create_at", $this->health_insurance->getCreate_at());
        $update->bindValue(":update_at", $this->health_insurance->getUpdate_at());

        $update->execute();
    }
}
