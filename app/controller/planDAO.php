<?php 

class PlanDAO {

    public $plan;
    private $db;

    public function __construct ($db) {
        $this->db = $db;
    }

    public function insert () {
        $insert = $this->db->prepare("INSERT INTO `plan` (name, accommodation, wallet, product, fk_health) VALUES (:name, :accommodation, :wallet, :product, :fk_health)");

        $insert->bindValue(":name", $this->plan->getName());
        $insert->bindValue(":accommodation", $this->plan->getAccommodation());
        $insert->bindValue(":wallet", $this->plan->getWallet());
        $insert->bindValue(":product", $this->plan->getProduct());
        $insert->bindValue(":fk_health", $this->plan->getFk_health());

        if ($insert->execute()) {
            return $this->db->lastInsertId();
        } else {
            return false;
        }
    }
    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `plan` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }


    public function listAll () {
        $list = $this->db->prepare("SELECT * FROM `plan`");
        $list->execute();
        
        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete () {
        $delete = $this->db->prepare("DELETE FROM `plan` WHERE `id` = :id");
        $delete->bindValue(":id", $this->plan->getId());
        
        $delete->execute();
    }

    public function update () {
        $update = $this->db->prepare("UPDATE  `plan` SET name = :name, accommodation = :accommodation, wallet = :wallet, product = :product, fk_health = :fk_health WHERE id = :id");

        $update->bindValue(":id", $this->plan->getId());
        $update->bindValue(":name", $this->plan->getName());
        $update->bindValue(":accommodation", $this->plan->getAccommodation());
        $update->bindValue(":wallet", $this->plan->getWallet());
        $update->bindValue(":product", $this->plan->getProduct());
        $update->bindValue(":fk_health", $this->plan->getFk_health());

        $update->execute();
    }


}