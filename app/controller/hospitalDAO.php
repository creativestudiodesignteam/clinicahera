<?php 

class HospitalDAO {

    public $hospital;
    private $db;

    public function __construct ($db) {
        $this->db = $db;
    }

    public function insert () {
        $insert = $this->db->prepare("INSERT INTO `hospital` (name, email, reason_social, cnpj, tellphone, fk_addresses, create_at, update_at) VALUES (:name, :email, :reason_social, :cnpj, :tellphone, :fk_addresses, :create_at, :update_at)");

        $insert->bindValue(":name", $this->hospital->getName());
        $insert->bindValue(":email", $this->hospital->getEmail());
        $insert->bindValue(":reason_social", $this->hospital->getReason_social());
        $insert->bindValue(":cnpj", $this->hospital->getCnpj());
        $insert->bindValue(":tellphone", $this->hospital->getTellphone());
        $insert->bindValue(":fk_addresses", $this->hospital->getFk_addresses());
        $insert->bindValue(":create_at", $this->hospital->getCreate_at());
        $insert->bindValue(":update_at", $this->hospital->getUpdate_at());

        $insert->execute();
    }
    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `hospital` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }
    public function listAll () {
        $list = $this->db->prepare("SELECT * FROM `hospital`");
        $list->execute();
        
        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete () {
        $delete = $this->db->prepare("DELETE FROM `hospital` WHERE `id` = :id");
        $delete->bindValue(":id", $this->hospital->getId());
        
        $delete->execute();
    }

    public function update () {
        $update = $this->db->prepare("UPDATE  `hospital` SET name = :name, email = :email, reason_social = :reason_social, cnpj = :cnpj, tellphone = :tellphone, fk_addresses = :fk_addresses, create_at = :create_at, update_at = :update_at WHERE id = :id");

        $update->bindValue(":id", $this->hospital->getId());
        $update->bindValue(":name", $this->hospital->getName());
        $update->bindValue(":email", $this->hospital->getEmail());
        $update->bindValue(":reason_social", $this->hospital->getReason_social());
        $update->bindValue(":cnpj", $this->hospital->getCnpj());
        $update->bindValue(":tellphone", $this->hospital->getTellphone());
        $update->bindValue(":fk_addresses", $this->hospital->getFk_addresses());
        $update->bindValue(":create_at", $this->hospital->getCreate_at());
        $update->bindValue(":update_at", $this->hospital->getUpdate_at());

        $update->execute();
    }


}