<?php

class CategoriesDAO
{

    public $categories;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `categories` (name) VALUES (:name)");

        $insert->bindValue(":name", $this->categories->getName());

        $insert->execute();
    }
    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `categories` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }
    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `categories`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `categories` WHERE `id` = :id");
        $delete->bindValue(":id", $this->categories->getId());

        $delete->execute();
    }

    public function update()
    {
        $update = $this->db->prepare("UPDATE  `categories` SET name = :name WHERE id = :id");

        $update->bindValue(":id", $this->categories->getId());
        $update->bindValue(":name", $this->categories->getName());

        $update->execute();
    }
}
