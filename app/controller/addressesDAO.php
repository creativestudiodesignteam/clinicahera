<?php

class AddressesDAO
{

    public $addresses;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `addresses` (postcode, state, city, neighborhood, street, number, complement) VALUES (:postcode, :state, :city, :neighborhood, :street, :number, :complement)");
        //postcode state city neighborhood street number complement
        $insert->bindValue(":postcode", $this->addresses->getPostcode());
        $insert->bindValue(":state", $this->addresses->getState());
        $insert->bindValue(":city", $this->addresses->getCity());
        $insert->bindValue(":neighborhood", $this->addresses->getNeighborhood());
        $insert->bindValue(":street", $this->addresses->getStreet());
        $insert->bindValue(":number", $this->addresses->getNumber());
        $insert->bindValue(":complement", $this->addresses->getComplement());

        if ($insert->execute()) {
            return $this->db->lastInsertId();
        } else {
            return false;
        }
    }
    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `addresses` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }
    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `addresses`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `addresses` WHERE `id` = :id");
        $delete->bindValue(":id", $this->addresses->getId());

        $delete->execute();
    }

    public function update()
    {
        $update = $this->db->prepare("UPDATE  `addresses` SET postcode = :postcode, state = :state, city = :city, neighborhood = :neighborhood, street = :street, number = :number, complement = :complement WHERE id = :id");

        $update->bindValue(":id", $this->addresses->getId());
        $update->bindValue(":postcode", $this->addresses->getPostcode());
        $update->bindValue(":state", $this->addresses->getState());
        $update->bindValue(":city", $this->addresses->getCity());
        $update->bindValue(":neighborhood", $this->addresses->getNeighborhood());
        $update->bindValue(":street", $this->addresses->getStreet());
        $update->bindValue(":number", $this->addresses->getNumber());
        $update->bindValue(":complement", $this->addresses->getComplement());

        $update->execute();
    }
}
