<?php 

class ConsultanciesDAO {

    public $consultancies;
    private $db;

    public function __construct ($db) {
        $this->db = $db;
    }

    public function insert () {
        $insert = $this->db->prepare("INSERT INTO `consultancies` (name, fk_clinics) VALUES (:name, :fk_clinics)");

        $insert->bindValue(":name", $this->consultancies->getName());
        $insert->bindValue(":fk_clinics", $this->consultancies->getFk_clinics());

        $insert->execute();
    }
    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `consultancies` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }

    public function listAll () {
        $list = $this->db->prepare("SELECT * FROM `consultancies`");
        $list->execute();
        
        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete () {
        $delete = $this->db->prepare("DELETE FROM `consultancies` WHERE `id` = :id");
        $delete->bindValue(":id", $this->consultancies->getId());
        
        $delete->execute();
    }

    public function update () {
        $update = $this->db->prepare("UPDATE  `consultancies` SET name = :name, fk_clinics = :fk_clinics WHERE id = :id");

        $update->bindValue(":id", $this->consultancies->getId());
        $update->bindValue(":name", $this->consultancies->getName());
        $update->bindValue(":fk_clinics", $this->consultancies->getFk_clinics());

        $update->execute();
    }


}