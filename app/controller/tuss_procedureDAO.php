<?php

class Tuss_procedureDAO
{

    public $tuss_procedure;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `tuss_procedure` (fk_procedure, fk_tuss) VALUES (:fk_procedure, :fk_tuss)");

        $insert->bindValue(":fk_procedure", $this->tuss_procedure->getFk_procedure());
        $insert->bindValue(":fk_tuss", $this->tuss_procedure->getFk_tuss());

        if ($insert->execute()) {
            return $this->db->lastInsertId();
        } else {
            return false;
        }
    }

    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `tuss_procedure` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }

    public function getByIdProcedure($id)
    {
        $list = $this->db->prepare("SELECT * FROM `tuss_procedure` WHERE `fk_procedure`='$id'");
        $list->execute();
        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `tuss_procedure`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `tuss_procedure` WHERE `id` = :id");
        $delete->bindValue(":id", $this->tuss_procedure->getId());

        $delete->execute();
    }

    public function update()
    {
        $update = $this->db->prepare("UPDATE  `tuss_procedure` SET fk_procedure = :fk_procedure, fk_tuss = :fk_tuss, create_at = :create_at, update_at = :update_at WHERE id = :id");

        $update->bindValue(":id", $this->tuss_procedure->getId());
        $update->bindValue(":fk_procedure", $this->tuss_procedure->getFk_procedure());
        $update->bindValue(":fk_tuss", $this->tuss_procedure->getFk_tuss());
        $update->bindValue(":create_at", $this->tuss_procedure->getCreate_at());
        $update->bindValue(":update_at", $this->tuss_procedure->getUpdate_at());

        $update->execute();
    }
}
