<?php

class ClinicsDAO
{

    public $clinics;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `clinics` (name, cnpj, email, tellphone, cellphone, fk_addresses) VALUES (:name, :cnpj, :email, :tellphone, :cellphone, :fk_addresses)");

        $insert->bindValue(":name", $this->clinics->getName());
        $insert->bindValue(":cnpj", $this->clinics->getCnpj());
        $insert->bindValue(":email", $this->clinics->getEmail());
        $insert->bindValue(":tellphone", $this->clinics->getTellphone());
        $insert->bindValue(":cellphone", $this->clinics->getCellphone());
        $insert->bindValue(":fk_addresses", $this->clinics->getFk_addresses());
        if ($insert->execute()) {
            return $this->db->lastInsertId();
        } else {
            return false;
        }
    }
    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `clinics` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }
    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `clinics`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `clinics` WHERE `id` = :id");
        $delete->bindValue(":id", $this->clinics->getId());

        $delete->execute();
    }

    public function update()
    {
        $update = $this->db->prepare("UPDATE  `clinics` SET name = :name, cnpj = :cnpj, email = :email, tellphone = :tellphone, cellphone = :cellphone, fk_addresses = :fk_addresses WHERE id = :id");

        $update->bindValue(":id", $this->clinics->getId());
        $update->bindValue(":name", $this->clinics->getName());
        $update->bindValue(":cnpj", $this->clinics->getCnpj());
        $update->bindValue(":email", $this->clinics->getEmail());
        $update->bindValue(":tellphone", $this->clinics->getTellphone());
        $update->bindValue(":cellphone", $this->clinics->getCellphone());
        $update->bindValue(":fk_addresses", $this->clinics->getFk_addresses());

        $update->execute();
    }
}
