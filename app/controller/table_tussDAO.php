<?php

class Table_tussDAO
{

    public $table_tuss;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `table_tuss` (cod,group_tuss, sub_group, procedure_tuss) VALUES (:cod, :group_tuss, :sub_group, :procedure_tuss)");

        $insert->bindValue(":cod", $this->table_tuss->getCod());
        $insert->bindValue(":group_tuss", $this->table_tuss->getGroup_tuss());
        $insert->bindValue(":sub_group", $this->table_tuss->getSub_group());
        $insert->bindValue(":procedure_tuss", $this->table_tuss->getProcedure_tuss());

        $insert->execute();
    }

    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `table_tuss` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }

    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `table_tuss`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `table_tuss` WHERE `id` = :id");
        $delete->bindValue(":id", $this->table_tuss->getId());

        $delete->execute();
    }

    public function update()
    {
        $update = $this->db->prepare("UPDATE  `table_tuss` SET cod = :cod ,group_tuss = :group_tuss, sub_group = :sub_group, procedure_tuss = :procedure_tuss WHERE id = :id");

        $update->bindValue(":id", $this->table_tuss->getId());
        $update->bindValue(":cod", $this->table_tuss->getCod());
        $update->bindValue(":group_tuss", $this->table_tuss->getGroup_tuss());
        $update->bindValue(":sub_group", $this->table_tuss->getSub_group());
        $update->bindValue(":procedure_tuss", $this->table_tuss->getProcedure_tuss());

        $update->execute();
    }
}
