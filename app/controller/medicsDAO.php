<?php

class MedicsDAO
{

    public $medics;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `medics` (name, time, cpf, cnpj, reason, rg, birthday, specialty, council_number, council_initials, council_uf, sexo, email, tellphone, cellphone, fk_addresses, fk_clinics, fk_bank) VALUES (:name, :time, :cpf, :cnpj, :reason, :rg, :birthday, :specialty, :council_number, :council_initials, :council_uf, :sexo, :email, :tellphone, :cellphone, :fk_addresses, :fk_clinics, :fk_bank)");

        $insert->bindValue(":name", $this->medics->getName());
        $insert->bindValue(":time", $this->medics->getTime());
        $insert->bindValue(":cpf", $this->medics->getCpf());
        $insert->bindValue(":cnpj", $this->medics->getCnpj());
        $insert->bindValue(":reason", $this->medics->getReason());
        $insert->bindValue(":rg", $this->medics->getRg());
        $insert->bindValue(":birthday", $this->medics->getBirthday());
        $insert->bindValue(":specialty", $this->medics->getSpecialty());
        $insert->bindValue(":council_number", $this->medics->getCouncil_number());
        $insert->bindValue(":council_initials", $this->medics->getCouncil_initials());
        $insert->bindValue(":council_uf", $this->medics->getCouncil_uf());
        $insert->bindValue(":sexo", $this->medics->getSexo());
        $insert->bindValue(":email", $this->medics->getEmail());
        $insert->bindValue(":tellphone", $this->medics->getTellphone());
        $insert->bindValue(":cellphone", $this->medics->getCellphone());
        $insert->bindValue(":fk_addresses", $this->medics->getFk_addresses());
        $insert->bindValue(":fk_clinics", $this->medics->getFk_clinics());
        $insert->bindValue(":fk_bank", $this->medics->getFk_bank());
        if ($insert->execute()) {
            return $this->db->lastInsertId();
        } else {
            return false;
        }
    }

    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `medics`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `medics` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }
    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `medics` WHERE `id` = :id");
        $delete->bindValue(":id", $this->medics->getId());

        $delete->execute();
    }

    public function update()
    {
        $update = $this->db->prepare("UPDATE  `medics` SET name = :name, time = :time, cpf = :cpf, cnpj = :cnpj, reason = :reason, rg = :rg, birthday = :birthday, specialty = :specialty, council_number = :council_number, council_initials = :council_initials, council_uf = :council_uf, sexo = :sexo, email = :email, tellphone = :tellphone, cellphone = :cellphone, fk_addresses = :fk_addresses, fk_clinics = :fk_clinics, fk_bank = :fk_bank, created_at = :created_at, updated_at = :updated_at WHERE id = :id");

        $update->bindValue(":id", $this->medics->getId());
        $update->bindValue(":name", $this->medics->getName());
        $update->bindValue(":time", $this->medics->getTime());
        $update->bindValue(":cpf", $this->medics->getCpf());
        $update->bindValue(":cnpj", $this->medics->getCnpj());
        $update->bindValue(":reason", $this->medics->getReason());
        $update->bindValue(":rg", $this->medics->getRg());
        $update->bindValue(":birthday", $this->medics->getBirthday());
        $update->bindValue(":specialty", $this->medics->getSpecialty());
        $update->bindValue(":council_number", $this->medics->getCouncil_number());
        $update->bindValue(":council_initials", $this->medics->getCouncil_initials());
        $update->bindValue(":council_uf", $this->medics->getCouncil_uf());
        $update->bindValue(":sexo", $this->medics->getSexo());
        $update->bindValue(":email", $this->medics->getEmail());
        $update->bindValue(":tellphone", $this->medics->getTellphone());
        $update->bindValue(":cellphone", $this->medics->getCellphone());
        $update->bindValue(":fk_addresses", $this->medics->getFk_addresses());
        $update->bindValue(":fk_clinics", $this->medics->getFk_clinics());
        $update->bindValue(":fk_bank", $this->medics->getFk_bank());
        $update->bindValue(":created_at", $this->medics->getCreated_at());
        $update->bindValue(":updated_at", $this->medics->getUpdated_at());

        $update->execute();
    }
}
