<?php

class GroupDAO
{

    public $group;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `group` (name) VALUES (:name)");

        $insert->bindValue(":name", $this->group->getName());

        $insert->execute();
    }
    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `group` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }
    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `group`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `group` WHERE `id` = :id");
        $delete->bindValue(":id", $this->group->getId());

        $delete->execute();
    }

    public function update()
    {
        $update = $this->db->prepare("UPDATE  `group` SET name = :name WHERE id = :id");

        $update->bindValue(":id", $this->group->getId());
        $update->bindValue(":name", $this->group->getName());

        $update->execute();
    }
}
