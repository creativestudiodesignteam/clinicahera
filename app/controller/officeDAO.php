<?php

class OfficeDAO
{

    public $office;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `office` (dayWeek, timeStart, timeEnd, fk_medics) VALUES (:dayWeek, :timeStart, :timeEnd, :fk_medics)");

        $insert->bindValue(":dayWeek", $this->office->getDayWeek());
        $insert->bindValue(":timeStart", $this->office->getTimeStart());
        $insert->bindValue(":timeEnd", $this->office->getTimeEnd());
        $insert->bindValue(":fk_medics", $this->office->getFk_medics());

        if ($insert->execute()) {
            return $this->db->lastInsertId();
        } else {
            return false;
        }
    }

    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `office`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `office` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }
    public function getByIdMedics($id)
    {
        $list = $this->db->prepare("SELECT * FROM `office` WHERE `fk_medics`='$id'");
        $list->execute();
        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `office` WHERE `id` = :id");
        $delete->bindValue(":id", $this->office->getId());

        $delete->execute();
    }

    public function update()
    {
        $update = $this->db->prepare("UPDATE  `office` SET dayWeek = :dayWeek, timeStart = :timeStart, timeEnd = :timeEnd, fk_medics = :fk_medics WHERE id = :id");

        $update->bindValue(":id", $this->office->getId());
        $update->bindValue(":dayWeek", $this->office->getDayWeek());
        $update->bindValue(":timeStart", $this->office->getTimeStart());
        $update->bindValue(":timeEnd", $this->office->getTimeEnd());
        $update->bindValue(":fk_medics", $this->office->getFk_medics());

        $update->execute();
    }
}
