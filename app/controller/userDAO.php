<?php

class UserDAO
{

    public $user;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `user` (name, email, password, tellphone, cellphone, fk_addresses, fk_group) VALUES (:name, :email, :password, :tellphone, :cellphone, :fk_addresses, :fk_group)");

        $insert->bindValue(":name", $this->user->getName());
        $insert->bindValue(":email", $this->user->getEmail());
        $insert->bindValue(":password", $this->user->getPassword());
        $insert->bindValue(":tellphone", $this->user->getTellphone());
        $insert->bindValue(":cellphone", $this->user->getCellphone());
        $insert->bindValue(":fk_addresses", $this->user->getFk_addresses());
        $insert->bindValue(":fk_group", $this->user->getFk_group());

        $insert->execute();
    }
    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `user` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }
    public function emailExists($email)
    {
        $list = $this->db->prepare("SELECT * FROM `user` WHERE `email`='$email'");
        $list->execute();
        return $list->fetchAll(PDO::FETCH_ASSOC);
    }
    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `user`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `user` WHERE `id` = :id");
        $delete->bindValue(":id", $this->user->getId());

        $delete->execute();
    }

    public function update()
    {
        $update = $this->db->prepare("UPDATE  `user` SET name = :name, email = :email, password = :password, tellphone = :tellphone, cellphone = :cellphone, fk_addresses = :fk_addresses, fk_group = :fk_group WHERE id = :id");

        $update->bindValue(":id", $this->user->getId());
        $update->bindValue(":name", $this->user->getName());
        $update->bindValue(":email", $this->user->getEmail());
        $update->bindValue(":password", $this->user->getPassword());
        $update->bindValue(":tellphone", $this->user->getTellphone());
        $update->bindValue(":cellphone", $this->user->getCellphone());
        $update->bindValue(":fk_addresses", $this->user->getFk_addresses());
        $update->bindValue(":fk_group", $this->user->getFk_group());

        $update->execute();
    }
}
