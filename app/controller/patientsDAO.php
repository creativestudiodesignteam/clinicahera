<?php

class PatientsDAO
{

    public $patients;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `patients` (name, email, rg, cpf, emitting_organ, sexo, birth_place, birthday, groups_references, situation, specialty, tellphone_commercial, tellphone_residential, name_message, cellphone, fk_addresses, fk_medic, fk_plan, create_at, update_at) VALUES (:name, :email, :rg, :cpf, :emitting_organ, :sexo, :birth_place, :birthday, :groups_references, :situation, :specialty, :tellphone_commercial, :tellphone_residential, :name_message, :cellphone, :fk_addresses, :fk_medic, :fk_plan, :create_at, :update_at)");

        $insert->bindValue(":name", $this->patients->getName());
        $insert->bindValue(":email", $this->patients->getEmail());
        $insert->bindValue(":rg", $this->patients->getRg());
        $insert->bindValue(":cpf", $this->patients->getCpf());
        $insert->bindValue(":emitting_organ", $this->patients->getEmitting_organ());
        $insert->bindValue(":sexo", $this->patients->getSexo());
        $insert->bindValue(":birth_place", $this->patients->getBirth_place());
        $insert->bindValue(":birthday", $this->patients->getBirthday());
        $insert->bindValue(":groups_references", $this->patients->getGroups_references());
        $insert->bindValue(":situation", $this->patients->getSituation());
        $insert->bindValue(":specialty", $this->patients->getSpecialty());
        $insert->bindValue(":tellphone_commercial", $this->patients->getTellphone_commercial());
        $insert->bindValue(":tellphone_residential", $this->patients->getTellphone_residential());
        $insert->bindValue(":name_message", $this->patients->getName_message());
        $insert->bindValue(":cellphone", $this->patients->getCellphone());
        $insert->bindValue(":fk_addresses", $this->patients->getFk_addresses());
        $insert->bindValue(":fk_medic", $this->patients->getFk_medic());
        $insert->bindValue(":fk_plan", $this->patients->getFk_plan());
        $insert->bindValue(":create_at", $this->patients->getCreate_at());
        $insert->bindValue(":update_at", $this->patients->getUpdate_at());

        $insert->execute();
    }

    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `patients`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }
    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `patients` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }

    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `patients` WHERE `id` = :id");
        $delete->bindValue(":id", $this->patients->getId());

        $delete->execute();
    }

    public function update()
    {
        $update = $this->db->prepare("UPDATE  `patients` SET name = :name, email = :email, rg = :rg, cpf = :cpf, emitting_organ = :emitting_organ, sexo = :sexo, birth_place = :birth_place, birthday = :birthday, groups_references = :groups_references, situation = :situation, specialty = :specialty, tellphone_commercial = :tellphone_commercial, tellphone_residential = :tellphone_residential, name_message = :name_message, cellphone = :cellphone, fk_addresses = :fk_addresses, fk_medic = :fk_medic, fk_plan = :fk_plan, create_at = :create_at, update_at = :update_at WHERE id = :id");

        $update->bindValue(":id", $this->patients->getId());
        $update->bindValue(":name", $this->patients->getName());
        $update->bindValue(":email", $this->patients->getEmail());
        $update->bindValue(":rg", $this->patients->getRg());
        $update->bindValue(":cpf", $this->patients->getCpf());
        $update->bindValue(":emitting_organ", $this->patients->getEmitting_organ());
        $update->bindValue(":sexo", $this->patients->getSexo());
        $update->bindValue(":birth_place", $this->patients->getBirth_place());
        $update->bindValue(":birthday", $this->patients->getBirthday());
        $update->bindValue(":groups_references", $this->patients->getGroups_references());
        $update->bindValue(":situation", $this->patients->getSituation());
        $update->bindValue(":specialty", $this->patients->getSpecialty());
        $update->bindValue(":tellphone_commercial", $this->patients->getTellphone_commercial());
        $update->bindValue(":tellphone_residential", $this->patients->getTellphone_residential());
        $update->bindValue(":name_message", $this->patients->getName_message());
        $update->bindValue(":cellphone", $this->patients->getCellphone());
        $update->bindValue(":fk_addresses", $this->patients->getFk_addresses());
        $update->bindValue(":fk_medic", $this->patients->getFk_medic());
        $update->bindValue(":fk_plan", $this->patients->getFk_plan());
        $update->bindValue(":create_at", $this->patients->getCreate_at());
        $update->bindValue(":update_at", $this->patients->getUpdate_at());

        $update->execute();
    }
}
