<?php

class Bank_accountDAO
{

    public $bank_account;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `bank_account` (name, number, agency, account) VALUES (:name, :number, :agency, :account)");

        $insert->bindValue(":name", $this->bank_account->getName());
        $insert->bindValue(":number", $this->bank_account->getNumber());
        $insert->bindValue(":agency", $this->bank_account->getAgency());
        $insert->bindValue(":account", $this->bank_account->getAccount());

        if ($insert->execute()) {
            return $this->db->lastInsertId();
        } else {
            return false;
        }
    }

    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `bank_account` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }


    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `bank_account`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `bank_account` WHERE `id` = :id");
        $delete->bindValue(":id", $this->bank_account->getId());

        $delete->execute();
    }

    public function update()
    {
        $update = $this->db->prepare("UPDATE  `bank_account` SET name = :name, number = :number, agency = :agency, account = :account WHERE id = :id");

        $update->bindValue(":id", $this->bank_account->getId());
        $update->bindValue(":name", $this->bank_account->getName());
        $update->bindValue(":number", $this->bank_account->getNumber());
        $update->bindValue(":agency", $this->bank_account->getAgency());
        $update->bindValue(":account", $this->bank_account->getAccount());

        $update->execute();
    }
}
