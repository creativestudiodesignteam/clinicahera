<?php

class ProceduresDAO
{

    public $procedures;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `procedures` (name, value, date, hours, duration, table_tuss, situation, obs, dateSendDocs, material, indication, cid_10, type_charge, fk_hospital, fk_patient, fk_medics) VALUES (:name, :value, :date, :hours, :duration, :table_tuss, :situation, :obs, :dateSendDocs, :material, :indication, :cid_10, :type_charge, :fk_hospital, :fk_patient, :fk_medics)");

        $insert->bindValue(":name", $this->procedures->getName());
        $insert->bindValue(":value", $this->procedures->getValue());
        $insert->bindValue(":date", $this->procedures->getDate());
        $insert->bindValue(":hours", $this->procedures->getHours());
        $insert->bindValue(":duration", $this->procedures->getDuration());
        $insert->bindValue(":table_tuss", $this->procedures->getTable_tuss());
        $insert->bindValue(":situation", $this->procedures->getSituation());
        $insert->bindValue(":obs", $this->procedures->getObs());
        $insert->bindValue(":dateSendDocs", $this->procedures->getDateSendDocs());
        $insert->bindValue(":material", $this->procedures->getMaterial());
        $insert->bindValue(":indication", $this->procedures->getIndication());
        $insert->bindValue(":cid_10", $this->procedures->getCid_10());
        $insert->bindValue(":type_charge", $this->procedures->getType_charge());
        $insert->bindValue(":fk_hospital", $this->procedures->getFk_hospital());
        $insert->bindValue(":fk_patient", $this->procedures->getFk_patient());
        $insert->bindValue(":fk_medics", $this->procedures->getFk_medics());
        if ($insert->execute()) {
            return $this->db->lastInsertId();
        } else {
            return false;
        }
    }
    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `procedures` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }

    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `procedures`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `procedures` WHERE `id` = :id");
        $delete->bindValue(":id", $this->procedures->getId());

        $delete->execute();
    }
    public function getByConfirmed()
    {
        $list = $this->db->prepare("SELECT * FROM `procedures` WHERE `situation`= 'Operado'");
        $list->execute();
        return $list->fetchAll(PDO::FETCH_ASSOC);
    }
    public function update()
    {
        $update = $this->db->prepare("UPDATE  `procedures` SET name = :name, value = :value, date = :date, hours = :hours, duration = :duration, table_tuss = :table_tuss, situation = :situation, obs = :obs, dateSendDocs = :dateSendDocs, material = :material, indication = :indication, cid_10 = :cid_10, type_charge = :type_charge, fk_hospital = :fk_hospital, fk_patient = :fk_patient, fk_medics = :fk_medics WHERE id = :id");

        $update->bindValue(":id", $this->procedures->getId());
        $update->bindValue(":name", $this->procedures->getName());
        $update->bindValue(":value", $this->procedures->getValue());
        $update->bindValue(":date", $this->procedures->getDate());
        $update->bindValue(":hours", $this->procedures->getHours());
        $update->bindValue(":duration", $this->procedures->getDuration());
        $update->bindValue(":table_tuss", $this->procedures->getTable_tuss());
        $update->bindValue(":situation", $this->procedures->getSituation());
        $update->bindValue(":obs", $this->procedures->getObs());
        $update->bindValue(":dateSendDocs", $this->procedures->getDateSendDocs());
        $update->bindValue(":material", $this->procedures->getMaterial());
        $update->bindValue(":indication", $this->procedures->getIndication());
        $update->bindValue(":cid_10", $this->procedures->getCid_10());
        $update->bindValue(":type_charge", $this->procedures->getType_charge());
        $update->bindValue(":fk_hospital", $this->procedures->getFk_hospital());
        $update->bindValue(":fk_patient", $this->procedures->getFk_patient());
        $update->bindValue(":fk_medics", $this->procedures->getFk_medics());

        $update->execute();
    }

    public function update_team()
    {
        $update = $this->db->prepare("UPDATE  `procedures` SET fk_team = 1 WHERE id = :id");

        $update->bindValue(":id", $this->procedures->getId());

        $update->execute();
    }
}
