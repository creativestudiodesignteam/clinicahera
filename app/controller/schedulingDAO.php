<?php

class SchedulingDAO
{

    public $scheduling;
    private $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function insert()
    {
        $insert = $this->db->prepare("INSERT INTO `scheduling` (email, tellphone, value, type, reason, situation, color, start, end, fk_medic, fk_patients) VALUES (:email, :tellphone, :value, :type, :reason, :situation, :color, :start, :end, :fk_medic, :fk_patients)");

        $insert->bindValue(":email", $this->scheduling->getEmail());
        $insert->bindValue(":tellphone", $this->scheduling->getTellphone());
        $insert->bindValue(":type", $this->scheduling->getType());
        $insert->bindValue(":reason", $this->scheduling->getReason());
        $insert->bindValue(":situation", $this->scheduling->getSituation());
        $insert->bindValue(":color", $this->scheduling->getColor());
        $insert->bindValue(":start", $this->scheduling->getStart());
        $insert->bindValue(":end", $this->scheduling->getEnd());
        $insert->bindValue(":value", $this->scheduling->getValue());
        $insert->bindValue(":fk_medic", $this->scheduling->getFk_medic());
        $insert->bindValue(":fk_patients", $this->scheduling->getFk_patients());

        $insert->execute();
    }
    public function getById($id)
    {
        $list = $this->db->prepare("SELECT * FROM `scheduling` WHERE `id`='$id'");
        $list->execute();
        return $list->fetch(PDO::FETCH_ASSOC);
    }
    public function getByConfirmed()
    {
        $list = $this->db->prepare("SELECT * FROM `scheduling` WHERE `situation`= 'Atendido' ");
        $list->execute();
        return $list->fetchAll(PDO::FETCH_ASSOC);
    }


    public function listAll()
    {
        $list = $this->db->prepare("SELECT * FROM `scheduling`");
        $list->execute();

        return $list->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete()
    {
        $delete = $this->db->prepare("DELETE FROM `scheduling` WHERE `id` = :id");
        $delete->bindValue(":id", $this->scheduling->getId());

        $delete->execute();
    }

    public function update()
    {
        $update = $this->db->prepare("UPDATE  `scheduling` SET email = :email, tellphone = :tellphone, value = :value,type = :type, reason = :reason, situation = :situation, color = :color, start = :start, end = :end, fk_medic = :fk_medic, fk_patients = :fk_patients WHERE id = :id");

        $update->bindValue(":id", $this->scheduling->getId());
        $update->bindValue(":email", $this->scheduling->getEmail());
        $update->bindValue(":tellphone", $this->scheduling->getTellphone());
        $update->bindValue(":type", $this->scheduling->getType());
        $update->bindValue(":reason", $this->scheduling->getReason());
        $update->bindValue(":situation", $this->scheduling->getSituation());
        $update->bindValue(":color", $this->scheduling->getColor());
        $update->bindValue(":start", $this->scheduling->getStart());
        $update->bindValue(":end", $this->scheduling->getEnd());
        $update->bindValue(":value", $this->scheduling->getValue());
        $update->bindValue(":fk_medic", $this->scheduling->getFk_medic());
        $update->bindValue(":fk_patients", $this->scheduling->getFk_patients());

        $update->execute();
    }
}
