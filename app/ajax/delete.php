<?php

include '../core/session.class.php';
include '../core/connection.class.php';

$db         = new Connection();
$session    = new Session();

$db = $db->connect();
$session->start();

if (isset($_SESSION['id'])) {
  if (isset($_GET['provider']) && isset($_GET['id']) && ctype_digit($_GET['id'])) {
    $list = array('clinics', 'health_insurance_companies', 'health_insurances', 'bank_accounts', 'addresses', 'medics', 'patients', 'scheduling', 'services', 'users');

    if (in_array($_GET['provider'], $list)) {
      $provider = $_GET['provider'];

      $update = $db->prepare("UPDATE `$provider` SET `provider` = :provider WHERE `id` = :id");

      $update->bindValue(':id', $_GET['id']);
      $update->bindValue(':provider', '0');

      $update->execute();
    }
  }
}
